USE [SimplCommerce]
GO
ALTER TABLE [dbo].[WishList_WishListItem] DROP CONSTRAINT [FK_WishList_WishListItem_WishList_WishList_WishListId]
GO
ALTER TABLE [dbo].[WishList_WishListItem] DROP CONSTRAINT [FK_WishList_WishListItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[WishList_WishList] DROP CONSTRAINT [FK_WishList_WishList_Core_User_UserId]
GO
ALTER TABLE [dbo].[Tax_TaxRate] DROP CONSTRAINT [FK_Tax_TaxRate_Tax_TaxClass_TaxClassId]
GO
ALTER TABLE [dbo].[Tax_TaxRate] DROP CONSTRAINT [FK_Tax_TaxRate_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Tax_TaxRate] DROP CONSTRAINT [FK_Tax_TaxRate_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[ShoppingCart_CartItem] DROP CONSTRAINT [FK_ShoppingCart_CartItem_ShoppingCart_Cart_CartId]
GO
ALTER TABLE [dbo].[ShoppingCart_CartItem] DROP CONSTRAINT [FK_ShoppingCart_CartItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[ShoppingCart_Cart] DROP CONSTRAINT [FK_ShoppingCart_Cart_Core_User_CustomerId]
GO
ALTER TABLE [dbo].[ShoppingCart_Cart] DROP CONSTRAINT [FK_ShoppingCart_Cart_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination] DROP CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination] DROP CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_District_DistrictId]
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination] DROP CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Shipments_ShipmentItem] DROP CONSTRAINT [FK_Shipments_ShipmentItem_Shipments_Shipment_ShipmentId]
GO
ALTER TABLE [dbo].[Shipments_ShipmentItem] DROP CONSTRAINT [FK_Shipments_ShipmentItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Shipments_Shipment] DROP CONSTRAINT [FK_Shipments_Shipment_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Shipments_Shipment] DROP CONSTRAINT [FK_Shipments_Shipment_Inventory_Warehouse_WarehouseId]
GO
ALTER TABLE [dbo].[Shipments_Shipment] DROP CONSTRAINT [FK_Shipments_Shipment_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Reviews_Review] DROP CONSTRAINT [FK_Reviews_Review_Core_User_UserId]
GO
ALTER TABLE [dbo].[Reviews_Reply] DROP CONSTRAINT [FK_Reviews_Reply_Reviews_Review_ReviewId]
GO
ALTER TABLE [dbo].[Reviews_Reply] DROP CONSTRAINT [FK_Reviews_Reply_Core_User_UserId]
GO
ALTER TABLE [dbo].[ProductComparison_ComparingProduct] DROP CONSTRAINT [FK_ProductComparison_ComparingProduct_Core_User_UserId]
GO
ALTER TABLE [dbo].[ProductComparison_ComparingProduct] DROP CONSTRAINT [FK_ProductComparison_ComparingProduct_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Pricing_Coupon] DROP CONSTRAINT [FK_Pricing_Coupon_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CatalogRuleCustomerGroup] DROP CONSTRAINT [FK_Pricing_CatalogRuleCustomerGroup_Pricing_CatalogRule_CatalogRuleId]
GO
ALTER TABLE [dbo].[Pricing_CatalogRuleCustomerGroup] DROP CONSTRAINT [FK_Pricing_CatalogRuleCustomerGroup_Core_CustomerGroup_CustomerGroupId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage] DROP CONSTRAINT [FK_Pricing_CartRuleUsage_Pricing_Coupon_CouponId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage] DROP CONSTRAINT [FK_Pricing_CartRuleUsage_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage] DROP CONSTRAINT [FK_Pricing_CartRuleUsage_Core_User_UserId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleProduct] DROP CONSTRAINT [FK_Pricing_CartRuleProduct_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleProduct] DROP CONSTRAINT [FK_Pricing_CartRuleProduct_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCustomerGroup] DROP CONSTRAINT [FK_Pricing_CartRuleCustomerGroup_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCustomerGroup] DROP CONSTRAINT [FK_Pricing_CartRuleCustomerGroup_Core_CustomerGroup_CustomerGroupId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCategory] DROP CONSTRAINT [FK_Pricing_CartRuleCategory_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCategory] DROP CONSTRAINT [FK_Pricing_CartRuleCategory_Catalog_Category_CategoryId]
GO
ALTER TABLE [dbo].[Payments_Payment] DROP CONSTRAINT [FK_Payments_Payment_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Orders_OrderItem] DROP CONSTRAINT [FK_Orders_OrderItem_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Orders_OrderItem] DROP CONSTRAINT [FK_Orders_OrderItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Orders_OrderHistory] DROP CONSTRAINT [FK_Orders_OrderHistory_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Orders_OrderHistory] DROP CONSTRAINT [FK_Orders_OrderHistory_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Orders_OrderAddress] DROP CONSTRAINT [FK_Orders_OrderAddress_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Orders_OrderAddress] DROP CONSTRAINT [FK_Orders_OrderAddress_Core_District_DistrictId]
GO
ALTER TABLE [dbo].[Orders_OrderAddress] DROP CONSTRAINT [FK_Orders_OrderAddress_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Orders_Order] DROP CONSTRAINT [FK_Orders_Order_Orders_OrderAddress_ShippingAddressId]
GO
ALTER TABLE [dbo].[Orders_Order] DROP CONSTRAINT [FK_Orders_Order_Orders_OrderAddress_BillingAddressId]
GO
ALTER TABLE [dbo].[Orders_Order] DROP CONSTRAINT [FK_Orders_Order_Orders_Order_ParentId]
GO
ALTER TABLE [dbo].[Orders_Order] DROP CONSTRAINT [FK_Orders_Order_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[Orders_Order] DROP CONSTRAINT [FK_Orders_Order_Core_User_CustomerId]
GO
ALTER TABLE [dbo].[Orders_Order] DROP CONSTRAINT [FK_Orders_Order_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[News_NewsItemCategory] DROP CONSTRAINT [FK_News_NewsItemCategory_News_NewsItem_NewsItemId]
GO
ALTER TABLE [dbo].[News_NewsItemCategory] DROP CONSTRAINT [FK_News_NewsItemCategory_News_NewsCategory_CategoryId]
GO
ALTER TABLE [dbo].[News_NewsItem] DROP CONSTRAINT [FK_News_NewsItem_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[News_NewsItem] DROP CONSTRAINT [FK_News_NewsItem_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[News_NewsItem] DROP CONSTRAINT [FK_News_NewsItem_Core_Media_ThumbnailImageId]
GO
ALTER TABLE [dbo].[Localization_Resource] DROP CONSTRAINT [FK_Localization_Resource_Localization_Culture_CultureId]
GO
ALTER TABLE [dbo].[Localization_LocalizedContentProperty] DROP CONSTRAINT [FK_Localization_LocalizedContentProperty_Localization_Culture_CultureId]
GO
ALTER TABLE [dbo].[Inventory_Warehouse] DROP CONSTRAINT [FK_Inventory_Warehouse_Core_Vendor_VendorId]
GO
ALTER TABLE [dbo].[Inventory_Warehouse] DROP CONSTRAINT [FK_Inventory_Warehouse_Core_Address_AddressId]
GO
ALTER TABLE [dbo].[Inventory_StockHistory] DROP CONSTRAINT [FK_Inventory_StockHistory_Inventory_Warehouse_WarehouseId]
GO
ALTER TABLE [dbo].[Inventory_StockHistory] DROP CONSTRAINT [FK_Inventory_StockHistory_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Inventory_StockHistory] DROP CONSTRAINT [FK_Inventory_StockHistory_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Inventory_Stock] DROP CONSTRAINT [FK_Inventory_Stock_Inventory_Warehouse_WarehouseId]
GO
ALTER TABLE [dbo].[Inventory_Stock] DROP CONSTRAINT [FK_Inventory_Stock_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Core_WidgetInstance] DROP CONSTRAINT [FK_Core_WidgetInstance_Core_WidgetZone_WidgetZoneId]
GO
ALTER TABLE [dbo].[Core_WidgetInstance] DROP CONSTRAINT [FK_Core_WidgetInstance_Core_Widget_WidgetId]
GO
ALTER TABLE [dbo].[Core_UserToken] DROP CONSTRAINT [FK_Core_UserToken_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserRole] DROP CONSTRAINT [FK_Core_UserRole_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserRole] DROP CONSTRAINT [FK_Core_UserRole_Core_Role_RoleId]
GO
ALTER TABLE [dbo].[Core_UserLogin] DROP CONSTRAINT [FK_Core_UserLogin_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserClaim] DROP CONSTRAINT [FK_Core_UserClaim_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserAddress] DROP CONSTRAINT [FK_Core_UserAddress_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserAddress] DROP CONSTRAINT [FK_Core_UserAddress_Core_Address_AddressId]
GO
ALTER TABLE [dbo].[Core_User] DROP CONSTRAINT [FK_Core_User_Core_Vendor_VendorId]
GO
ALTER TABLE [dbo].[Core_User] DROP CONSTRAINT [FK_Core_User_Core_UserAddress_DefaultShippingAddressId]
GO
ALTER TABLE [dbo].[Core_User] DROP CONSTRAINT [FK_Core_User_Core_UserAddress_DefaultBillingAddressId]
GO
ALTER TABLE [dbo].[Core_StateOrProvince] DROP CONSTRAINT [FK_Core_StateOrProvince_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Core_RoleClaim] DROP CONSTRAINT [FK_Core_RoleClaim_Core_Role_RoleId]
GO
ALTER TABLE [dbo].[Core_Entity] DROP CONSTRAINT [FK_Core_Entity_Core_EntityType_EntityTypeId]
GO
ALTER TABLE [dbo].[Core_District] DROP CONSTRAINT [FK_Core_District_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Core_CustomerGroupUser] DROP CONSTRAINT [FK_Core_CustomerGroupUser_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_CustomerGroupUser] DROP CONSTRAINT [FK_Core_CustomerGroupUser_Core_CustomerGroup_CustomerGroupId]
GO
ALTER TABLE [dbo].[Core_Address] DROP CONSTRAINT [FK_Core_Address_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Core_Address] DROP CONSTRAINT [FK_Core_Address_Core_District_DistrictId]
GO
ALTER TABLE [dbo].[Core_Address] DROP CONSTRAINT [FK_Core_Address_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Contacts_Contact] DROP CONSTRAINT [FK_Contacts_Contact_Contacts_ContactArea_ContactAreaId]
GO
ALTER TABLE [dbo].[Comments_Comment] DROP CONSTRAINT [FK_Comments_Comment_Core_User_UserId]
GO
ALTER TABLE [dbo].[Comments_Comment] DROP CONSTRAINT [FK_Comments_Comment_Comments_Comment_ParentId]
GO
ALTER TABLE [dbo].[Cms_Page] DROP CONSTRAINT [FK_Cms_Page_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[Cms_Page] DROP CONSTRAINT [FK_Cms_Page_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Cms_MenuItem] DROP CONSTRAINT [FK_Cms_MenuItem_Core_Entity_EntityId]
GO
ALTER TABLE [dbo].[Cms_MenuItem] DROP CONSTRAINT [FK_Cms_MenuItem_Cms_MenuItem_ParentId]
GO
ALTER TABLE [dbo].[Cms_MenuItem] DROP CONSTRAINT [FK_Cms_MenuItem_Cms_Menu_MenuId]
GO
ALTER TABLE [dbo].[Catalog_ProductTemplateProductAttribute] DROP CONSTRAINT [FK_Catalog_ProductTemplateProductAttribute_Catalog_ProductTemplate_ProductTemplateId]
GO
ALTER TABLE [dbo].[Catalog_ProductTemplateProductAttribute] DROP CONSTRAINT [FK_Catalog_ProductTemplateProductAttribute_Catalog_ProductAttribute_ProductAttributeId]
GO
ALTER TABLE [dbo].[Catalog_ProductPriceHistory] DROP CONSTRAINT [FK_Catalog_ProductPriceHistory_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Catalog_ProductPriceHistory] DROP CONSTRAINT [FK_Catalog_ProductPriceHistory_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionValue] DROP CONSTRAINT [FK_Catalog_ProductOptionValue_Catalog_ProductOption_OptionId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionValue] DROP CONSTRAINT [FK_Catalog_ProductOptionValue_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionCombination] DROP CONSTRAINT [FK_Catalog_ProductOptionCombination_Catalog_ProductOption_OptionId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionCombination] DROP CONSTRAINT [FK_Catalog_ProductOptionCombination_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductCategory] DROP CONSTRAINT [FK_Catalog_ProductCategory_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductCategory] DROP CONSTRAINT [FK_Catalog_ProductCategory_Catalog_Category_CategoryId]
GO
ALTER TABLE [dbo].[Catalog_ProductAttribute] DROP CONSTRAINT [FK_Catalog_ProductAttribute_Catalog_ProductAttributeGroup_GroupId]
GO
ALTER TABLE [dbo].[Catalog_Product] DROP CONSTRAINT [FK_Catalog_Product_Tax_TaxClass_TaxClassId]
GO
ALTER TABLE [dbo].[Catalog_Product] DROP CONSTRAINT [FK_Catalog_Product_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[Catalog_Product] DROP CONSTRAINT [FK_Catalog_Product_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Catalog_Product] DROP CONSTRAINT [FK_Catalog_Product_Core_Media_ThumbnailImageId]
GO
ALTER TABLE [dbo].[Catalog_Product] DROP CONSTRAINT [FK_Catalog_Product_Catalog_Brand_BrandId]
GO
ALTER TABLE [dbo].[Catalog_Category] DROP CONSTRAINT [FK_Catalog_Category_Core_Media_ThumbnailImageId]
GO
ALTER TABLE [dbo].[Catalog_Category] DROP CONSTRAINT [FK_Catalog_Category_Catalog_Category_ParentId]
GO
ALTER TABLE [dbo].[ActivityLog_Activity] DROP CONSTRAINT [FK_ActivityLog_Activity_ActivityLog_ActivityType_ActivityTypeId]
GO
ALTER TABLE [dbo].[ShoppingCart_Cart] DROP CONSTRAINT [DF__ShoppingC__Locke__531856C7]
GO
/****** Object:  Index [IX_WishList_WishListItem_WishListId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_WishList_WishListItem_WishListId] ON [dbo].[WishList_WishListItem]
GO
/****** Object:  Index [IX_WishList_WishListItem_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_WishList_WishListItem_ProductId] ON [dbo].[WishList_WishListItem]
GO
/****** Object:  Index [IX_WishList_WishList_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_WishList_WishList_UserId] ON [dbo].[WishList_WishList]
GO
/****** Object:  Index [IX_Tax_TaxRate_TaxClassId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Tax_TaxRate_TaxClassId] ON [dbo].[Tax_TaxRate]
GO
/****** Object:  Index [IX_Tax_TaxRate_StateOrProvinceId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Tax_TaxRate_StateOrProvinceId] ON [dbo].[Tax_TaxRate]
GO
/****** Object:  Index [IX_Tax_TaxRate_CountryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Tax_TaxRate_CountryId] ON [dbo].[Tax_TaxRate]
GO
/****** Object:  Index [IX_ShoppingCart_CartItem_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShoppingCart_CartItem_ProductId] ON [dbo].[ShoppingCart_CartItem]
GO
/****** Object:  Index [IX_ShoppingCart_CartItem_CartId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShoppingCart_CartItem_CartId] ON [dbo].[ShoppingCart_CartItem]
GO
/****** Object:  Index [IX_ShoppingCart_Cart_CustomerId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShoppingCart_Cart_CustomerId] ON [dbo].[ShoppingCart_Cart]
GO
/****** Object:  Index [IX_ShoppingCart_Cart_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShoppingCart_Cart_CreatedById] ON [dbo].[ShoppingCart_Cart]
GO
/****** Object:  Index [IX_ShippingTableRate_PriceAndDestination_StateOrProvinceId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShippingTableRate_PriceAndDestination_StateOrProvinceId] ON [dbo].[ShippingTableRate_PriceAndDestination]
GO
/****** Object:  Index [IX_ShippingTableRate_PriceAndDestination_DistrictId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShippingTableRate_PriceAndDestination_DistrictId] ON [dbo].[ShippingTableRate_PriceAndDestination]
GO
/****** Object:  Index [IX_ShippingTableRate_PriceAndDestination_CountryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ShippingTableRate_PriceAndDestination_CountryId] ON [dbo].[ShippingTableRate_PriceAndDestination]
GO
/****** Object:  Index [IX_Shipments_ShipmentItem_ShipmentId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Shipments_ShipmentItem_ShipmentId] ON [dbo].[Shipments_ShipmentItem]
GO
/****** Object:  Index [IX_Shipments_ShipmentItem_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Shipments_ShipmentItem_ProductId] ON [dbo].[Shipments_ShipmentItem]
GO
/****** Object:  Index [IX_Shipments_Shipment_WarehouseId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Shipments_Shipment_WarehouseId] ON [dbo].[Shipments_Shipment]
GO
/****** Object:  Index [IX_Shipments_Shipment_OrderId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Shipments_Shipment_OrderId] ON [dbo].[Shipments_Shipment]
GO
/****** Object:  Index [IX_Shipments_Shipment_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Shipments_Shipment_CreatedById] ON [dbo].[Shipments_Shipment]
GO
/****** Object:  Index [IX_Reviews_Review_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Reviews_Review_UserId] ON [dbo].[Reviews_Review]
GO
/****** Object:  Index [IX_Reviews_Reply_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Reviews_Reply_UserId] ON [dbo].[Reviews_Reply]
GO
/****** Object:  Index [IX_Reviews_Reply_ReviewId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Reviews_Reply_ReviewId] ON [dbo].[Reviews_Reply]
GO
/****** Object:  Index [IX_ProductComparison_ComparingProduct_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ProductComparison_ComparingProduct_UserId] ON [dbo].[ProductComparison_ComparingProduct]
GO
/****** Object:  Index [IX_ProductComparison_ComparingProduct_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ProductComparison_ComparingProduct_ProductId] ON [dbo].[ProductComparison_ComparingProduct]
GO
/****** Object:  Index [IX_Pricing_Coupon_CartRuleId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_Coupon_CartRuleId] ON [dbo].[Pricing_Coupon]
GO
/****** Object:  Index [IX_Pricing_CatalogRuleCustomerGroup_CustomerGroupId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CatalogRuleCustomerGroup_CustomerGroupId] ON [dbo].[Pricing_CatalogRuleCustomerGroup]
GO
/****** Object:  Index [IX_Pricing_CartRuleUsage_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CartRuleUsage_UserId] ON [dbo].[Pricing_CartRuleUsage]
GO
/****** Object:  Index [IX_Pricing_CartRuleUsage_CouponId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CartRuleUsage_CouponId] ON [dbo].[Pricing_CartRuleUsage]
GO
/****** Object:  Index [IX_Pricing_CartRuleUsage_CartRuleId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CartRuleUsage_CartRuleId] ON [dbo].[Pricing_CartRuleUsage]
GO
/****** Object:  Index [IX_Pricing_CartRuleProduct_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CartRuleProduct_ProductId] ON [dbo].[Pricing_CartRuleProduct]
GO
/****** Object:  Index [IX_Pricing_CartRuleCustomerGroup_CustomerGroupId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CartRuleCustomerGroup_CustomerGroupId] ON [dbo].[Pricing_CartRuleCustomerGroup]
GO
/****** Object:  Index [IX_Pricing_CartRuleCategory_CategoryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Pricing_CartRuleCategory_CategoryId] ON [dbo].[Pricing_CartRuleCategory]
GO
/****** Object:  Index [IX_Payments_Payment_OrderId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Payments_Payment_OrderId] ON [dbo].[Payments_Payment]
GO
/****** Object:  Index [IX_Orders_OrderItem_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderItem_ProductId] ON [dbo].[Orders_OrderItem]
GO
/****** Object:  Index [IX_Orders_OrderItem_OrderId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderItem_OrderId] ON [dbo].[Orders_OrderItem]
GO
/****** Object:  Index [IX_Orders_OrderHistory_OrderId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderHistory_OrderId] ON [dbo].[Orders_OrderHistory]
GO
/****** Object:  Index [IX_Orders_OrderHistory_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderHistory_CreatedById] ON [dbo].[Orders_OrderHistory]
GO
/****** Object:  Index [IX_Orders_OrderAddress_StateOrProvinceId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderAddress_StateOrProvinceId] ON [dbo].[Orders_OrderAddress]
GO
/****** Object:  Index [IX_Orders_OrderAddress_DistrictId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderAddress_DistrictId] ON [dbo].[Orders_OrderAddress]
GO
/****** Object:  Index [IX_Orders_OrderAddress_CountryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_OrderAddress_CountryId] ON [dbo].[Orders_OrderAddress]
GO
/****** Object:  Index [IX_Orders_Order_ShippingAddressId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_Order_ShippingAddressId] ON [dbo].[Orders_Order]
GO
/****** Object:  Index [IX_Orders_Order_ParentId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_Order_ParentId] ON [dbo].[Orders_Order]
GO
/****** Object:  Index [IX_Orders_Order_LatestUpdatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_Order_LatestUpdatedById] ON [dbo].[Orders_Order]
GO
/****** Object:  Index [IX_Orders_Order_CustomerId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_Order_CustomerId] ON [dbo].[Orders_Order]
GO
/****** Object:  Index [IX_Orders_Order_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_Order_CreatedById] ON [dbo].[Orders_Order]
GO
/****** Object:  Index [IX_Orders_Order_BillingAddressId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Orders_Order_BillingAddressId] ON [dbo].[Orders_Order]
GO
/****** Object:  Index [IX_News_NewsItemCategory_NewsItemId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_News_NewsItemCategory_NewsItemId] ON [dbo].[News_NewsItemCategory]
GO
/****** Object:  Index [IX_News_NewsItem_ThumbnailImageId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_News_NewsItem_ThumbnailImageId] ON [dbo].[News_NewsItem]
GO
/****** Object:  Index [IX_News_NewsItem_LatestUpdatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_News_NewsItem_LatestUpdatedById] ON [dbo].[News_NewsItem]
GO
/****** Object:  Index [IX_News_NewsItem_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_News_NewsItem_CreatedById] ON [dbo].[News_NewsItem]
GO
/****** Object:  Index [IX_Localization_Resource_CultureId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Localization_Resource_CultureId] ON [dbo].[Localization_Resource]
GO
/****** Object:  Index [IX_Localization_LocalizedContentProperty_CultureId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Localization_LocalizedContentProperty_CultureId] ON [dbo].[Localization_LocalizedContentProperty]
GO
/****** Object:  Index [IX_Inventory_Warehouse_VendorId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_Warehouse_VendorId] ON [dbo].[Inventory_Warehouse]
GO
/****** Object:  Index [IX_Inventory_Warehouse_AddressId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_Warehouse_AddressId] ON [dbo].[Inventory_Warehouse]
GO
/****** Object:  Index [IX_Inventory_StockHistory_WarehouseId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_StockHistory_WarehouseId] ON [dbo].[Inventory_StockHistory]
GO
/****** Object:  Index [IX_Inventory_StockHistory_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_StockHistory_ProductId] ON [dbo].[Inventory_StockHistory]
GO
/****** Object:  Index [IX_Inventory_StockHistory_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_StockHistory_CreatedById] ON [dbo].[Inventory_StockHistory]
GO
/****** Object:  Index [IX_Inventory_Stock_WarehouseId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_Stock_WarehouseId] ON [dbo].[Inventory_Stock]
GO
/****** Object:  Index [IX_Inventory_Stock_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Inventory_Stock_ProductId] ON [dbo].[Inventory_Stock]
GO
/****** Object:  Index [IX_Core_WidgetInstance_WidgetZoneId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_WidgetInstance_WidgetZoneId] ON [dbo].[Core_WidgetInstance]
GO
/****** Object:  Index [IX_Core_WidgetInstance_WidgetId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_WidgetInstance_WidgetId] ON [dbo].[Core_WidgetInstance]
GO
/****** Object:  Index [IX_Core_UserRole_RoleId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_UserRole_RoleId] ON [dbo].[Core_UserRole]
GO
/****** Object:  Index [IX_Core_UserLogin_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_UserLogin_UserId] ON [dbo].[Core_UserLogin]
GO
/****** Object:  Index [IX_Core_UserClaim_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_UserClaim_UserId] ON [dbo].[Core_UserClaim]
GO
/****** Object:  Index [IX_Core_UserAddress_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_UserAddress_UserId] ON [dbo].[Core_UserAddress]
GO
/****** Object:  Index [IX_Core_UserAddress_AddressId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_UserAddress_AddressId] ON [dbo].[Core_UserAddress]
GO
/****** Object:  Index [UserNameIndex]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [UserNameIndex] ON [dbo].[Core_User]
GO
/****** Object:  Index [IX_Core_User_VendorId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_User_VendorId] ON [dbo].[Core_User]
GO
/****** Object:  Index [IX_Core_User_DefaultShippingAddressId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_User_DefaultShippingAddressId] ON [dbo].[Core_User]
GO
/****** Object:  Index [IX_Core_User_DefaultBillingAddressId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_User_DefaultBillingAddressId] ON [dbo].[Core_User]
GO
/****** Object:  Index [EmailIndex]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [EmailIndex] ON [dbo].[Core_User]
GO
/****** Object:  Index [IX_Core_StateOrProvince_CountryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_StateOrProvince_CountryId] ON [dbo].[Core_StateOrProvince]
GO
/****** Object:  Index [IX_Core_RoleClaim_RoleId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_RoleClaim_RoleId] ON [dbo].[Core_RoleClaim]
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [RoleNameIndex] ON [dbo].[Core_Role]
GO
/****** Object:  Index [IX_Core_Entity_EntityTypeId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_Entity_EntityTypeId] ON [dbo].[Core_Entity]
GO
/****** Object:  Index [IX_Core_District_StateOrProvinceId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_District_StateOrProvinceId] ON [dbo].[Core_District]
GO
/****** Object:  Index [IX_Core_CustomerGroupUser_CustomerGroupId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_CustomerGroupUser_CustomerGroupId] ON [dbo].[Core_CustomerGroupUser]
GO
/****** Object:  Index [IX_Core_CustomerGroup_Name]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_CustomerGroup_Name] ON [dbo].[Core_CustomerGroup]
GO
/****** Object:  Index [IX_Core_Address_StateOrProvinceId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_Address_StateOrProvinceId] ON [dbo].[Core_Address]
GO
/****** Object:  Index [IX_Core_Address_DistrictId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_Address_DistrictId] ON [dbo].[Core_Address]
GO
/****** Object:  Index [IX_Core_Address_CountryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Core_Address_CountryId] ON [dbo].[Core_Address]
GO
/****** Object:  Index [IX_Contacts_Contact_ContactAreaId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Contacts_Contact_ContactAreaId] ON [dbo].[Contacts_Contact]
GO
/****** Object:  Index [IX_Comments_Comment_UserId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Comments_Comment_UserId] ON [dbo].[Comments_Comment]
GO
/****** Object:  Index [IX_Comments_Comment_ParentId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Comments_Comment_ParentId] ON [dbo].[Comments_Comment]
GO
/****** Object:  Index [IX_Cms_Page_LatestUpdatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Cms_Page_LatestUpdatedById] ON [dbo].[Cms_Page]
GO
/****** Object:  Index [IX_Cms_Page_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Cms_Page_CreatedById] ON [dbo].[Cms_Page]
GO
/****** Object:  Index [IX_Cms_MenuItem_ParentId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Cms_MenuItem_ParentId] ON [dbo].[Cms_MenuItem]
GO
/****** Object:  Index [IX_Cms_MenuItem_MenuId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Cms_MenuItem_MenuId] ON [dbo].[Cms_MenuItem]
GO
/****** Object:  Index [IX_Cms_MenuItem_EntityId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Cms_MenuItem_EntityId] ON [dbo].[Cms_MenuItem]
GO
/****** Object:  Index [IX_Catalog_ProductTemplateProductAttribute_ProductAttributeId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductTemplateProductAttribute_ProductAttributeId] ON [dbo].[Catalog_ProductTemplateProductAttribute]
GO
/****** Object:  Index [IX_Catalog_ProductPriceHistory_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductPriceHistory_ProductId] ON [dbo].[Catalog_ProductPriceHistory]
GO
/****** Object:  Index [IX_Catalog_ProductPriceHistory_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductPriceHistory_CreatedById] ON [dbo].[Catalog_ProductPriceHistory]
GO
/****** Object:  Index [IX_Catalog_ProductOptionValue_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductOptionValue_ProductId] ON [dbo].[Catalog_ProductOptionValue]
GO
/****** Object:  Index [IX_Catalog_ProductOptionValue_OptionId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductOptionValue_OptionId] ON [dbo].[Catalog_ProductOptionValue]
GO
/****** Object:  Index [IX_Catalog_ProductOptionCombination_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductOptionCombination_ProductId] ON [dbo].[Catalog_ProductOptionCombination]
GO
/****** Object:  Index [IX_Catalog_ProductOptionCombination_OptionId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductOptionCombination_OptionId] ON [dbo].[Catalog_ProductOptionCombination]
GO
/****** Object:  Index [IX_Catalog_ProductCategory_ProductId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductCategory_ProductId] ON [dbo].[Catalog_ProductCategory]
GO
/****** Object:  Index [IX_Catalog_ProductCategory_CategoryId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductCategory_CategoryId] ON [dbo].[Catalog_ProductCategory]
GO
/****** Object:  Index [IX_Catalog_ProductAttribute_GroupId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_ProductAttribute_GroupId] ON [dbo].[Catalog_ProductAttribute]
GO
/****** Object:  Index [IX_Catalog_Product_ThumbnailImageId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Product_ThumbnailImageId] ON [dbo].[Catalog_Product]
GO
/****** Object:  Index [IX_Catalog_Product_TaxClassId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Product_TaxClassId] ON [dbo].[Catalog_Product]
GO
/****** Object:  Index [IX_Catalog_Product_LatestUpdatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Product_LatestUpdatedById] ON [dbo].[Catalog_Product]
GO
/****** Object:  Index [IX_Catalog_Product_CreatedById]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Product_CreatedById] ON [dbo].[Catalog_Product]
GO
/****** Object:  Index [IX_Catalog_Product_BrandId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Product_BrandId] ON [dbo].[Catalog_Product]
GO
/****** Object:  Index [IX_Catalog_Category_ThumbnailImageId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Category_ThumbnailImageId] ON [dbo].[Catalog_Category]
GO
/****** Object:  Index [IX_Catalog_Category_ParentId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_Catalog_Category_ParentId] ON [dbo].[Catalog_Category]
GO
/****** Object:  Index [IX_ActivityLog_Activity_ActivityTypeId]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP INDEX [IX_ActivityLog_Activity_ActivityTypeId] ON [dbo].[ActivityLog_Activity]
GO
/****** Object:  Table [dbo].[WishList_WishListItem]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WishList_WishListItem]') AND type in (N'U'))
DROP TABLE [dbo].[WishList_WishListItem]
GO
/****** Object:  Table [dbo].[WishList_WishList]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WishList_WishList]') AND type in (N'U'))
DROP TABLE [dbo].[WishList_WishList]
GO
/****** Object:  Table [dbo].[Tax_TaxRate]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_TaxRate]') AND type in (N'U'))
DROP TABLE [dbo].[Tax_TaxRate]
GO
/****** Object:  Table [dbo].[Tax_TaxClass]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_TaxClass]') AND type in (N'U'))
DROP TABLE [dbo].[Tax_TaxClass]
GO
/****** Object:  Table [dbo].[ShoppingCart_CartItem]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingCart_CartItem]') AND type in (N'U'))
DROP TABLE [dbo].[ShoppingCart_CartItem]
GO
/****** Object:  Table [dbo].[ShoppingCart_Cart]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingCart_Cart]') AND type in (N'U'))
DROP TABLE [dbo].[ShoppingCart_Cart]
GO
/****** Object:  Table [dbo].[ShippingTableRate_PriceAndDestination]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShippingTableRate_PriceAndDestination]') AND type in (N'U'))
DROP TABLE [dbo].[ShippingTableRate_PriceAndDestination]
GO
/****** Object:  Table [dbo].[Shipping_ShippingProvider]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Shipping_ShippingProvider]') AND type in (N'U'))
DROP TABLE [dbo].[Shipping_ShippingProvider]
GO
/****** Object:  Table [dbo].[Shipments_ShipmentItem]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Shipments_ShipmentItem]') AND type in (N'U'))
DROP TABLE [dbo].[Shipments_ShipmentItem]
GO
/****** Object:  Table [dbo].[Shipments_Shipment]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Shipments_Shipment]') AND type in (N'U'))
DROP TABLE [dbo].[Shipments_Shipment]
GO
/****** Object:  Table [dbo].[Search_Query]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Search_Query]') AND type in (N'U'))
DROP TABLE [dbo].[Search_Query]
GO
/****** Object:  Table [dbo].[Reviews_Review]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reviews_Review]') AND type in (N'U'))
DROP TABLE [dbo].[Reviews_Review]
GO
/****** Object:  Table [dbo].[Reviews_Reply]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reviews_Reply]') AND type in (N'U'))
DROP TABLE [dbo].[Reviews_Reply]
GO
/****** Object:  Table [dbo].[ProductRecentlyViewed_RecentlyViewedProduct]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductRecentlyViewed_RecentlyViewedProduct]') AND type in (N'U'))
DROP TABLE [dbo].[ProductRecentlyViewed_RecentlyViewedProduct]
GO
/****** Object:  Table [dbo].[ProductComparison_ComparingProduct]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductComparison_ComparingProduct]') AND type in (N'U'))
DROP TABLE [dbo].[ProductComparison_ComparingProduct]
GO
/****** Object:  Table [dbo].[Pricing_Coupon]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_Coupon]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_Coupon]
GO
/****** Object:  Table [dbo].[Pricing_CatalogRuleCustomerGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CatalogRuleCustomerGroup]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CatalogRuleCustomerGroup]
GO
/****** Object:  Table [dbo].[Pricing_CatalogRule]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CatalogRule]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CatalogRule]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleUsage]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CartRuleUsage]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CartRuleUsage]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleProduct]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CartRuleProduct]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CartRuleProduct]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleCustomerGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CartRuleCustomerGroup]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CartRuleCustomerGroup]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CartRuleCategory]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CartRuleCategory]
GO
/****** Object:  Table [dbo].[Pricing_CartRule]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pricing_CartRule]') AND type in (N'U'))
DROP TABLE [dbo].[Pricing_CartRule]
GO
/****** Object:  Table [dbo].[Payments_PaymentProvider]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Payments_PaymentProvider]') AND type in (N'U'))
DROP TABLE [dbo].[Payments_PaymentProvider]
GO
/****** Object:  Table [dbo].[Payments_Payment]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Payments_Payment]') AND type in (N'U'))
DROP TABLE [dbo].[Payments_Payment]
GO
/****** Object:  Table [dbo].[Orders_OrderItem]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders_OrderItem]') AND type in (N'U'))
DROP TABLE [dbo].[Orders_OrderItem]
GO
/****** Object:  Table [dbo].[Orders_OrderHistory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders_OrderHistory]') AND type in (N'U'))
DROP TABLE [dbo].[Orders_OrderHistory]
GO
/****** Object:  Table [dbo].[Orders_OrderAddress]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders_OrderAddress]') AND type in (N'U'))
DROP TABLE [dbo].[Orders_OrderAddress]
GO
/****** Object:  Table [dbo].[Orders_Order]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders_Order]') AND type in (N'U'))
DROP TABLE [dbo].[Orders_Order]
GO
/****** Object:  Table [dbo].[News_NewsItemCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News_NewsItemCategory]') AND type in (N'U'))
DROP TABLE [dbo].[News_NewsItemCategory]
GO
/****** Object:  Table [dbo].[News_NewsItem]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News_NewsItem]') AND type in (N'U'))
DROP TABLE [dbo].[News_NewsItem]
GO
/****** Object:  Table [dbo].[News_NewsCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News_NewsCategory]') AND type in (N'U'))
DROP TABLE [dbo].[News_NewsCategory]
GO
/****** Object:  Table [dbo].[Localization_Resource]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Localization_Resource]') AND type in (N'U'))
DROP TABLE [dbo].[Localization_Resource]
GO
/****** Object:  Table [dbo].[Localization_LocalizedContentProperty]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Localization_LocalizedContentProperty]') AND type in (N'U'))
DROP TABLE [dbo].[Localization_LocalizedContentProperty]
GO
/****** Object:  Table [dbo].[Localization_Culture]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Localization_Culture]') AND type in (N'U'))
DROP TABLE [dbo].[Localization_Culture]
GO
/****** Object:  Table [dbo].[Inventory_Warehouse]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_Warehouse]') AND type in (N'U'))
DROP TABLE [dbo].[Inventory_Warehouse]
GO
/****** Object:  Table [dbo].[Inventory_StockHistory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_StockHistory]') AND type in (N'U'))
DROP TABLE [dbo].[Inventory_StockHistory]
GO
/****** Object:  Table [dbo].[Inventory_Stock]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_Stock]') AND type in (N'U'))
DROP TABLE [dbo].[Inventory_Stock]
GO
/****** Object:  Table [dbo].[Core_WidgetZone]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_WidgetZone]') AND type in (N'U'))
DROP TABLE [dbo].[Core_WidgetZone]
GO
/****** Object:  Table [dbo].[Core_WidgetInstance]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_WidgetInstance]') AND type in (N'U'))
DROP TABLE [dbo].[Core_WidgetInstance]
GO
/****** Object:  Table [dbo].[Core_Widget]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Widget]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Widget]
GO
/****** Object:  Table [dbo].[Core_Vendor]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Vendor]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Vendor]
GO
/****** Object:  Table [dbo].[Core_UserToken]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_UserToken]') AND type in (N'U'))
DROP TABLE [dbo].[Core_UserToken]
GO
/****** Object:  Table [dbo].[Core_UserRole]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_UserRole]') AND type in (N'U'))
DROP TABLE [dbo].[Core_UserRole]
GO
/****** Object:  Table [dbo].[Core_UserLogin]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_UserLogin]') AND type in (N'U'))
DROP TABLE [dbo].[Core_UserLogin]
GO
/****** Object:  Table [dbo].[Core_UserClaim]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_UserClaim]') AND type in (N'U'))
DROP TABLE [dbo].[Core_UserClaim]
GO
/****** Object:  Table [dbo].[Core_UserAddress]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_UserAddress]') AND type in (N'U'))
DROP TABLE [dbo].[Core_UserAddress]
GO
/****** Object:  Table [dbo].[Core_User]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_User]') AND type in (N'U'))
DROP TABLE [dbo].[Core_User]
GO
/****** Object:  Table [dbo].[Core_StateOrProvince]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_StateOrProvince]') AND type in (N'U'))
DROP TABLE [dbo].[Core_StateOrProvince]
GO
/****** Object:  Table [dbo].[Core_RoleClaim]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_RoleClaim]') AND type in (N'U'))
DROP TABLE [dbo].[Core_RoleClaim]
GO
/****** Object:  Table [dbo].[Core_Role]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Role]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Role]
GO
/****** Object:  Table [dbo].[Core_Media]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Media]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Media]
GO
/****** Object:  Table [dbo].[Core_EntityType]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_EntityType]') AND type in (N'U'))
DROP TABLE [dbo].[Core_EntityType]
GO
/****** Object:  Table [dbo].[Core_Entity]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Entity]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Entity]
GO
/****** Object:  Table [dbo].[Core_District]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_District]') AND type in (N'U'))
DROP TABLE [dbo].[Core_District]
GO
/****** Object:  Table [dbo].[Core_CustomerGroupUser]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_CustomerGroupUser]') AND type in (N'U'))
DROP TABLE [dbo].[Core_CustomerGroupUser]
GO
/****** Object:  Table [dbo].[Core_CustomerGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_CustomerGroup]') AND type in (N'U'))
DROP TABLE [dbo].[Core_CustomerGroup]
GO
/****** Object:  Table [dbo].[Core_Country]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Country]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Country]
GO
/****** Object:  Table [dbo].[Core_AppSetting]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_AppSetting]') AND type in (N'U'))
DROP TABLE [dbo].[Core_AppSetting]
GO
/****** Object:  Table [dbo].[Core_Address]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Core_Address]') AND type in (N'U'))
DROP TABLE [dbo].[Core_Address]
GO
/****** Object:  Table [dbo].[Contacts_ContactArea]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contacts_ContactArea]') AND type in (N'U'))
DROP TABLE [dbo].[Contacts_ContactArea]
GO
/****** Object:  Table [dbo].[Contacts_Contact]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contacts_Contact]') AND type in (N'U'))
DROP TABLE [dbo].[Contacts_Contact]
GO
/****** Object:  Table [dbo].[Comments_Comment]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Comments_Comment]') AND type in (N'U'))
DROP TABLE [dbo].[Comments_Comment]
GO
/****** Object:  Table [dbo].[Cms_Page]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cms_Page]') AND type in (N'U'))
DROP TABLE [dbo].[Cms_Page]
GO
/****** Object:  Table [dbo].[Cms_MenuItem]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cms_MenuItem]') AND type in (N'U'))
DROP TABLE [dbo].[Cms_MenuItem]
GO
/****** Object:  Table [dbo].[Cms_Menu]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cms_Menu]') AND type in (N'U'))
DROP TABLE [dbo].[Cms_Menu]
GO
/****** Object:  Table [dbo].[Catalog_ProductTemplateProductAttribute]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductTemplateProductAttribute]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductTemplateProductAttribute]
GO
/****** Object:  Table [dbo].[Catalog_ProductTemplate]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductTemplate]
GO
/****** Object:  Table [dbo].[Catalog_ProductPriceHistory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductPriceHistory]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductPriceHistory]
GO
/****** Object:  Table [dbo].[Catalog_ProductOptionValue]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductOptionValue]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductOptionValue]
GO
/****** Object:  Table [dbo].[Catalog_ProductOptionCombination]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductOptionCombination]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductOptionCombination]
GO
/****** Object:  Table [dbo].[Catalog_ProductOption]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductOption]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductOption]
GO
/****** Object:  Table [dbo].[Catalog_ProductMedia]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductMedia]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductMedia]
GO
/****** Object:  Table [dbo].[Catalog_ProductLink]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductLink]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductLink]
GO
/****** Object:  Table [dbo].[Catalog_ProductCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductCategory]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductCategory]
GO
/****** Object:  Table [dbo].[Catalog_ProductAttributeValue]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductAttributeValue]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductAttributeValue]
GO
/****** Object:  Table [dbo].[Catalog_ProductAttributeGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductAttributeGroup]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductAttributeGroup]
GO
/****** Object:  Table [dbo].[Catalog_ProductAttribute]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_ProductAttribute]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_ProductAttribute]
GO
/****** Object:  Table [dbo].[Catalog_Product]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_Product]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_Product]
GO
/****** Object:  Table [dbo].[Catalog_Category]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_Category]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_Category]
GO
/****** Object:  Table [dbo].[Catalog_Brand]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Catalog_Brand]') AND type in (N'U'))
DROP TABLE [dbo].[Catalog_Brand]
GO
/****** Object:  Table [dbo].[ActivityLog_ActivityType]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityLog_ActivityType]') AND type in (N'U'))
DROP TABLE [dbo].[ActivityLog_ActivityType]
GO
/****** Object:  Table [dbo].[ActivityLog_Activity]    Script Date: 29/01/2021 9:48:55 SA ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityLog_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[ActivityLog_Activity]
GO
USE [master]
GO
/****** Object:  Database [SimplCommerce]    Script Date: 29/01/2021 9:48:55 SA ******/
DROP DATABASE [SimplCommerce]
GO
/****** Object:  Database [SimplCommerce]    Script Date: 29/01/2021 9:48:55 SA ******/
CREATE DATABASE [SimplCommerce]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SimplCommerce', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\SimplCommerce.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SimplCommerce_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\SimplCommerce_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [SimplCommerce] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SimplCommerce].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SimplCommerce] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SimplCommerce] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SimplCommerce] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SimplCommerce] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SimplCommerce] SET ARITHABORT OFF 
GO
ALTER DATABASE [SimplCommerce] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SimplCommerce] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SimplCommerce] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SimplCommerce] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SimplCommerce] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SimplCommerce] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SimplCommerce] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SimplCommerce] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SimplCommerce] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SimplCommerce] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SimplCommerce] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SimplCommerce] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SimplCommerce] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SimplCommerce] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SimplCommerce] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SimplCommerce] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [SimplCommerce] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SimplCommerce] SET RECOVERY FULL 
GO
ALTER DATABASE [SimplCommerce] SET  MULTI_USER 
GO
ALTER DATABASE [SimplCommerce] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SimplCommerce] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SimplCommerce] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SimplCommerce] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SimplCommerce] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SimplCommerce] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'SimplCommerce', N'ON'
GO
ALTER DATABASE [SimplCommerce] SET QUERY_STORE = OFF
GO
USE [SimplCommerce]
GO
/****** Object:  Table [dbo].[ActivityLog_Activity]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLog_Activity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActivityTypeId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityTypeId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_ActivityLog_Activity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ActivityLog_ActivityType]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLog_ActivityType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_ActivityLog_ActivityType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_Brand]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_Brand](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Catalog_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_Category]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[MetaTitle] [nvarchar](450) NULL,
	[MetaKeywords] [nvarchar](450) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[DisplayOrder] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[IncludeInMenu] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
	[ThumbnailImageId] [bigint] NULL,
 CONSTRAINT [PK_Catalog_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_Product]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[MetaTitle] [nvarchar](450) NULL,
	[MetaKeywords] [nvarchar](450) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[PublishedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedById] [bigint] NOT NULL,
	[ShortDescription] [nvarchar](450) NULL,
	[Description] [nvarchar](max) NULL,
	[Specification] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[OldPrice] [decimal](18, 2) NULL,
	[SpecialPrice] [decimal](18, 2) NULL,
	[SpecialPriceStart] [datetimeoffset](7) NULL,
	[SpecialPriceEnd] [datetimeoffset](7) NULL,
	[HasOptions] [bit] NOT NULL,
	[IsVisibleIndividually] [bit] NOT NULL,
	[IsFeatured] [bit] NOT NULL,
	[IsCallForPricing] [bit] NOT NULL,
	[IsAllowToOrder] [bit] NOT NULL,
	[StockTrackingIsEnabled] [bit] NOT NULL,
	[StockQuantity] [int] NOT NULL,
	[Sku] [nvarchar](450) NULL,
	[Gtin] [nvarchar](450) NULL,
	[NormalizedName] [nvarchar](450) NULL,
	[DisplayOrder] [int] NOT NULL,
	[VendorId] [bigint] NULL,
	[ThumbnailImageId] [bigint] NULL,
	[ReviewsCount] [int] NOT NULL,
	[RatingAverage] [float] NULL,
	[BrandId] [bigint] NULL,
	[TaxClassId] [bigint] NULL,
 CONSTRAINT [PK_Catalog_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductAttribute]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductAttribute](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[GroupId] [bigint] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductAttribute] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductAttributeGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductAttributeGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Catalog_ProductAttributeGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductAttributeValue]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductAttributeValue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AttributeId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Catalog_ProductAttributeValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductCategory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IsFeaturedProduct] [bit] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductLink]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductLink](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[LinkedProductId] [bigint] NOT NULL,
	[LinkType] [int] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductLink] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductMedia]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductMedia](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[MediaId] [bigint] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductMedia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductOption]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductOption](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Catalog_ProductOption] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductOptionCombination]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductOptionCombination](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[OptionId] [bigint] NOT NULL,
	[Value] [nvarchar](450) NULL,
	[SortIndex] [int] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductOptionCombination] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductOptionValue]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductOptionValue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OptionId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Value] [nvarchar](450) NULL,
	[DisplayType] [nvarchar](450) NULL,
	[SortIndex] [int] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductOptionValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductPriceHistory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductPriceHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[Price] [decimal](18, 2) NULL,
	[OldPrice] [decimal](18, 2) NULL,
	[SpecialPrice] [decimal](18, 2) NULL,
	[SpecialPriceStart] [datetimeoffset](7) NULL,
	[SpecialPriceEnd] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_Catalog_ProductPriceHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductTemplate]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductTemplate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Catalog_ProductTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catalog_ProductTemplateProductAttribute]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalog_ProductTemplateProductAttribute](
	[ProductTemplateId] [bigint] NOT NULL,
	[ProductAttributeId] [bigint] NOT NULL,
 CONSTRAINT [PK_Catalog_ProductTemplateProductAttribute] PRIMARY KEY CLUSTERED 
(
	[ProductTemplateId] ASC,
	[ProductAttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cms_Menu]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Menu](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[IsSystem] [bit] NOT NULL,
 CONSTRAINT [PK_Cms_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cms_MenuItem]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_MenuItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentId] [bigint] NULL,
	[MenuId] [bigint] NOT NULL,
	[EntityId] [bigint] NULL,
	[CustomLink] [nvarchar](450) NULL,
	[Name] [nvarchar](450) NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_Cms_MenuItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cms_Page]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cms_Page](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[MetaTitle] [nvarchar](450) NULL,
	[MetaKeywords] [nvarchar](450) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[PublishedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedById] [bigint] NOT NULL,
	[Body] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cms_Page] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments_Comment]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments_Comment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[CommentText] [nvarchar](max) NULL,
	[CommenterName] [nvarchar](450) NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[EntityTypeId] [nvarchar](450) NULL,
	[EntityId] [bigint] NOT NULL,
	[ParentId] [bigint] NULL,
 CONSTRAINT [PK_Comments_Comment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts_Contact]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts_Contact](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](450) NULL,
	[PhoneNumber] [nvarchar](450) NULL,
	[EmailAddress] [nvarchar](450) NULL,
	[Address] [nvarchar](450) NULL,
	[Content] [nvarchar](max) NULL,
	[ContactAreaId] [bigint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Contacts_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts_ContactArea]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts_ContactArea](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Contacts_ContactArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Address]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Address](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactName] [nvarchar](450) NULL,
	[Phone] [nvarchar](450) NULL,
	[AddressLine1] [nvarchar](450) NULL,
	[AddressLine2] [nvarchar](450) NULL,
	[City] [nvarchar](450) NULL,
	[ZipCode] [nvarchar](450) NULL,
	[DistrictId] [bigint] NULL,
	[StateOrProvinceId] [bigint] NOT NULL,
	[CountryId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Core_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_AppSetting]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_AppSetting](
	[Id] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](450) NULL,
	[Module] [nvarchar](450) NULL,
	[IsVisibleInCommonSettingPage] [bit] NOT NULL,
 CONSTRAINT [PK_Core_AppSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Country]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Country](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Code3] [nvarchar](450) NULL,
	[IsBillingEnabled] [bit] NOT NULL,
	[IsShippingEnabled] [bit] NOT NULL,
	[IsCityEnabled] [bit] NOT NULL,
	[IsZipCodeEnabled] [bit] NOT NULL,
	[IsDistrictEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_Core_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_CustomerGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_CustomerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Core_CustomerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_CustomerGroupUser]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_CustomerGroupUser](
	[UserId] [bigint] NOT NULL,
	[CustomerGroupId] [bigint] NOT NULL,
 CONSTRAINT [PK_Core_CustomerGroupUser] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_District]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_District](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateOrProvinceId] [bigint] NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Type] [nvarchar](450) NULL,
	[Location] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Entity]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Entity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityTypeId] [nvarchar](450) NULL,
 CONSTRAINT [PK_Core_Entity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_EntityType]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_EntityType](
	[Id] [nvarchar](450) NOT NULL,
	[IsMenuable] [bit] NOT NULL,
	[AreaName] [nvarchar](450) NULL,
	[RoutingController] [nvarchar](450) NULL,
	[RoutingAction] [nvarchar](450) NULL,
 CONSTRAINT [PK_Core_EntityType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Media]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Media](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Caption] [nvarchar](450) NULL,
	[FileSize] [int] NOT NULL,
	[FileName] [nvarchar](450) NULL,
	[MediaType] [int] NOT NULL,
 CONSTRAINT [PK_Core_Media] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Role]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Role](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_RoleClaim]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_RoleClaim](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_RoleClaim] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_StateOrProvince]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_StateOrProvince](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CountryId] [nvarchar](450) NULL,
	[Code] [nvarchar](450) NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Type] [nvarchar](450) NULL,
 CONSTRAINT [PK_Core_StateOrProvince] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_User]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserGuid] [uniqueidentifier] NOT NULL,
	[FullName] [nvarchar](450) NOT NULL,
	[VendorId] [bigint] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[DefaultShippingAddressId] [bigint] NULL,
	[DefaultBillingAddressId] [bigint] NULL,
	[RefreshTokenHash] [nvarchar](450) NULL,
	[Culture] [nvarchar](450) NULL,
	[ExtensionData] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_UserAddress]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_UserAddress](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[AddressId] [bigint] NOT NULL,
	[AddressType] [int] NOT NULL,
	[LastUsedOn] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_Core_UserAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_UserClaim]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_UserClaim](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_UserClaim] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_UserLogin]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_UserLogin](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_Core_UserLogin] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_UserRole]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_UserRole](
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
 CONSTRAINT [PK_Core_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_UserToken]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_UserToken](
	[UserId] [bigint] NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_UserToken] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Vendor]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Vendor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Core_Vendor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_Widget]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_Widget](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[ViewComponentName] [nvarchar](450) NULL,
	[CreateUrl] [nvarchar](450) NULL,
	[EditUrl] [nvarchar](450) NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[IsPublished] [bit] NOT NULL,
 CONSTRAINT [PK_Core_Widget] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_WidgetInstance]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_WidgetInstance](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[PublishStart] [datetimeoffset](7) NULL,
	[PublishEnd] [datetimeoffset](7) NULL,
	[WidgetId] [nvarchar](450) NULL,
	[WidgetZoneId] [bigint] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[Data] [nvarchar](max) NULL,
	[HtmlData] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_WidgetInstance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Core_WidgetZone]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Core_WidgetZone](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Core_WidgetZone] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_Stock]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_Stock](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[WarehouseId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[ReservedQuantity] [int] NOT NULL,
 CONSTRAINT [PK_Inventory_Stock] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_StockHistory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_StockHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[WarehouseId] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[CreatedById] [bigint] NOT NULL,
	[AdjustedQuantity] [bigint] NOT NULL,
	[Note] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Inventory_StockHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_Warehouse]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_Warehouse](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[AddressId] [bigint] NOT NULL,
	[VendorId] [bigint] NULL,
 CONSTRAINT [PK_Inventory_Warehouse] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Localization_Culture]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Localization_Culture](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Localization_Culture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Localization_LocalizedContentProperty]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Localization_LocalizedContentProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EntityId] [bigint] NOT NULL,
	[EntityType] [nvarchar](450) NULL,
	[CultureId] [nvarchar](450) NOT NULL,
	[ProperyName] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Localization_LocalizedContentProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Localization_Resource]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Localization_Resource](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[CultureId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Localization_Resource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News_NewsCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News_NewsCategory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[MetaTitle] [nvarchar](450) NULL,
	[MetaKeywords] [nvarchar](450) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[DisplayOrder] [int] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_News_NewsCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News_NewsItem]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News_NewsItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Slug] [nvarchar](450) NOT NULL,
	[MetaTitle] [nvarchar](450) NULL,
	[MetaKeywords] [nvarchar](450) NULL,
	[MetaDescription] [nvarchar](max) NULL,
	[IsPublished] [bit] NOT NULL,
	[PublishedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedById] [bigint] NOT NULL,
	[ShortContent] [nvarchar](450) NULL,
	[FullContent] [nvarchar](max) NULL,
	[ThumbnailImageId] [bigint] NULL,
 CONSTRAINT [PK_News_NewsItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News_NewsItemCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News_NewsItemCategory](
	[CategoryId] [bigint] NOT NULL,
	[NewsItemId] [bigint] NOT NULL,
 CONSTRAINT [PK_News_NewsItemCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC,
	[NewsItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders_Order]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders_Order](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[CreatedById] [bigint] NOT NULL,
	[VendorId] [bigint] NULL,
	[CouponCode] [nvarchar](450) NULL,
	[CouponRuleName] [nvarchar](450) NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[SubTotalWithDiscount] [decimal](18, 2) NOT NULL,
	[ShippingAddressId] [bigint] NOT NULL,
	[BillingAddressId] [bigint] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[OrderNote] [nvarchar](1000) NULL,
	[ParentId] [bigint] NULL,
	[IsMasterOrder] [bit] NOT NULL,
	[ShippingMethod] [nvarchar](450) NULL,
	[ShippingFeeAmount] [decimal](18, 2) NOT NULL,
	[TaxAmount] [decimal](18, 2) NOT NULL,
	[OrderTotal] [decimal](18, 2) NOT NULL,
	[PaymentMethod] [nvarchar](450) NULL,
	[PaymentFeeAmount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Orders_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders_OrderAddress]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders_OrderAddress](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactName] [nvarchar](450) NULL,
	[Phone] [nvarchar](450) NULL,
	[AddressLine1] [nvarchar](450) NULL,
	[AddressLine2] [nvarchar](450) NULL,
	[City] [nvarchar](450) NULL,
	[ZipCode] [nvarchar](450) NULL,
	[DistrictId] [bigint] NULL,
	[StateOrProvinceId] [bigint] NOT NULL,
	[CountryId] [nvarchar](450) NULL,
 CONSTRAINT [PK_Orders_OrderAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders_OrderHistory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders_OrderHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[OldStatus] [int] NULL,
	[NewStatus] [int] NOT NULL,
	[OrderSnapshot] [nvarchar](max) NULL,
	[Note] [nvarchar](1000) NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[CreatedById] [bigint] NOT NULL,
 CONSTRAINT [PK_Orders_OrderHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders_OrderItem]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders_OrderItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NULL,
	[ProductId] [bigint] NOT NULL,
	[ProductPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[TaxAmount] [decimal](18, 2) NOT NULL,
	[TaxPercent] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Orders_OrderItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payments_Payment]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments_Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[PaymentFee] [decimal](18, 2) NOT NULL,
	[PaymentMethod] [nvarchar](450) NULL,
	[GatewayTransactionId] [nvarchar](450) NULL,
	[Status] [int] NOT NULL,
	[FailureMessage] [nvarchar](max) NULL,
 CONSTRAINT [PK_Payments_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payments_PaymentProvider]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments_PaymentProvider](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[ConfigureUrl] [nvarchar](450) NULL,
	[LandingViewComponentName] [nvarchar](450) NULL,
	[AdditionalSettings] [nvarchar](max) NULL,
 CONSTRAINT [PK_Payments_PaymentProvider] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CartRule]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CartRule](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[StartOn] [datetimeoffset](7) NULL,
	[EndOn] [datetimeoffset](7) NULL,
	[IsCouponRequired] [bit] NOT NULL,
	[RuleToApply] [nvarchar](450) NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[MaxDiscountAmount] [decimal](18, 2) NULL,
	[DiscountStep] [int] NULL,
	[UsageLimitPerCoupon] [int] NULL,
	[UsageLimitPerCustomer] [int] NULL,
 CONSTRAINT [PK_Pricing_CartRule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleCategory]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CartRuleCategory](
	[CategoryId] [bigint] NOT NULL,
	[CartRuleId] [bigint] NOT NULL,
 CONSTRAINT [PK_Pricing_CartRuleCategory] PRIMARY KEY CLUSTERED 
(
	[CartRuleId] ASC,
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleCustomerGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CartRuleCustomerGroup](
	[CartRuleId] [bigint] NOT NULL,
	[CustomerGroupId] [bigint] NOT NULL,
 CONSTRAINT [PK_Pricing_CartRuleCustomerGroup] PRIMARY KEY CLUSTERED 
(
	[CartRuleId] ASC,
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleProduct]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CartRuleProduct](
	[ProductId] [bigint] NOT NULL,
	[CartRuleId] [bigint] NOT NULL,
 CONSTRAINT [PK_Pricing_CartRuleProduct] PRIMARY KEY CLUSTERED 
(
	[CartRuleId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CartRuleUsage]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CartRuleUsage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CartRuleId] [bigint] NOT NULL,
	[CouponId] [bigint] NULL,
	[UserId] [bigint] NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Pricing_CartRuleUsage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CatalogRule]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CatalogRule](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[StartOn] [datetimeoffset](7) NULL,
	[EndOn] [datetimeoffset](7) NULL,
	[RuleToApply] [nvarchar](450) NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[MaxDiscountAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Pricing_CatalogRule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_CatalogRuleCustomerGroup]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_CatalogRuleCustomerGroup](
	[CatalogRuleId] [bigint] NOT NULL,
	[CustomerGroupId] [bigint] NOT NULL,
 CONSTRAINT [PK_Pricing_CatalogRuleCustomerGroup] PRIMARY KEY CLUSTERED 
(
	[CatalogRuleId] ASC,
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pricing_Coupon]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pricing_Coupon](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CartRuleId] [bigint] NOT NULL,
	[Code] [nvarchar](450) NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Pricing_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductComparison_ComparingProduct]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductComparison_ComparingProduct](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
 CONSTRAINT [PK_ProductComparison_ComparingProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductRecentlyViewed_RecentlyViewedProduct]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductRecentlyViewed_RecentlyViewedProduct](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[LatestViewedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_ProductRecentlyViewed_RecentlyViewedProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reviews_Reply]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reviews_Reply](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ReviewId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[ReplierName] [nvarchar](450) NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Reviews_Reply] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reviews_Review]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reviews_Review](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Title] [nvarchar](450) NULL,
	[Comment] [nvarchar](max) NULL,
	[Rating] [int] NOT NULL,
	[ReviewerName] [nvarchar](450) NULL,
	[Status] [int] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[EntityTypeId] [nvarchar](450) NULL,
	[EntityId] [bigint] NOT NULL,
 CONSTRAINT [PK_Reviews_Review] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Search_Query]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Search_Query](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[QueryText] [nvarchar](500) NOT NULL,
	[ResultsCount] [int] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Search_Query] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipments_Shipment]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipments_Shipment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[TrackingNumber] [nvarchar](450) NULL,
	[WarehouseId] [bigint] NOT NULL,
	[VendorId] [bigint] NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_Shipments_Shipment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipments_ShipmentItem]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipments_ShipmentItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ShipmentId] [bigint] NOT NULL,
	[OrderItemId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Shipments_ShipmentItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipping_ShippingProvider]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipping_ShippingProvider](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[ConfigureUrl] [nvarchar](450) NULL,
	[ToAllShippingEnabledCountries] [bit] NOT NULL,
	[OnlyCountryIdsString] [nvarchar](1000) NULL,
	[ToAllShippingEnabledStatesOrProvinces] [bit] NOT NULL,
	[OnlyStateOrProvinceIdsString] [nvarchar](1000) NULL,
	[AdditionalSettings] [nvarchar](max) NULL,
	[ShippingPriceServiceTypeName] [nvarchar](450) NULL,
 CONSTRAINT [PK_Shipping_ShippingProvider] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShippingTableRate_PriceAndDestination]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShippingTableRate_PriceAndDestination](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CountryId] [nvarchar](450) NULL,
	[StateOrProvinceId] [bigint] NULL,
	[DistrictId] [bigint] NULL,
	[ZipCode] [nvarchar](450) NULL,
	[Note] [nvarchar](max) NULL,
	[MinOrderSubtotal] [decimal](18, 2) NOT NULL,
	[ShippingPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ShippingTableRate_PriceAndDestination] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShoppingCart_Cart]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShoppingCart_Cart](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[CreatedById] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CouponCode] [nvarchar](450) NULL,
	[CouponRuleName] [nvarchar](450) NULL,
	[ShippingMethod] [nvarchar](450) NULL,
	[IsProductPriceIncludeTax] [bit] NOT NULL,
	[ShippingAmount] [decimal](18, 2) NULL,
	[TaxAmount] [decimal](18, 2) NULL,
	[ShippingData] [nvarchar](max) NULL,
	[OrderNote] [nvarchar](1000) NULL,
	[LockedOnCheckout] [bit] NOT NULL,
 CONSTRAINT [PK_ShoppingCart_Cart] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShoppingCart_CartItem]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShoppingCart_CartItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[CartId] [bigint] NOT NULL,
 CONSTRAINT [PK_ShoppingCart_CartItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tax_TaxClass]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tax_TaxClass](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_Tax_TaxClass] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tax_TaxRate]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tax_TaxRate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TaxClassId] [bigint] NOT NULL,
	[CountryId] [nvarchar](450) NULL,
	[StateOrProvinceId] [bigint] NULL,
	[Rate] [decimal](18, 2) NOT NULL,
	[ZipCode] [nvarchar](450) NULL,
 CONSTRAINT [PK_Tax_TaxRate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WishList_WishList]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WishList_WishList](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[SharingCode] [nvarchar](450) NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_WishList_WishList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WishList_WishListItem]    Script Date: 29/01/2021 9:48:55 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WishList_WishListItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WishListId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Quantity] [int] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[LatestUpdatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_WishList_WishListItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ActivityLog_Activity] ON 

INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (1, 1, 18, CAST(N'2020-11-22T09:30:07.5025422+07:00' AS DateTimeOffset), 22, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (2, 1, 29, CAST(N'2020-11-22T09:30:37.7382024+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (3, 1, 285, CAST(N'2020-11-25T01:32:55.1292698+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (4, 1, 294, CAST(N'2020-11-25T23:10:36.9213703+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (5, 1, 10, CAST(N'2020-11-25T23:41:21.8722340+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (6, 1, 10, CAST(N'2020-11-25T23:46:13.7242971+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (7, 1, 35, CAST(N'2020-11-28T23:16:32.1821061+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (8, 1, 10, CAST(N'2020-11-29T22:29:06.4770328+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (9, 1, 10, CAST(N'2020-11-29T22:40:35.5879690+07:00' AS DateTimeOffset), 1, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (10, 1, 10, CAST(N'2020-11-29T22:41:13.4019932+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (11, 1, 10, CAST(N'2020-11-29T22:41:57.0397569+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (12, 1, 10, CAST(N'2020-11-29T22:43:57.2852356+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (13, 1, 10, CAST(N'2020-11-29T22:44:01.9152711+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (14, 1, 54, CAST(N'2020-11-30T22:04:13.9337667+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (15, 1, 92, CAST(N'2020-11-30T22:06:48.7135097+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (16, 1, 99, CAST(N'2020-11-30T22:06:55.9492449+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (17, 1, 488, CAST(N'2020-12-09T01:19:56.6287385+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (18, 1, 10, CAST(N'2020-12-09T01:20:46.7794255+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (19, 1, 10, CAST(N'2020-12-09T01:21:06.7911870+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (20, 1, 10, CAST(N'2020-12-09T01:21:18.6937691+07:00' AS DateTimeOffset), 1, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (21, 1, 10, CAST(N'2020-12-09T01:21:29.0747495+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (22, 1, 10, CAST(N'2020-12-09T16:08:05.4669265+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (23, 1, 0, CAST(N'2020-12-11T17:52:51.6486986+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (24, 1, 0, CAST(N'2020-12-14T23:34:58.6033672+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (25, 1, 0, CAST(N'2020-12-14T23:36:51.7208982+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (26, 1, 0, CAST(N'2020-12-14T23:38:16.2665071+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (27, 1, 0, CAST(N'2020-12-14T23:38:26.1641736+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (28, 1, 10, CAST(N'2020-12-14T23:41:45.4292854+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (29, 1, 10, CAST(N'2020-12-14T23:41:55.4802288+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (30, 1, 10, CAST(N'2020-12-14T23:42:24.7793486+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (31, 1, 10, CAST(N'2020-12-14T23:43:05.8706169+07:00' AS DateTimeOffset), 1, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (32, 1, 10, CAST(N'2020-12-14T23:44:55.9881115+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (33, 1, 10, CAST(N'2020-12-14T23:48:34.4780158+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (34, 1, 10, CAST(N'2020-12-14T23:48:45.8958260+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (35, 1, 10, CAST(N'2020-12-14T23:50:56.5316251+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (36, 1, 10, CAST(N'2020-12-14T23:51:44.4211848+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (37, 1, 10, CAST(N'2020-12-14T23:52:28.3852960+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (38, 1, 10, CAST(N'2020-12-14T23:56:06.2990651+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (39, 1, 10, CAST(N'2020-12-15T00:21:01.2863614+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (40, 1, 10, CAST(N'2020-12-15T00:21:46.9376275+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (41, 1, 10, CAST(N'2020-12-15T00:21:55.7595540+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (42, 1, 10, CAST(N'2020-12-18T21:18:12.2829095+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (43, 1, 10, CAST(N'2020-12-29T21:14:26.3791105+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (44, 1, 10, CAST(N'2020-12-29T21:16:28.8102844+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (45, 1, 10, CAST(N'2020-12-29T21:17:01.4681959+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (46, 1, 10, CAST(N'2020-12-29T21:19:50.4618628+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (47, 1, 10, CAST(N'2020-12-29T21:20:49.3605613+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (48, 1, 10, CAST(N'2020-12-29T21:21:27.3648234+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (49, 1, 10, CAST(N'2020-12-29T21:22:38.5024590+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (50, 1, 10, CAST(N'2020-12-29T21:23:45.7673679+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (51, 1, 10, CAST(N'2020-12-29T21:25:48.1393473+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (52, 1, 10, CAST(N'2020-12-29T21:27:30.8263295+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (53, 1, 10, CAST(N'2020-12-29T21:27:35.2942921+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (54, 1, 10, CAST(N'2020-12-29T21:27:44.0876138+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (55, 1, 10, CAST(N'2020-12-29T21:27:51.7451698+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (56, 1, 10, CAST(N'2020-12-29T21:43:57.7054196+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (57, 1, 10, CAST(N'2020-12-29T21:44:00.5710505+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (58, 1, 10, CAST(N'2020-12-29T21:45:17.1892872+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (59, 1, 10, CAST(N'2020-12-29T21:53:10.9235916+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (60, 1, 10, CAST(N'2020-12-29T22:41:35.0007157+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (61, 1, 10, CAST(N'2020-12-29T23:43:08.9686652+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (62, 1, 10, CAST(N'2020-12-29T23:44:20.7973854+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (63, 1, 10, CAST(N'2020-12-29T23:55:50.2118824+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (64, 1, 10, CAST(N'2020-12-30T00:07:40.1845701+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (65, 1, 10, CAST(N'2020-12-30T00:08:48.5908327+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (66, 1, 0, CAST(N'2021-01-05T10:06:16.1685844+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (67, 1, 0, CAST(N'2021-01-05T10:37:45.4424180+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (68, 1, 10, CAST(N'2021-01-05T10:40:03.6490054+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (69, 1, 10, CAST(N'2021-01-05T10:43:01.8351722+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (70, 1, 10, CAST(N'2021-01-05T10:43:11.4297062+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (71, 1, 10, CAST(N'2021-01-05T10:43:24.2185098+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (72, 1, 10, CAST(N'2021-01-05T10:45:21.8471269+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (73, 1, 10, CAST(N'2021-01-05T10:45:25.9109629+07:00' AS DateTimeOffset), 14, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (74, 1, 10, CAST(N'2021-01-05T10:45:31.2760189+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (75, 1, 10, CAST(N'2021-01-05T10:54:07.3134402+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (76, 1, 10, CAST(N'2021-01-05T10:55:22.2606809+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (77, 1, 10, CAST(N'2021-01-05T10:58:19.2616974+07:00' AS DateTimeOffset), 9, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (78, 1, 10, CAST(N'2021-01-05T11:00:48.0567183+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (79, 1, 10, CAST(N'2021-01-05T11:02:20.2149167+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (80, 1, 10, CAST(N'2021-01-05T11:03:39.2310285+07:00' AS DateTimeOffset), 5, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (81, 1, 10, CAST(N'2021-01-05T11:13:06.7568984+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (82, 1, 10, CAST(N'2021-01-05T11:13:12.1786258+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (83, 1, 10, CAST(N'2021-01-05T11:16:05.9748001+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (84, 1, 10, CAST(N'2021-01-05T11:26:24.4766529+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (85, 1, 10, CAST(N'2021-01-05T11:32:05.0441429+07:00' AS DateTimeOffset), 17, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (86, 1, 10, CAST(N'2021-01-05T11:32:26.5074148+07:00' AS DateTimeOffset), 17, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (87, 1, 10, CAST(N'2021-01-05T11:40:55.2344173+07:00' AS DateTimeOffset), 17, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (88, 1, 10, CAST(N'2021-01-05T11:41:00.9793554+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (89, 1, 10, CAST(N'2021-01-05T11:41:38.9646104+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (90, 1, 10, CAST(N'2021-01-05T11:42:06.6258172+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (91, 1, 10, CAST(N'2021-01-05T11:42:15.6059459+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (92, 1, 10, CAST(N'2021-01-05T11:47:54.8463211+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (93, 1, 10, CAST(N'2021-01-05T12:28:55.0126817+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (94, 1, 10, CAST(N'2021-01-05T12:35:37.8400935+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (95, 1, 0, CAST(N'2021-01-05T21:21:12.5765316+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (96, 1, 10, CAST(N'2021-01-05T21:37:42.6534025+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (97, 1, 0, CAST(N'2021-01-06T23:34:41.8966148+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (98, 1, 554, CAST(N'2021-01-15T10:28:21.9325961+07:00' AS DateTimeOffset), 17, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (99, 1, 554, CAST(N'2021-01-15T10:28:57.8641506+07:00' AS DateTimeOffset), 16, N'Product')
GO
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (100, 1, 554, CAST(N'2021-01-15T10:30:37.8051005+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (101, 1, 554, CAST(N'2021-01-15T10:31:01.4380340+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (102, 1, 554, CAST(N'2021-01-15T10:31:17.4115915+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (103, 1, 554, CAST(N'2021-01-15T10:31:57.0435279+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (104, 1, 554, CAST(N'2021-01-15T10:32:08.6064722+07:00' AS DateTimeOffset), 16, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (105, 1, 554, CAST(N'2021-01-15T10:32:41.7476909+07:00' AS DateTimeOffset), 16, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (106, 1, 554, CAST(N'2021-01-15T10:33:02.6415761+07:00' AS DateTimeOffset), 16, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (107, 1, 0, CAST(N'2021-01-27T01:37:17.3991745+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (108, 1, 0, CAST(N'2021-01-27T21:34:52.0652020+07:00' AS DateTimeOffset), 15, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (109, 1, 0, CAST(N'2021-01-27T21:35:08.6204597+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (110, 1, 0, CAST(N'2021-01-27T21:35:14.2862751+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (111, 1, 0, CAST(N'2021-01-27T21:41:12.4157096+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (112, 1, 0, CAST(N'2021-01-27T21:48:40.6745859+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (113, 1, 0, CAST(N'2021-01-27T21:53:43.7252395+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (114, 1, 0, CAST(N'2021-01-27T21:59:16.5623229+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (115, 1, 0, CAST(N'2021-01-27T22:01:51.7062754+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (116, 1, 0, CAST(N'2021-01-27T22:04:13.7233233+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (117, 1, 0, CAST(N'2021-01-27T22:04:27.8173827+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (118, 1, 0, CAST(N'2021-01-27T22:04:36.3006750+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (119, 1, 0, CAST(N'2021-01-27T22:04:43.2472318+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (120, 1, 0, CAST(N'2021-01-27T22:08:44.9228327+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (121, 1, 10, CAST(N'2021-01-27T22:11:34.0964161+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (122, 1, 10, CAST(N'2021-01-27T22:12:45.1893884+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (123, 1, 10, CAST(N'2021-01-27T22:12:55.7665712+07:00' AS DateTimeOffset), 17, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (124, 1, 10, CAST(N'2021-01-27T22:23:55.6652248+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (125, 1, 10, CAST(N'2021-01-27T22:24:02.9382808+07:00' AS DateTimeOffset), 17, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (126, 1, 0, CAST(N'2021-01-27T22:25:59.9091523+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (127, 1, 0, CAST(N'2021-01-27T22:54:14.3966720+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (128, 1, 0, CAST(N'2021-01-27T22:56:40.8783728+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (129, 1, 0, CAST(N'2021-01-27T22:57:51.7652088+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (130, 1, 0, CAST(N'2021-01-27T22:59:33.7243864+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (131, 1, 0, CAST(N'2021-01-27T23:01:17.2856932+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (132, 1, 0, CAST(N'2021-01-27T23:03:54.9244165+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (133, 1, 0, CAST(N'2021-01-27T23:08:42.6058110+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (134, 1, 10, CAST(N'2021-01-27T23:09:05.1951636+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (135, 1, 10, CAST(N'2021-01-27T23:09:12.0416404+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (136, 1, 10, CAST(N'2021-01-27T23:09:33.3837645+07:00' AS DateTimeOffset), 18, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (137, 1, 0, CAST(N'2021-01-28T14:12:04.3885765+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (138, 1, 0, CAST(N'2021-01-28T14:12:13.6991653+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (139, 1, 0, CAST(N'2021-01-28T14:12:23.3709051+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (140, 1, 0, CAST(N'2021-01-28T14:24:30.4146610+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (141, 1, 0, CAST(N'2021-01-28T14:25:30.7812746+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (142, 1, 556, CAST(N'2021-01-28T14:37:44.9533906+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (143, 1, 556, CAST(N'2021-01-28T14:37:53.6621093+07:00' AS DateTimeOffset), 20, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (144, 1, 556, CAST(N'2021-01-28T14:38:23.5216502+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (145, 1, 556, CAST(N'2021-01-28T14:38:45.0397726+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (146, 1, 556, CAST(N'2021-01-28T14:40:18.1701745+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (147, 1, 556, CAST(N'2021-01-28T14:40:29.5427836+07:00' AS DateTimeOffset), 19, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (148, 1, 10, CAST(N'2021-01-28T14:43:43.6946286+07:00' AS DateTimeOffset), 22, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (149, 1, 10, CAST(N'2021-01-28T14:44:24.0410085+07:00' AS DateTimeOffset), 21, N'Product')
INSERT [dbo].[ActivityLog_Activity] ([Id], [ActivityTypeId], [UserId], [CreatedOn], [EntityId], [EntityTypeId]) VALUES (150, 1, 10, CAST(N'2021-01-28T14:48:13.9103447+07:00' AS DateTimeOffset), 22, N'Product')
SET IDENTITY_INSERT [dbo].[ActivityLog_Activity] OFF
GO
SET IDENTITY_INSERT [dbo].[ActivityLog_ActivityType] ON 

INSERT [dbo].[ActivityLog_ActivityType] ([Id], [Name]) VALUES (1, N'EntityView')
SET IDENTITY_INSERT [dbo].[ActivityLog_ActivityType] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_Brand] ON 

INSERT [dbo].[Catalog_Brand] ([Id], [Name], [Slug], [Description], [IsPublished], [IsDeleted]) VALUES (1, N'Nike', N'nike', NULL, 1, 0)
INSERT [dbo].[Catalog_Brand] ([Id], [Name], [Slug], [Description], [IsPublished], [IsDeleted]) VALUES (2, N'Adidas', N'adidas', NULL, 1, 0)
INSERT [dbo].[Catalog_Brand] ([Id], [Name], [Slug], [Description], [IsPublished], [IsDeleted]) VALUES (3, N'Puma', N'puma', NULL, 1, 0)
INSERT [dbo].[Catalog_Brand] ([Id], [Name], [Slug], [Description], [IsPublished], [IsDeleted]) VALUES (4, N'Converse', N'converse', NULL, 1, 0)
INSERT [dbo].[Catalog_Brand] ([Id], [Name], [Slug], [Description], [IsPublished], [IsDeleted]) VALUES (7, N'Vans', N'vans', NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[Catalog_Brand] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_Category] ON 

INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (1, N'Nam', N'nam', NULL, NULL, NULL, NULL, 0, 1, 1, 0, NULL, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (2, N'Tennis', N'tennis', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 1, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (3, N'Bóng đá', N'bong-da-2', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 1, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (4, N'Nữ', N'nu', NULL, NULL, NULL, NULL, 0, 1, 1, 0, NULL, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (5, N'Phòng tập thể dục và rèn luyện', N'phong-tap-the-duc-va-ren-luyen', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 4, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (6, N'Đi bộ', N'di-bo', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 4, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (7, N'Trẻ em', N'tre-em', NULL, NULL, NULL, NULL, 0, 1, 1, 0, NULL, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (8, N'Young Kids', N'young-kids', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 7, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (9, N'Older Kids', N'older-kids', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 7, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (10, N'Khác', N'khac', NULL, NULL, NULL, NULL, 0, 1, 1, 0, NULL, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (11, N'Chạy bộ', N'chay-bo', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 10, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (12, N'Bóng đá', N'bong-da', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 10, NULL)
INSERT [dbo].[Catalog_Category] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [Description], [DisplayOrder], [IsPublished], [IncludeInMenu], [IsDeleted], [ParentId], [ThumbnailImageId]) VALUES (13, N'Tự do', N'tu-do', NULL, NULL, NULL, NULL, 0, 1, 1, 0, 10, NULL)
SET IDENTITY_INSERT [dbo].[Catalog_Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_Product] ON 

INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (15, N'AIR MAX 90 CITY PACK', N'air-max-90-city-pack', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-05T11:12:09.2621249+07:00' AS DateTimeOffset), CAST(N'2021-01-05T11:12:09.2621277+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(4500000.00 AS Decimal(18, 2)), CAST(5000000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 100, NULL, NULL, NULL, 0, NULL, 53, 0, NULL, 1, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (16, N'AIR MAX 270 REACT', N'air-max-270-react', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-05T11:18:54.4637272+07:00' AS DateTimeOffset), CAST(N'2021-01-05T11:18:54.4637295+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(5800000.00 AS Decimal(18, 2)), CAST(6000000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 100, NULL, NULL, NULL, 0, NULL, 58, 0, NULL, 1, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (17, N'ZX 2K BOOST', N'zx-2k-boost', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-05T11:29:27.4615512+07:00' AS DateTimeOffset), CAST(N'2021-01-05T11:29:27.4615535+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(2900000.00 AS Decimal(18, 2)), CAST(3000000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 94, NULL, NULL, NULL, 0, NULL, 63, 0, NULL, 2, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (18, N'JORDAN 1 LOW CACTUS', N'jordan-1-low-cactus', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-05T11:45:57.7065571+07:00' AS DateTimeOffset), CAST(N'2021-01-05T11:45:57.7065601+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(4800000.00 AS Decimal(18, 2)), CAST(6000000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 95, NULL, NULL, NULL, 0, NULL, 68, 0, NULL, 3, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (19, N'Sneakers Puma RS-X Hard Drive', N'sneakers-puma-rs-x-hard-drive', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-05T21:36:52.0893111+07:00' AS DateTimeOffset), CAST(N'2021-01-05T21:36:52.0893230+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(2300000.00 AS Decimal(18, 2)), CAST(3000000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 89, NULL, NULL, NULL, 0, NULL, 105, 0, NULL, 3, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (20, N'XSPORT Adi.das Alpha.bounce Instinct M', N'xsport-adi.das-alpha.bounce-instinct-m', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-26T22:52:11.0718984+07:00' AS DateTimeOffset), CAST(N'2021-01-26T22:52:11.0719006+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(1000000.00 AS Decimal(18, 2)), CAST(1500000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 0, NULL, NULL, NULL, 0, NULL, 114, 0, NULL, 2, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (21, N'XSPORT Ni.ke Air Max 97', N'xsport-ni.ke-air-max-97', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-26T22:54:41.4889716+07:00' AS DateTimeOffset), CAST(N'2021-01-26T22:54:41.4889737+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(1500000.00 AS Decimal(18, 2)), CAST(2000000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 1, 0, 1, 1, 90, NULL, NULL, NULL, 0, NULL, 119, 0, NULL, 1, 1)
INSERT [dbo].[Catalog_Product] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [ShortDescription], [Description], [Specification], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd], [HasOptions], [IsVisibleIndividually], [IsFeatured], [IsCallForPricing], [IsAllowToOrder], [StockTrackingIsEnabled], [StockQuantity], [Sku], [Gtin], [NormalizedName], [DisplayOrder], [VendorId], [ThumbnailImageId], [ReviewsCount], [RatingAverage], [BrandId], [TaxClassId]) VALUES (22, N'Nike Air Max 2', N'nike-air-max-2', NULL, NULL, NULL, 1, NULL, 0, 10, CAST(N'2021-01-28T14:43:25.2850053+07:00' AS DateTimeOffset), CAST(N'2021-01-28T14:43:25.2850104+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(1000000.00 AS Decimal(18, 2)), CAST(1200000.00 AS Decimal(18, 2)), NULL, NULL, NULL, 0, 1, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, NULL, 140, 0, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Catalog_Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_ProductCategory] ON 

INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (87, 0, 0, 5, 18)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (88, 0, 0, 6, 18)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (96, 0, 0, 4, 18)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (97, 0, 0, 10, 15)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (98, 0, 0, 12, 15)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (99, 0, 0, 11, 15)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (100, 0, 0, 13, 15)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (110, 0, 0, 10, 16)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (111, 0, 0, 12, 16)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (112, 0, 0, 13, 16)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (113, 0, 0, 11, 16)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (133, 0, 0, 8, 17)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (134, 0, 0, 7, 17)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (135, 0, 0, 9, 17)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (142, 0, 0, 3, 19)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (143, 0, 0, 2, 19)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (144, 0, 0, 1, 19)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (149, 0, 0, 1, 20)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (150, 0, 0, 3, 20)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (151, 0, 0, 2, 20)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (152, 0, 0, 4, 21)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (153, 0, 0, 6, 21)
INSERT [dbo].[Catalog_ProductCategory] ([Id], [IsFeaturedProduct], [DisplayOrder], [CategoryId], [ProductId]) VALUES (154, 0, 0, 5, 21)
SET IDENTITY_INSERT [dbo].[Catalog_ProductCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_ProductLink] ON 

INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (1, 17, 17, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (2, 17, 16, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (3, 17, 15, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (4, 17, 17, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (5, 17, 16, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (6, 17, 15, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (7, 16, 17, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (8, 16, 16, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (9, 16, 15, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (10, 16, 17, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (11, 16, 16, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (12, 16, 15, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (13, 15, 17, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (14, 15, 16, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (15, 15, 15, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (16, 15, 17, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (17, 15, 16, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (18, 15, 15, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (19, 18, 17, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (20, 18, 15, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (21, 18, 16, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (22, 18, 17, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (23, 18, 15, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (24, 18, 16, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (25, 15, 18, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (26, 15, 18, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (27, 16, 18, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (28, 16, 18, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (29, 17, 18, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (30, 17, 18, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (31, 18, 18, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (32, 18, 18, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (33, 19, 17, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (34, 19, 18, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (35, 19, 15, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (36, 19, 16, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (37, 19, 18, 2)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (38, 19, 15, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (39, 19, 16, 3)
INSERT [dbo].[Catalog_ProductLink] ([Id], [ProductId], [LinkedProductId], [LinkType]) VALUES (40, 19, 17, 2)
SET IDENTITY_INSERT [dbo].[Catalog_ProductLink] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_ProductMedia] ON 

INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (1, 1, 2, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (2, 1, 4, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (3, 1, 3, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (21, 14, 25, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (22, 14, 26, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (23, 14, 27, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (24, 14, 28, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (29, 13, 33, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (30, 13, 34, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (31, 13, 35, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (32, 13, 36, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (33, 9, 37, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (34, 9, 38, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (35, 9, 39, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (36, 9, 40, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (37, 8, 41, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (38, 8, 42, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (39, 8, 43, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (40, 8, 44, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (41, 5, 45, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (42, 5, 46, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (43, 5, 47, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (44, 5, 48, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (77, 18, 85, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (78, 18, 86, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (79, 18, 87, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (80, 18, 88, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (81, 17, 89, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (82, 17, 90, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (83, 17, 91, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (84, 17, 92, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (85, 16, 93, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (86, 16, 94, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (87, 16, 95, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (88, 16, 96, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (89, 15, 97, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (90, 15, 98, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (91, 15, 99, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (92, 15, 100, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (93, 15, 101, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (109, 21, 120, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (110, 21, 121, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (111, 21, 122, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (112, 21, 123, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (113, 20, 124, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (114, 20, 125, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (115, 20, 126, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (116, 20, 127, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (121, 19, 132, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (122, 19, 133, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (123, 19, 134, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (124, 19, 135, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (125, 22, 136, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (126, 22, 137, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (127, 22, 138, 0)
INSERT [dbo].[Catalog_ProductMedia] ([Id], [ProductId], [MediaId], [DisplayOrder]) VALUES (128, 22, 139, 0)
SET IDENTITY_INSERT [dbo].[Catalog_ProductMedia] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_ProductOption] ON 

INSERT [dbo].[Catalog_ProductOption] ([Id], [Name]) VALUES (1, N'Màu sắc')
INSERT [dbo].[Catalog_ProductOption] ([Id], [Name]) VALUES (2, N'Kích thước')
SET IDENTITY_INSERT [dbo].[Catalog_ProductOption] OFF
GO
SET IDENTITY_INSERT [dbo].[Catalog_ProductPriceHistory] ON 

INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (10, 18, 10, CAST(N'2021-01-05T11:45:57.7506229+07:00' AS DateTimeOffset), CAST(4800000.00 AS Decimal(18, 2)), CAST(6000000.00 AS Decimal(18, 2)), NULL, NULL, NULL)
INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (11, 19, 10, CAST(N'2021-01-05T21:36:52.3374114+07:00' AS DateTimeOffset), CAST(2300000.00 AS Decimal(18, 2)), CAST(3000000.00 AS Decimal(18, 2)), NULL, NULL, NULL)
INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (12, 20, 10, CAST(N'2021-01-26T22:52:11.1281892+07:00' AS DateTimeOffset), CAST(0.00 AS Decimal(18, 2)), CAST(1500000.00 AS Decimal(18, 2)), CAST(1000000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (13, 21, 10, CAST(N'2021-01-26T22:54:41.5388808+07:00' AS DateTimeOffset), CAST(0.00 AS Decimal(18, 2)), CAST(2000000.00 AS Decimal(18, 2)), CAST(1500000.00 AS Decimal(18, 2)), NULL, NULL)
INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (14, 21, 10, CAST(N'2021-01-26T22:55:43.0989738+07:00' AS DateTimeOffset), CAST(1500000.00 AS Decimal(18, 2)), CAST(2000000.00 AS Decimal(18, 2)), NULL, NULL, NULL)
INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (15, 20, 10, CAST(N'2021-01-26T22:56:01.0151001+07:00' AS DateTimeOffset), CAST(1000000.00 AS Decimal(18, 2)), CAST(1500000.00 AS Decimal(18, 2)), NULL, NULL, NULL)
INSERT [dbo].[Catalog_ProductPriceHistory] ([Id], [ProductId], [CreatedById], [CreatedOn], [Price], [OldPrice], [SpecialPrice], [SpecialPriceStart], [SpecialPriceEnd]) VALUES (16, 22, 10, CAST(N'2021-01-28T14:43:25.4382997+07:00' AS DateTimeOffset), CAST(1000000.00 AS Decimal(18, 2)), CAST(1200000.00 AS Decimal(18, 2)), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Catalog_ProductPriceHistory] OFF
GO
SET IDENTITY_INSERT [dbo].[Cms_Menu] ON 

INSERT [dbo].[Cms_Menu] ([Id], [Name], [IsPublished], [IsSystem]) VALUES (1, N'Dịch vụ khách hàng', 0, 1)
INSERT [dbo].[Cms_Menu] ([Id], [Name], [IsPublished], [IsSystem]) VALUES (2, N'Thông tin', 0, 1)
SET IDENTITY_INSERT [dbo].[Cms_Menu] OFF
GO
SET IDENTITY_INSERT [dbo].[Cms_MenuItem] ON 

INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (1, NULL, 1, 21, NULL, N'Trung tâm chăm sóc khách hàng', 0)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (2, NULL, 1, 22, NULL, N'
Làm thế nào để mua hàng', 1)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (3, NULL, 1, 23, NULL, N'Chuyển hàng', 2)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (4, NULL, 1, 24, NULL, N'Trở về', 3)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (5, NULL, 1, 25, NULL, N'
Sự bảo đảm', 4)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (6, NULL, 2, 18, NULL, N'
Về chúng tôi', 0)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (7, NULL, 2, 19, NULL, N'
Điều khoản sử dụng', 1)
INSERT [dbo].[Cms_MenuItem] ([Id], [ParentId], [MenuId], [EntityId], [CustomLink], [Name], [DisplayOrder]) VALUES (8, NULL, 2, 20, NULL, N'Bảo mật', 2)
SET IDENTITY_INSERT [dbo].[Cms_MenuItem] OFF
GO
SET IDENTITY_INSERT [dbo].[Cms_Page] ON 

INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (1, N'Về chúng tôi', N'about-us', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T08:45:44.9533333+00:00' AS DateTimeOffset), CAST(N'2020-11-20T08:45:44.9533333+00:00' AS DateTimeOffset), 10, N'
<h1> Về chúng tôi </h1> <p> Thông tin của bạn. Sử dụng trang web quản trị để cập nhật </p>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (2, N'
Điều khoản và điều kiện', N'terms-of-use', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T02:54:57.1766667+00:00' AS DateTimeOffset), CAST(N'2020-11-20T02:54:57.1766667+00:00' AS DateTimeOffset), 10, N'<h1> Điều khoản và Điều kiện </h1> <p> Điều khoản và điều kiện của bạn. Sử dụng trang web quản trị để cập nhật </p>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (3, N'
Chính sách bảo mật', N'privacy', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T02:59:31.9633333+00:00' AS DateTimeOffset), CAST(N'2020-11-20T02:59:31.9633333+00:00' AS DateTimeOffset), 10, N'
<h1> Chính sách Bảo mật </h1> <p> Thông tin chính sách bảo mật của bạn. Sử dụng trang web quản trị để cập nhật </p>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (4, N'
Trung tâm trợ giúp', N'help-center', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T03:14:04.4466667+00:00' AS DateTimeOffset), CAST(N'2020-11-20T03:14:04.4466667+00:00' AS DateTimeOffset), 10, N'
<h1> Trung tâm trợ giúp </h1>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (5, N'
Mua thế nào', N'help-center/how-to-buy', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T03:16:14.3233333+00:00' AS DateTimeOffset), CAST(N'2020-11-20T03:24:13.8333333+00:00' AS DateTimeOffset), 10, N'
<h1> Cách mua </h1> <p> Hướng dẫn cách mua của bạn. Sử dụng trang web quản trị để cập nhật </p>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (6, N'
Vận chuyển và giao hàng', N'help-center/shipping', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T02:57:13.4966667+00:00' AS DateTimeOffset), CAST(N'2020-11-20T03:23:31.5966667+00:00' AS DateTimeOffset), 10, N'
<h1> Vận chuyển và Giao hàng </h1> <p> Thông tin vận chuyển và giao hàng. Sử dụng trang web quản trị để cập nhật </p>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (7, N'
Làm thế nào để trở lại', N'help-center/how-to-return', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T03:17:42.0666667+00:00' AS DateTimeOffset), CAST(N'2020-11-20T03:17:42.0666667+00:00' AS DateTimeOffset), 10, N'
<h1> Cách quay lại </h1> <p> Hướng dẫn cách trả lại của bạn. Sử dụng trang web quản trị để cập nhật </p>')
INSERT [dbo].[Cms_Page] ([Id], [Name], [Slug], [MetaTitle], [MetaKeywords], [MetaDescription], [IsPublished], [PublishedOn], [IsDeleted], [CreatedById], [CreatedOn], [LatestUpdatedOn], [LatestUpdatedById], [Body]) VALUES (8, N'
Sự bảo đảm', N'help-center/warranty', NULL, NULL, NULL, 0, NULL, 0, 10, CAST(N'2020-11-20T03:19:01.9266667+00:00' AS DateTimeOffset), CAST(N'2020-11-20T03:19:01.9266667+00:00' AS DateTimeOffset), 10, N'
<h1>Warranty</h1> <p> Chính sách bảo hành của bạn. Sử dụng trang web quản trị để cập nhật </p>')
SET IDENTITY_INSERT [dbo].[Cms_Page] OFF
GO
SET IDENTITY_INSERT [dbo].[Contacts_ContactArea] ON 

INSERT [dbo].[Contacts_ContactArea] ([Id], [Name], [IsDeleted]) VALUES (1, N'Khắc Dấu Quốc Tiến', 1)
INSERT [dbo].[Contacts_ContactArea] ([Id], [Name], [IsDeleted]) VALUES (2, N'Khắc Dấu Quốc Tiến', 1)
INSERT [dbo].[Contacts_ContactArea] ([Id], [Name], [IsDeleted]) VALUES (3, N'Khắc Dấu Quốc Tiến', 1)
SET IDENTITY_INSERT [dbo].[Contacts_ContactArea] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_Address] ON 

INSERT [dbo].[Core_Address] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (1, N'Tri Nghia', N'0967666173', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 766, 1, N'VN')
INSERT [dbo].[Core_Address] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (2, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Core_Address] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (3, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 784, 1, N'VN')
INSERT [dbo].[Core_Address] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (4, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 274, 1, N'VN')
SET IDENTITY_INSERT [dbo].[Core_Address] OFF
GO
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Catalog.IsCommentsRequireApproval', N'true', N'Catalog', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Catalog.IsProductPriceIncludeTax', N'true', N'Catalog', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Catalog.ProductPageSize', N'10', N'Catalog', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Global.AssetBundling', N'false', N'Core', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Global.AssetVersion', N'1.0', N'Core', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Global.CurrencyCulture', N'vi', N'Core', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Global.CurrencyDecimalPlace', N'0', N'Core', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Global.DefaultCultureUI', N'vi', N'Core', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'GoogleAppKey', N'true', N'Contact', 0)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Localization.LocalizedConentEnable', N'true', N'Localization', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'News.PageSize', N'10', N'News', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'SmtpPassword', N'true', N'EmailSenderSmpt', 0)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'SmtpPort', N'587', N'EmailSenderSmpt', 0)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'SmtpServer', N'smtp.gmail.com', N'EmailSenderSmpt', 0)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'SmtpUsername', N'true', N'EmailSenderSmpt', 0)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Tax.DefaultTaxClassId', N'1', N'Tax', 1)
INSERT [dbo].[Core_AppSetting] ([Id], [Value], [Module], [IsVisibleInCommonSettingPage]) VALUES (N'Theme', N'CozaStore', N'Core', 0)
GO
INSERT [dbo].[Core_Country] ([Id], [Name], [Code3], [IsBillingEnabled], [IsShippingEnabled], [IsCityEnabled], [IsZipCodeEnabled], [IsDistrictEnabled]) VALUES (N'VN', N'Việt Nam', N'VNM', 1, 1, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Core_District] ON 

INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (1, 79, N'Ba Đình', N'Quận', N'21 02 08N, 105 49 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (2, 79, N'Hoàn Kiếm', N'Quận', N'21 01 53N, 105 51 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (3, 79, N'Tây Hồ', N'Quận', N'21 04 10N, 105 49 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (4, 79, N'Long Biên', N'Quận', N'21 02 21N, 105 53 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (5, 79, N'Cầu Giấy', N'Quận', N'21 01 52N, 105 47 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (6, 79, N'Đống Đa', N'Quận', N'21 00 56N, 105 49 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (7, 79, N'Hai Bà Trưng', N'Quận', N'21 00 27N, 105 51 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (8, 79, N'Hoàng Mai', N'Quận', N'20 58 33N, 105 51 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (9, 79, N'Thanh Xuân', N'Quận', N'20 59 44N, 105 48 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (16, 79, N'Sóc Sơn', N'Huyện', N'21 16 55N, 105 49 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (17, 79, N'Đông Anh', N'Huyện', N'21 08 16N, 105 49 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (18, 79, N'Gia Lâm', N'Huyện', N'21 01 28N, 105 56 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (19, 79, N'Từ Liêm', N'Huyện', N'21 02 39N, 105 45 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (20, 79, N'Thanh Trì', N'Huyện', N'20 56 32N, 105 50 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (24, 2, N'Hà Giang', N'Thị Xã', N'22 46 23N, 105 02 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (26, 2, N'Đồng Văn', N'Huyện', N'23 14 34N, 105 15 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (27, 2, N'Mèo Vạc', N'Huyện', N'23 09 10N, 105 26 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (28, 2, N'Yên Minh', N'Huyện', N'23 04 20N, 105 10 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (29, 2, N'Quản Bạ', N'Huyện', N'23 04 03N, 104 58 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (30, 2, N'Vị Xuyên', N'Huyện', N'22 45 50N, 104 56 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (31, 2, N'Bắc Mê', N'Huyện', N'22 45 48N, 105 16 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (32, 2, N'Hoàng Su Phì', N'Huyện', N'22 41 37N, 104 40 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (33, 2, N'Xín Mần', N'Huyện', N'22 38 05N, 104 28 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (34, 2, N'Bắc Quang', N'Huyện', N'22 23 42N, 104 55 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (35, 2, N'Quang Bình', N'Huyện', N'22 23 07N, 104 37 11E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (40, 4, N'Cao Bằng', N'Thị Xã', N'22 39 20N, 106 15 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (42, 4, N'Bảo Lâm', N'Huyện', N'22 52 37N, 105 27 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (43, 4, N'Bảo Lạc', N'Huyện', N'22 52 31N, 105 42 41E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (44, 4, N'Thông Nông', N'Huyện', N'22 49 09N, 105 57 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (45, 4, N'Hà Quảng', N'Huyện', N'22 53 42N, 106 06 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (46, 4, N'Trà Lĩnh', N'Huyện', N'22 48 14N, 106 19 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (47, 4, N'Trùng Khánh', N'Huyện', N'22 49 31N, 106 33 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (48, 4, N'Hạ Lang', N'Huyện', N'22 42 37N, 106 41 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (49, 4, N'Quảng Uyên', N'Huyện', N'22 40 15N, 106 27 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (50, 4, N'Phục Hoà', N'Huyện', N'22 33 52N, 106 30 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (51, 4, N'Hoà An', N'Huyện', N'22 41 20N, 106 02 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (52, 4, N'Nguyên Bình', N'Huyện', N'22 38 52N, 105 57 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (53, 4, N'Thạch An', N'Huyện', N'22 28 51N, 106 19 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (58, 6, N'Bắc Kạn', N'Thị Xã', N'22 08 00N, 105 51 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (60, 6, N'Pác Nặm', N'Huyện', N'22 35 46N, 105 40 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (61, 6, N'Ba Bể', N'Huyện', N'22 23 54N, 105 43 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (62, 6, N'Ngân Sơn', N'Huyện', N'22 25 50N, 106 01 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (63, 6, N'Bạch Thông', N'Huyện', N'22 12 04N, 105 51 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (64, 6, N'Chợ Đồn', N'Huyện', N'22 11 18N, 105 34 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (65, 6, N'Chợ Mới', N'Huyện', N'21 57 56N, 105 51 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (66, 6, N'Na Rì', N'Huyện', N'22 09 48N, 106 05 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (70, 8, N'Tuyên Quang', N'Thị Xã', N'21 49 40N, 105 13 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (72, 8, N'Nà Hang', N'Huyện', N'22 28 34N, 105 21 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (73, 8, N'Chiêm Hóa', N'Huyện', N'22 12 49N, 105 14 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (74, 8, N'Hàm Yên', N'Huyện', N'22 05 46N, 105 00 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (75, 8, N'Yên Sơn', N'Huyện', N'21 51 53N, 105 18 14E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (76, 8, N'Sơn Dương', N'Huyện', N'21 40 22N, 105 22 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (80, 10, N'Lào Cai', N'Thành Phố', N'22 25 07N, 103 58 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (82, 10, N'Bát Xát', N'Huyện', N'22 35 50N, 103 44 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (83, 10, N'Mường Khương', N'Huyện', N'22 41 05N, 104 08 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (84, 10, N'Si Ma Cai', N'Huyện', N'22 39 46N, 104 16 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (85, 10, N'Bắc Hà', N'Huyện', N'22 30 08N, 104 18 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (86, 10, N'Bảo Thắng', N'Huyện', N'22 22 47N, 104 10 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (87, 10, N'Bảo Yên', N'Huyện', N'22 17 38N, 104 25 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (88, 10, N'Sa Pa', N'Huyện', N'22 18 54N, 103 54 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (89, 10, N'Văn Bàn', N'Huyện', N'22 03 48N, 104 10 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (94, 11, N'Điện Biên Phủ', N'Thành Phố', N'21 24 52N, 103 02 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (95, 11, N'Mường Lay', N'Thị Xã', N'22 01 47N, 103 07 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (96, 11, N'Mường Nhé', N'Huyện', N'22 06 11N, 102 30 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (97, 11, N'Mường Chà', N'Huyện', N'21 50 31N, 103 03 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (98, 11, N'Tủa Chùa', N'Huyện', N'21 58 19N, 103 23 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (99, 11, N'Tuần Giáo', N'Huyện', N'21 38 03N, 103 21 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (100, 11, N'Điện Biên', N'Huyện', N'21 15 19N, 103 03 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (101, 11, N'Điện Biên Đông', N'Huyện', N'21 14 07N, 103 15 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (102, 11, N'Mường Ảng', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (104, 12, N'Lai Châu', N'Thị Xã', N'22 23 15N, 103 24 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (106, 12, N'Tam Đường', N'Huyện', N'22 20 16N, 103 32 53E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (107, 12, N'Mường Tè', N'Huyện', N'22 24 16N, 102 43 11E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (108, 12, N'Sìn Hồ', N'Huyện', N'22 17 21N, 103 18 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (109, 12, N'Phong Thổ', N'Huyện', N'22 38 24N, 103 22 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (110, 12, N'Than Uyên', N'Huyện', N'21 59 35N, 103 45 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (111, 12, N'Tân Uyên', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (116, 14, N'Sơn La', N'Thành Phố', N'21 20 39N, 103 54 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (118, 14, N'Quỳnh Nhai', N'Huyện', N'21 46 34N, 103 39 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (119, 14, N'Thuận Châu', N'Huyện', N'21 24 54N, 103 39 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (120, 14, N'Mường La', N'Huyện', N'21 31 38N, 104 02 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (121, 14, N'Bắc Yên', N'Huyện', N'21 13 23N, 104 22 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (122, 14, N'Phù Yên', N'Huyện', N'21 13 33N, 104 41 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (123, 14, N'Mộc Châu', N'Huyện', N'20 49 21N, 104 43 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (124, 14, N'Yên Châu', N'Huyện', N'20 59 33N, 104 19 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (125, 14, N'Mai Sơn', N'Huyện', N'21 10 08N, 103 59 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (126, 14, N'Sông Mã', N'Huyện', N'21 06 04N, 103 43 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (127, 14, N'Sốp Cộp', N'Huyện', N'20 52 46N, 103 30 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (132, 15, N'Yên Bái', N'Thành Phố', N'21 44 28N, 104 53 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (133, 15, N'Nghĩa Lộ', N'Thị Xã', N'21 35 58N, 104 29 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (135, 15, N'Lục Yên', N'Huyện', N'22 06 44N, 104 43 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (136, 15, N'Văn Yên', N'Huyện', N'21 55 55N, 104 33 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (137, 15, N'Mù Cang Chải', N'Huyện', N'21 48 22N, 104 09 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (138, 15, N'Trấn Yên', N'Huyện', N'21 42 20N, 104 48 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (139, 15, N'Trạm Tấu', N'Huyện', N'21 30 40N, 104 28 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (140, 15, N'Văn Chấn', N'Huyện', N'21 34 15N, 104 35 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (141, 15, N'Yên Bình', N'Huyện', N'21 52 24N, 104 55 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (148, 17, N'Hòa Bình', N'Thành Phố', N'20 50 36N, 105 19 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (150, 17, N'Đà Bắc', N'Huyện', N'20 55 58N, 105 04 52E')
GO
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (151, 17, N'Kỳ Sơn', N'Huyện', N'20 54 06N, 105 23 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (152, 17, N'Lương Sơn', N'Huyện', N'20 53 16N, 105 30 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (153, 17, N'Kim Bôi', N'Huyện', N'20 40 34N, 105 32 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (154, 17, N'Cao Phong', N'Huyện', N'20 41 23N, 105 17 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (155, 17, N'Tân Lạc', N'Huyện', N'20 36 44N, 105 15 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (156, 17, N'Mai Châu', N'Huyện', N'20 40 56N, 104 59 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (157, 17, N'Lạc Sơn', N'Huyện', N'20 29 59N, 105 24 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (158, 17, N'Yên Thủy', N'Huyện', N'20 25 42N, 105 37 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (159, 17, N'Lạc Thủy', N'Huyện', N'20 29 12N, 105 44 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (164, 19, N'Thái Nguyên', N'Thành Phố', N'21 33 20N, 105 48 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (165, 19, N'Sông Công', N'Thị Xã', N'21 29 14N, 105 48 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (167, 19, N'Định Hóa', N'Huyện', N'21 53 50N, 105 38 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (168, 19, N'Phú Lương', N'Huyện', N'21 45 57N, 105 43 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (169, 19, N'Đồng Hỷ', N'Huyện', N'21 41 10N, 105 55 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (170, 19, N'Võ Nhai', N'Huyện', N'21 47 14N, 106 02 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (171, 19, N'Đại Từ', N'Huyện', N'21 36 17N, 105 37 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (172, 19, N'Phổ Yên', N'Huyện', N'21 27 08N, 105 45 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (173, 19, N'Phú Bình', N'Huyện', N'21 29 36N, 105 57 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (178, 20, N'Lạng Sơn', N'Thành Phố', N'21 51 19N, 106 44 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (180, 20, N'Tràng Định', N'Huyện', N'22 18 09N, 106 26 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (181, 20, N'Bình Gia', N'Huyện', N'22 03 56N, 106 19 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (182, 20, N'Văn Lãng', N'Huyện', N'22 01 59N, 106 34 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (183, 20, N'Cao Lộc', N'Huyện', N'21 53 05N, 106 50 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (184, 20, N'Văn Quan', N'Huyện', N'21 51 04N, 106 33 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (185, 20, N'Bắc Sơn', N'Huyện', N'21 48 57N, 106 15 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (186, 20, N'Hữu Lũng', N'Huyện', N'21 34 33N, 106 20 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (187, 20, N'Chi Lăng', N'Huyện', N'21 40 05N, 106 37 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (188, 20, N'Lộc Bình', N'Huyện', N'21 40 41N, 106 58 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (189, 20, N'Đình Lập', N'Huyện', N'21 32 07N, 107 07 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (193, 22, N'Hạ Long', N'Thành Phố', N'20 52 24N, 107 05 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (194, 22, N'Móng Cái', N'Thành Phố', N'21 26 31N, 107 55 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (195, 22, N'Cẩm Phả', N'Thị Xã', N'21 03 42N, 107 17 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (196, 22, N'Uông Bí', N'Thị Xã', N'21 04 33N, 106 45 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (198, 22, N'Bình Liêu', N'Huyện', N'21 33 07N, 107 26 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (199, 22, N'Tiên Yên', N'Huyện', N'21 22 24N, 107 22 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (200, 22, N'Đầm Hà', N'Huyện', N'21 21 23N, 107 34 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (201, 22, N'Hải Hà', N'Huyện', N'21 25 50N, 107 41 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (202, 22, N'Ba Chẽ', N'Huyện', N'21 15 40N, 107 09 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (203, 22, N'Vân Đồn', N'Huyện', N'20 56 17N, 107 28 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (204, 22, N'Hoành Bồ', N'Huyện', N'21 06 30N, 107 02 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (205, 22, N'Đông Triều', N'Huyện', N'21 07 10N, 106 34 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (206, 22, N'Yên Hưng', N'Huyện', N'20 55 40N, 106 51 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (207, 22, N'Cô Tô', N'Huyện', N'21 05 01N, 107 49 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (213, 24, N'Bắc Giang', N'Thành Phố', N'21 17 36N, 106 11 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (215, 24, N'Yên Thế', N'Huyện', N'21 31 29N, 106 09 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (216, 24, N'Tân Yên', N'Huyện', N'21 23 23N, 106 05 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (217, 24, N'Lạng Giang', N'Huyện', N'21 21 58N, 106 15 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (218, 24, N'Lục Nam', N'Huyện', N'21 18 16N, 106 29 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (219, 24, N'Lục Ngạn', N'Huyện', N'21 26 15N, 106 39 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (220, 24, N'Sơn Động', N'Huyện', N'21 19 42N, 106 51 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (221, 24, N'Yên Dũng', N'Huyện', N'21 12 22N, 106 14 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (222, 24, N'Việt Yên', N'Huyện', N'21 16 16N, 106 04 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (223, 24, N'Hiệp Hòa', N'Huyện', N'21 15 51N, 105 57 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (227, 25, N'Việt Trì', N'Thành Phố', N'21 19 01N, 105 23 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (228, 25, N'Phú Thọ', N'Thị Xã', N'21 24 54N, 105 13 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (230, 25, N'Đoan Hùng', N'Huyện', N'21 36 56N, 105 08 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (231, 25, N'Hạ Hoà', N'Huyện', N'21 35 13N, 105 00 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (232, 25, N'Thanh Ba', N'Huyện', N'21 27 04N, 105 09 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (233, 25, N'Phù Ninh', N'Huyện', N'21 26 59N, 105 18 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (234, 25, N'Yên Lập', N'Huyện', N'21 22 21N, 105 01 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (235, 25, N'Cẩm Khê', N'Huyện', N'21 23 04N, 105 05 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (236, 25, N'Tam Nông', N'Huyện', N'21 18 24N, 105 14 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (237, 25, N'Lâm Thao', N'Huyện', N'21 19 37N, 105 18 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (238, 25, N'Thanh Sơn', N'Huyện', N'21 08 32N, 105 04 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (239, 25, N'Thanh Thuỷ', N'Huyện', N'21 07 26N, 105 17 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (240, 25, N'Tân Sơn', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (243, 26, N'Vĩnh Yên', N'Thành Phố', N'21 18 26N, 105 35 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (244, 26, N'Phúc Yên', N'Thị Xã', N'21 18 55N, 105 43 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (246, 26, N'Lập Thạch', N'Huyện', N'21 24 45N, 105 25 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (247, 26, N'Tam Dương', N'Huyện', N'21 21 40N, 105 33 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (248, 26, N'Tam Đảo', N'Huyện', N'21 27 34N, 105 35 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (249, 26, N'Bình Xuyên', N'Huyện', N'21 19 48N, 105 39 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (250, 1, N'Mê Linh', N'Huyện', N'21 10 53N, 105 42 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (251, 26, N'Yên Lạc', N'Huyện', N'21 13 17N, 105 34 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (252, 26, N'Vĩnh Tường', N'Huyện', N'21 14 58N, 105 29 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (253, 26, N'Sông Lô', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (256, 27, N'Bắc Ninh', N'Thành Phố', N'21 10 48N, 106 03 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (258, 27, N'Yên Phong', N'Huyện', N'21 12 40N, 105 59 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (259, 27, N'Quế Võ', N'Huyện', N'21 08 44N, 106 11 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (260, 27, N'Tiên Du', N'Huyện', N'21 07 37N, 106 02 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (261, 27, N'Từ Sơn', N'Thị Xã', N'21 07 12N, 105 57 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (262, 27, N'Thuận Thành', N'Huyện', N'21 02 24N, 106 04 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (263, 27, N'Gia Bình', N'Huyện', N'21 03 55N, 106 12 53E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (264, 27, N'Lương Tài', N'Huyện', N'21 01 33N, 106 13 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (268, 1, N'Hà Đông', N'Quận', N'20 57 25N, 105 45 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (269, 1, N'Sơn Tây', N'Thị Xã', N'21 05 51N, 105 28 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (271, 1, N'Ba Vì', N'Huyện', N'21 09 40N, 105 22 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (272, 1, N'Phúc Thọ', N'Huyện', N'21 06 32N, 105 34 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (273, 1, N'Đan Phượng', N'Huyện', N'21 07 13N, 105 40 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (274, 1, N'Hoài Đức', N'Huyện', N'21 01 25N, 105 42 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (275, 1, N'Quốc Oai', N'Huyện', N'20 58 45N, 105 36 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (276, 1, N'Thạch Thất', N'Huyện', N'21 02 17N, 105 33 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (277, 1, N'Chương Mỹ', N'Huyện', N'20 52 46N, 105 39 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (278, 1, N'Thanh Oai', N'Huyện', N'20 51 44N, 105 46 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (279, 1, N'Thường Tín', N'Huyện', N'20 49 59N, 105 22 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (280, 1, N'Phú Xuyên', N'Huyện', N'20 43 37N, 105 53 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (281, 1, N'Ứng Hòa', N'Huyện', N'20 42 41N, 105 47 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (282, 1, N'Mỹ Đức', N'Huyện', N'20 41 53N, 105 43 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (288, 30, N'Hải Dương', N'Thành Phố', N'20 56 14N, 106 18 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (290, 30, N'Chí Linh', N'Huyện', N'21 07 47N, 106 23 46E')
GO
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (291, 30, N'Nam Sách', N'Huyện', N'21 00 15N, 106 20 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (292, 30, N'Kinh Môn', N'Huyện', N'21 00 04N, 106 30 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (293, 30, N'Kim Thành', N'Huyện', N'20 55 40N, 106 30 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (294, 30, N'Thanh Hà', N'Huyện', N'20 53 19N, 106 25 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (295, 30, N'Cẩm Giàng', N'Huyện', N'20 57 05N, 106 12 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (296, 30, N'Bình Giang', N'Huyện', N'20 52 36N, 106 11 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (297, 30, N'Gia Lộc', N'Huyện', N'20 51 01N, 106 17 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (298, 30, N'Tứ Kỳ', N'Huyện', N'20 49 05N, 106 24 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (299, 30, N'Ninh Giang', N'Huyện', N'20 45 13N, 106 20 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (300, 30, N'Thanh Miện', N'Huyện', N'20 46 02N, 106 12 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (303, 31, N'Hồng Bàng', N'Quận', N'20 52 37N, 106 38 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (304, 31, N'Ngô Quyền', N'Quận', N'20 51 13N, 106 41 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (305, 31, N'Lê Chân', N'Quận', N'20 50 12N, 106 40 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (306, 31, N'Hải An', N'Quận', N'20 49 38N, 106 45 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (307, 31, N'Kiến An', N'Quận', N'20 48 26N, 106 38 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (308, 31, N'Đồ Sơn', N'Quận', N'20 42 41N, 106 44 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (309, 31, N'Kinh Dương', N'Quận', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (311, 31, N'Thuỷ Nguyên', N'Huyện', N'20 56 36N, 106 39 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (312, 31, N'An Dương', N'Huyện', N'20 53 06N, 106 35 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (313, 31, N'An Lão', N'Huyện', N'20 47 54N, 106 33 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (314, 31, N'Kiến Thụy', N'Huyện', N'20 45 13N, 106 41 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (315, 31, N'Tiên Lãng', N'Huyện', N'20 42 23N, 106 36 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (316, 31, N'Vĩnh Bảo', N'Huyện', N'20 40 56N, 106 29 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (317, 31, N'Cát Hải', N'Huyện', N'20 47 09N, 106 58 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (318, 31, N'Bạch Long Vĩ', N'Huyện', N'20 08 41N, 107 42 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (323, 33, N'Hưng Yên', N'Thành Phố', N'20 39 38N, 106 03 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (325, 33, N'Văn Lâm', N'Huyện', N'20 58 31N, 106 02 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (326, 33, N'Văn Giang', N'Huyện', N'20 55 51N, 105 57 14E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (327, 33, N'Yên Mỹ', N'Huyện', N'20 53 45N, 106 01 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (328, 33, N'Mỹ Hào', N'Huyện', N'20 55 35N, 106 05 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (329, 33, N'Ân Thi', N'Huyện', N'20 48 49N, 106 05 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (330, 33, N'Khoái Châu', N'Huyện', N'20 49 53N, 105 58 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (331, 33, N'Kim Động', N'Huyện', N'20 44 47N, 106 01 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (332, 33, N'Tiên Lữ', N'Huyện', N'20 40 05N, 106 07 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (333, 33, N'Phù Cừ', N'Huyện', N'20 42 51N, 106 11 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (336, 34, N'Thái Bình', N'Thành Phố', N'20 26 45N, 106 19 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (338, 34, N'Quỳnh Phụ', N'Huyện', N'20 38 57N, 106 21 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (339, 34, N'Hưng Hà', N'Huyện', N'20 35 38N, 106 12 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (340, 34, N'Đông Hưng', N'Huyện', N'20 32 50N, 106 20 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (341, 34, N'Thái Thụy', N'Huyện', N'20 32 33N, 106 32 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (342, 34, N'Tiền Hải', N'Huyện', N'20 21 05N, 106 32 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (343, 34, N'Kiến Xương', N'Huyện', N'20 23 52N, 106 25 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (344, 34, N'Vũ Thư', N'Huyện', N'20 25 29N, 106 16 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (347, 35, N'Phủ Lý', N'Thành Phố', N'20 32 19N, 105 54 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (349, 35, N'Duy Tiên', N'Huyện', N'20 37 33N, 105 58 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (350, 35, N'Kim Bảng', N'Huyện', N'20 34 19N, 105 50 41E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (351, 35, N'Thanh Liêm', N'Huyện', N'20 27 31N, 105 55 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (352, 35, N'Bình Lục', N'Huyện', N'20 29 23N, 106 02 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (353, 35, N'Lý Nhân', N'Huyện', N'20 32 55N, 106 04 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (356, 36, N'Nam Định', N'Thành Phố', N'20 25 07N, 106 09 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (358, 36, N'Mỹ Lộc', N'Huyện', N'20 27 21N, 106 07 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (359, 36, N'Vụ Bản', N'Huyện', N'20 22 57N, 106 05 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (360, 36, N'Ý Yên', N'Huyện', N'20 19 48N, 106 01 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (361, 36, N'Nghĩa Hưng', N'Huyện', N'20 05 37N, 106 08 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (362, 36, N'Nam Trực', N'Huyện', N'20 20 08N, 106 12 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (363, 36, N'Trực Ninh', N'Huyện', N'20 14 42N, 106 12 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (364, 36, N'Xuân Trường', N'Huyện', N'20 17 53N, 106 21 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (365, 36, N'Giao Thủy', N'Huyện', N'20 14 45N, 106 28 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (366, 36, N'Hải Hậu', N'Huyện', N'20 06 26N, 106 16 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (369, 37, N'Ninh Bình', N'Thành Phố', N'20 14 45N, 105 58 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (370, 37, N'Tam Điệp', N'Thị Xã', N'20 09 42N, 103 52 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (372, 37, N'Nho Quan', N'Huyện', N'20 18 46N, 105 42 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (373, 37, N'Gia Viễn', N'Huyện', N'20 19 50N, 105 52 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (374, 37, N'Hoa Lư', N'Huyện', N'20 15 04N, 105 55 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (375, 37, N'Yên Khánh', N'Huyện', N'20 11 26N, 106 04 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (376, 37, N'Kim Sơn', N'Huyện', N'20 02 07N, 106 05 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (377, 37, N'Yên Mô', N'Huyện', N'20 07 45N, 105 59 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (380, 38, N'Thanh Hóa', N'Thành Phố', N'19 48 26N, 105 47 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (381, 38, N'Bỉm Sơn', N'Thị Xã', N'20 05 21N, 105 51 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (382, 38, N'Sầm Sơn', N'Thị Xã', N'19 45 11N, 105 54 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (384, 38, N'Mường Lát', N'Huyện', N'20 30 42N, 104 37 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (385, 38, N'Quan Hóa', N'Huyện', N'20 29 15N, 104 56 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (386, 38, N'Bá Thước', N'Huyện', N'20 22 48N, 105 14 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (387, 38, N'Quan Sơn', N'Huyện', N'20 15 17N, 104 51 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (388, 38, N'Lang Chánh', N'Huyện', N'20 08 58N, 105 07 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (389, 38, N'Ngọc Lặc', N'Huyện', N'20 04 08N, 105 22 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (390, 38, N'Cẩm Thủy', N'Huyện', N'20 12 20N, 105 27 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (391, 38, N'Thạch Thành', N'Huyện', N'21 12 41N, 105 36 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (392, 38, N'Hà Trung', N'Huyện', N'20 03 20N, 105 51 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (393, 38, N'Vĩnh Lộc', N'Huyện', N'20 02 29N, 105 39 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (394, 38, N'Yên Định', N'Huyện', N'20 00 31N, 105 37 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (395, 38, N'Thọ Xuân', N'Huyện', N'19 55 39N, 105 29 14E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (396, 38, N'Thường Xuân', N'Huyện', N'19 54 55N, 105 10 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (397, 38, N'Triệu Sơn', N'Huyện', N'19 48 11N, 105 34 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (398, 38, N'Thiệu Hoá', N'Huyện', N'19 53 56N, 105 40 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (399, 38, N'Hoằng Hóa', N'Huyện', N'19 51 59N, 105 51 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (400, 38, N'Hậu Lộc', N'Huyện', N'19 56 02N, 105 53 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (401, 38, N'Nga Sơn', N'Huyện', N'20 00 16N, 105 59 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (402, 38, N'Như Xuân', N'Huyện', N'19 5 55N, 105 20 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (403, 38, N'Như Thanh', N'Huyện', N'19 35 19N, 105 33 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (404, 38, N'Nông Cống', N'Huyện', N'19 36 58N, 105 40 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (405, 38, N'Đông Sơn', N'Huyện', N'19 47 44N, 105 42 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (406, 38, N'Quảng Xương', N'Huyện', N'19 40 53N, 105 48 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (407, 38, N'Tĩnh Gia', N'Huyện', N'19 27 13N, 105 43 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (412, 40, N'Vinh', N'Thành Phố', N'18 41 06N, 105 42 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (413, 40, N'Cửa Lò', N'Thị Xã', N'18 47 26N, 105 43 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (414, 40, N'Thái Hoà', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (415, 40, N'Quế Phong', N'Huyện', N'19 42 25N, 104 54 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (416, 40, N'Quỳ Châu', N'Huyện', N'19 32 16N, 105 03 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (417, 40, N'Kỳ Sơn', N'Huyện', N'19 24 36N, 104 09 45E')
GO
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (418, 40, N'Tương Dương', N'Huyện', N'19 19 15N, 104 35 41E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (419, 40, N'Nghĩa Đàn', N'Huyện', N'19 21 19N, 105 26 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (420, 40, N'Quỳ Hợp', N'Huyện', N'19 19 24N, 105 09 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (421, 40, N'Quỳnh Lưu', N'Huyện', N'19 14 01N, 105 37 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (422, 40, N'Con Cuông', N'Huyện', N'19 03 50N, 107 47 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (423, 40, N'Tân Kỳ', N'Huyện', N'19 06 11N, 105 14 14E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (424, 40, N'Anh Sơn', N'Huyện', N'18 58 04N, 105 04 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (425, 40, N'Diễn Châu', N'Huyện', N'19 01 20N, 105 34 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (426, 40, N'Yên Thành', N'Huyện', N'19 01 25N, 105 26 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (427, 40, N'Đô Lương', N'Huyện', N'18 55 00N, 105 21 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (428, 40, N'Thanh Chương', N'Huyện', N'18 44 11N, 105 12 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (429, 40, N'Nghi Lộc', N'Huyện', N'18 47 41N, 105 31 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (430, 40, N'Nam Đàn', N'Huyện', N'18 40 28N, 105 30 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (431, 40, N'Hưng Nguyên', N'Huyện', N'18 41 13N, 105 37 41E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (436, 42, N'Hà Tĩnh', N'Thành Phố', N'18 21 20N, 105 54 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (437, 42, N'Hồng Lĩnh', N'Thị Xã', N'18 32 05N, 105 42 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (439, 42, N'Hương Sơn', N'Huyện', N'18 26 47N, 105 19 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (440, 42, N'Đức Thọ', N'Huyện', N'18 29 23N, 105 36 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (441, 42, N'Vũ Quang', N'Huyện', N'18 19 30N, 105 26 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (442, 42, N'Nghi Xuân', N'Huyện', N'18 38 46N, 105 46 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (443, 42, N'Can Lộc', N'Huyện', N'18 26 39N, 105 46 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (444, 42, N'Hương Khê', N'Huyện', N'18 11 36N, 105 41 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (445, 42, N'Thạch Hà', N'Huyện', N'18 19 29N, 105 52 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (446, 42, N'Cẩm Xuyên', N'Huyện', N'18 11 32N, 106 00 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (447, 42, N'Kỳ Anh', N'Huyện', N'18 05 15N, 106 15 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (448, 42, N'Lộc Hà', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (450, 44, N'Đồng Hới', N'Thành Phố', N'17 26 53N, 106 35 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (452, 44, N'Minh Hóa', N'Huyện', N'17 44 03N, 105 51 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (453, 44, N'Tuyên Hóa', N'Huyện', N'17 54 04N, 105 58 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (454, 44, N'Quảng Trạch', N'Huyện', N'17 50 04N, 106 22 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (455, 44, N'Bố Trạch', N'Huyện', N'17 29 13N, 106 06 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (456, 44, N'Quảng Ninh', N'Huyện', N'17 15 15N, 106 32 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (457, 44, N'Lệ Thủy', N'Huyện', N'17 07 35N, 106 41 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (461, 45, N'Đông Hà', N'Thành Phố', N'16 48 12N, 107 05 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (462, 45, N'Quảng Trị', N'Thị Xã', N'16 44 37N, 107 11 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (464, 45, N'Vĩnh Linh', N'Huyện', N'17 01 35N, 106 53 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (465, 45, N'Hướng Hóa', N'Huyện', N'16 42 19N, 106 40 14E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (466, 45, N'Gio Linh', N'Huyện', N'16 54 49N, 106 56 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (467, 45, N'Đa Krông', N'Huyện', N'16 33 58N, 106 55 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (468, 45, N'Cam Lộ', N'Huyện', N'16 47 09N, 106 57 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (469, 45, N'Triệu Phong', N'Huyện', N'16 46 32N, 107 09 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (470, 45, N'Hải Lăng', N'Huyện', N'16 41 07N, 107 13 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (471, 45, N'Cồn Cỏ', N'Huyện', N'19 09 39N, 107 19 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (474, 46, N'Huế', N'Thành Phố', N'16 27 16N, 107 34 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (476, 46, N'Phong Điền', N'Huyện', N'16 32 42N, 106 16 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (477, 46, N'Quảng Điền', N'Huyện', N'16 35 21N, 107 29 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (478, 46, N'Phú Vang', N'Huyện', N'16 27 20N, 107 42 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (479, 46, N'Hương Thủy', N'Huyện', N'16 19 27N, 107 37 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (480, 46, N'Hương Trà', N'Huyện', N'16 25 49N, 107 28 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (481, 46, N'A Lưới', N'Huyện', N'16 13 59N, 107 16 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (482, 46, N'Phú Lộc', N'Huyện', N'16 17 16N, 107 55 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (483, 46, N'Nam Đông', N'Huyện', N'16 07 11N, 107 41 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (490, 48, N'Liên Chiểu', N'Quận', N'16 07 26N, 108 07 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (491, 48, N'Thanh Khê', N'Quận', N'16 03 28N, 108 11 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (492, 48, N'Hải Châu', N'Quận', N'16 03 38N, 108 11 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (493, 48, N'Sơn Trà', N'Quận', N'16 06 13N, 108 16 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (494, 48, N'Ngũ Hành Sơn', N'Quận', N'16 00 30N, 108 15 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (495, 48, N'Cẩm Lệ', N'Quận', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (497, 48, N'Hoà Vang', N'Huyện', N'16 03 59N, 108 01 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (498, 48, N'Hoàng Sa', N'Huyện', N'16 21 36N, 111 57 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (502, 49, N'Tam Kỳ', N'Thành Phố', N'15 34 37N, 108 29 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (503, 49, N'Hội An', N'Thành Phố', N'15 53 20N, 108 20 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (504, 49, N'Tây Giang', N'Huyện', N'15 53 46N, 107 25 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (505, 49, N'Đông Giang', N'Huyện', N'15 54 44N, 107 47 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (506, 49, N'Đại Lộc', N'Huyện', N'15 50 10N, 107 58 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (507, 49, N'Điện Bàn', N'Huyện', N'15 54 15N, 108 13 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (508, 49, N'Duy Xuyên', N'Huyện', N'15 47 58N, 108 13 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (509, 49, N'Quế Sơn', N'Huyện', N'15 41 03N, 108 05 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (510, 49, N'Nam Giang', N'Huyện', N'15 36 37N, 107 33 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (511, 49, N'Phước Sơn', N'Huyện', N'15 23 17N, 107 50 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (512, 49, N'Hiệp Đức', N'Huyện', N'15 31 09N, 108 05 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (513, 49, N'Thăng Bình', N'Huyện', N'15 42 29N, 108 22 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (514, 49, N'Tiên Phước', N'Huyện', N'15 29 30N, 108 15 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (515, 49, N'Bắc Trà My', N'Huyện', N'15 08 00N, 108 05 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (516, 49, N'Nam Trà My', N'Huyện', N'15 16 40N, 108 12 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (517, 49, N'Núi Thành', N'Huyện', N'15 26 53N, 108 34 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (518, 49, N'Phú Ninh', N'Huyện', N'15 30 43N, 108 24 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (519, 49, N'Nông Sơn', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (522, 51, N'Quảng Ngãi', N'Thành Phố', N'15 07 17N, 108 48 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (524, 51, N'Bình Sơn', N'Huyện', N'15 18 45N, 108 45 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (525, 51, N'Trà Bồng', N'Huyện', N'15 13 30N, 108 29 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (526, 51, N'Tây Trà', N'Huyện', N'15 11 13N, 108 22 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (527, 51, N'Sơn Tịnh', N'Huyện', N'15 11 49N, 108 45 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (528, 51, N'Tư Nghĩa', N'Huyện', N'15 05 25N, 108 45 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (529, 51, N'Sơn Hà', N'Huyện', N'14 58 35N, 108 29 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (530, 51, N'Sơn Tây', N'Huyện', N'14 58 11N, 108 21 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (531, 51, N'Minh Long', N'Huyện', N'14 56 49N, 108 40 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (532, 51, N'Nghĩa Hành', N'Huyện', N'14 58 06N, 108 46 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (533, 51, N'Mộ Đức', N'Huyện', N'11 59 13N, 108 52 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (534, 51, N'Đức Phổ', N'Huyện', N'14 44 59N, 108 56 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (535, 51, N'Ba Tơ', N'Huyện', N'14 42 52N, 108 43 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (536, 51, N'Lý Sơn', N'Huyện', N'15 22 50N, 109 06 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (540, 52, N'Qui Nhơn', N'Thành Phố', N'13 47 15N, 109 12 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (542, 52, N'An Lão', N'Huyện', N'14 32 10N, 108 47 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (543, 52, N'Hoài Nhơn', N'Huyện', N'14 30 56N, 109 01 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (544, 52, N'Hoài Ân', N'Huyện', N'14 20 51N, 108 54 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (545, 52, N'Phù Mỹ', N'Huyện', N'14 14 41N, 109 05 43E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (546, 52, N'Vĩnh Thạnh', N'Huyện', N'14 14 26N, 108 44 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (547, 52, N'Tây Sơn', N'Huyện', N'13 56 47N, 108 53 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (548, 52, N'Phù Cát', N'Huyện', N'14 03 48N, 109 03 57E')
GO
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (549, 52, N'An Nhơn', N'Huyện', N'13 51 28N, 109 04 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (550, 52, N'Tuy Phước', N'Huyện', N'13 46 30N, 109 05 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (551, 52, N'Vân Canh', N'Huyện', N'13 40 35N, 108 57 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (555, 54, N'Tuy Hòa', N'Thành Phố', N'13 05 42N, 109 15 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (557, 54, N'Sông Cầu', N'Thị Xã', N'13 31 40N, 109 12 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (558, 54, N'Đồng Xuân', N'Huyện', N'13 24 59N, 108 56 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (559, 54, N'Tuy An', N'Huyện', N'13 15 19N, 109 12 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (560, 54, N'Sơn Hòa', N'Huyện', N'13 12 16N, 108 57 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (561, 54, N'Sông Hinh', N'Huyện', N'12 54 19N, 108 53 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (562, 54, N'Tây Hoà', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (563, 54, N'Phú Hoà', N'Huyện', N'13 04 07N, 109 11 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (564, 54, N'Đông Hoà', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (568, 56, N'Nha Trang', N'Thành Phố', N'12 15 40N, 109 10 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (569, 56, N'Cam Ranh', N'Thị Xã', N'11 59 05N, 108 08 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (570, 56, N'Cam Lâm', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (571, 56, N'Vạn Ninh', N'Huyện', N'12 43 10N, 109 11 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (572, 56, N'Ninh Hòa', N'Huyện', N'12 32 54N, 109 06 11E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (573, 56, N'Khánh Vĩnh', N'Huyện', N'12 17 50N, 108 51 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (574, 56, N'Diên Khánh', N'Huyện', N'12 13 19N, 109 02 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (575, 56, N'Khánh Sơn', N'Huyện', N'12 02 17N, 108 53 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (576, 56, N'Trường Sa', N'Huyện', N'9 07 27N, 114 15 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (582, 58, N'Phan Rang-Tháp Chàm', N'Thành Phố', N'11 36 11N, 108 58 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (584, 58, N'Bác Ái', N'Huyện', N'11 54 45N, 108 51 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (585, 58, N'Ninh Sơn', N'Huyện', N'11 42 36N, 108 44 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (586, 58, N'Ninh Hải', N'Huyện', N'11 42 46N, 109 05 41E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (587, 58, N'Ninh Phước', N'Huyện', N'11 28 56N, 108 50 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (588, 58, N'Thuận Bắc', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (589, 58, N'Thuận Nam', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (593, 60, N'Phan Thiết', N'Thành Phố', N'10 54 16N, 108 03 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (594, 60, N'La Gi', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (595, 60, N'Tuy Phong', N'Huyện', N'11 20 26N, 108 41 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (596, 60, N'Bắc Bình', N'Huyện', N'11 15 52N, 108 21 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (597, 60, N'Hàm Thuận Bắc', N'Huyện', N'11 09 18N, 108 03 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (598, 60, N'Hàm Thuận Nam', N'Huyện', N'10 56 20N, 107 54 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (599, 60, N'Tánh Linh', N'Huyện', N'11 08 26N, 107 41 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (600, 60, N'Đức Linh', N'Huyện', N'11 11 43N, 107 31 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (601, 60, N'Hàm Tân', N'Huyện', N'10 44 41N, 107 41 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (602, 60, N'Phú Quí', N'Huyện', N'10 33 13N, 108 57 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (608, 62, N'Kon Tum', N'Thành Phố', N'14 20 32N, 107 58 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (610, 62, N'Đắk Glei', N'Huyện', N'15 08 13N, 107 44 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (611, 62, N'Ngọc Hồi', N'Huyện', N'14 44 23N, 107 38 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (612, 62, N'Đắk Tô', N'Huyện', N'14 46 49N, 107 55 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (613, 62, N'Kon Plông', N'Huyện', N'14 48 13N, 108 20 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (614, 62, N'Kon Rẫy', N'Huyện', N'14 32 56N, 108 13 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (615, 62, N'Đắk Hà', N'Huyện', N'14 36 50N, 107 59 55E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (616, 62, N'Sa Thầy', N'Huyện', N'14 16 02N, 107 36 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (617, 62, N'Tu Mơ Rông', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (622, 64, N'Pleiku', N'Thành Phố', N'13 57 42N, 107 58 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (623, 64, N'An Khê', N'Thị Xã', N'14 01 24N, 108 41 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (624, 64, N'Ayun Pa', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (625, 64, N'Kbang', N'Huyện', N'14 18 08N, 108 29 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (626, 64, N'Đăk Đoa', N'Huyện', N'14 07 02N, 108 09 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (627, 64, N'Chư Păh', N'Huyện', N'14 13 30N, 107 56 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (628, 64, N'Ia Grai', N'Huyện', N'13 59 25N, 107 43 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (629, 64, N'Mang Yang', N'Huyện', N'13 57 26N, 108 18 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (630, 64, N'Kông Chro', N'Huyện', N'13 45 47N, 108 36 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (631, 64, N'Đức Cơ', N'Huyện', N'13 46 16N, 107 38 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (632, 64, N'Chư Prông', N'Huyện', N'13 35 39N, 107 47 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (633, 64, N'Chư Sê', N'Huyện', N'13 37 04N, 108 06 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (634, 64, N'Đăk Pơ', N'Huyện', N'13 55 47N, 108 36 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (635, 64, N'Ia Pa', N'Huyện', N'13 31 37N, 108 30 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (637, 64, N'Krông Pa', N'Huyện', N'13 14 13N, 108 39 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (638, 64, N'Phú Thiện', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (639, 64, N'Chư Pưh', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (643, 66, N'Buôn Ma Thuột', N'Thành Phố', N'12 39 43N, 108 10 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (644, 66, N'Buôn Hồ', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (645, 66, N'Ea H''leo', N'Huyện', N'13 13 52N, 108 12 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (646, 66, N'Ea Súp', N'Huyện', N'13 10 59N, 107 46 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (647, 66, N'Buôn Đôn', N'Huyện', N'12 52 45N, 107 45 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (648, 66, N'Cư M''gar', N'Huyện', N'12 53 47N, 108 04 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (649, 66, N'Krông Búk', N'Huyện', N'12 56 27N, 108 13 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (650, 66, N'Krông Năng', N'Huyện', N'12 59 41N, 108 23 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (651, 66, N'Ea Kar', N'Huyện', N'12 48 17N, 108 32 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (652, 66, N'M''đrắk', N'Huyện', N'12 42 14N, 108 47 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (653, 66, N'Krông Bông', N'Huyện', N'12 27 08N, 108 27 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (654, 66, N'Krông Pắc', N'Huyện', N'12 41 20N, 108 18 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (655, 66, N'Krông A Na', N'Huyện', N'12 31 51N, 108 05 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (656, 66, N'Lắk', N'Huyện', N'12 19 20N, 108 12 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (657, 66, N'Cư Kuin', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (660, 67, N'Gia Nghĩa', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (661, 67, N'Đắk Glong', N'Huyện', N'12 01 53N, 107 50 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (662, 67, N'Cư Jút', N'Huyện', N'12 40 56N, 107 44 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (663, 67, N'Đắk Mil', N'Huyện', N'12 31 08N, 107 42 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (664, 67, N'Krông Nô', N'Huyện', N'12 22 16N, 107 53 49E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (665, 67, N'Đắk Song', N'Huyện', N'12 14 04N, 107 36 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (666, 67, N'Đắk R''lấp', N'Huyện', N'12 02 30N, 107 25 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (667, 67, N'Tuy Đức', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (672, 68, N'Đà Lạt', N'Thành Phố', N'11 54 33N, 108 27 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (673, 68, N'Bảo Lộc', N'Thị Xã', N'11 32 48N, 107 47 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (674, 68, N'Đam Rông', N'Huyện', N'12 02 35N, 108 10 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (675, 68, N'Lạc Dương', N'Huyện', N'12 08 30N, 108 27 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (676, 68, N'Lâm Hà', N'Huyện', N'11 55 26N, 108 11 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (677, 68, N'Đơn Dương', N'Huyện', N'11 48 26N, 108 32 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (678, 68, N'Đức Trọng', N'Huyện', N'11 41 50N, 108 18 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (679, 68, N'Di Linh', N'Huyện', N'11 31 10N, 108 05 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (680, 68, N'Bảo Lâm', N'Huyện', N'11 38 31N, 107 43 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (681, 68, N'Đạ Huoai', N'Huyện', N'11 27 11N, 107 38 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (682, 68, N'Đạ Tẻh', N'Huyện', N'11 33 46N, 107 32 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (683, 68, N'Cát Tiên', N'Huyện', N'11 39 38N, 107 23 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (688, 70, N'Phước Long', N'Thị Xã', N'')
GO
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (689, 70, N'Đồng Xoài', N'Thị Xã', N'11 31 01N, 106 50 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (690, 70, N'Bình Long', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (691, 70, N'Bù Gia Mập', N'Huyện', N'11 56 57N, 106 59 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (692, 70, N'Lộc Ninh', N'Huyện', N'11 49 28N, 106 35 11E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (693, 70, N'Bù Đốp', N'Huyện', N'11 59 08N, 106 49 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (694, 70, N'Hớn Quản', N'Huyện', N'11 37 37N, 106 36 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (695, 70, N'Đồng Phù', N'Huyện', N'11 28 45N, 106 57 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (696, 70, N'Bù Đăng', N'Huyện', N'11 46 09N, 107 14 14E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (697, 70, N'Chơn Thành', N'Huyện', N'11 28 45N, 106 39 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (703, 72, N'Tây Ninh', N'Thị Xã', N'11 21 59N, 106 07 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (705, 72, N'Tân Biên', N'Huyện', N'11 35 14N, 105 57 53E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (706, 72, N'Tân Châu', N'Huyện', N'11 34 49N, 106 17 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (707, 72, N'Dương Minh Châu', N'Huyện', N'11 22 04N, 106 16 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (708, 72, N'Châu Thành', N'Huyện', N'11 19 02N, 106 00 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (709, 72, N'Hòa Thành', N'Huyện', N'11 15 31N, 106 08 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (710, 72, N'Gò Dầu', N'Huyện', N'11 09 39N, 106 14 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (711, 72, N'Bến Cầu', N'Huyện', N'11 07 50N, 106 08 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (712, 72, N'Trảng Bàng', N'Huyện', N'11 06 18N, 106 23 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (718, 74, N'Thủ Dầu Một', N'Thị Xã', N'11 00 01N, 106 38 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (720, 74, N'Dầu Tiếng', N'Huyện', N'11 19 07N, 106 26 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (721, 74, N'Bến Cát', N'Huyện', N'11 12 42N, 106 36 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (722, 74, N'Phú Giáo', N'Huyện', N'11 20 21N, 106 47 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (723, 74, N'Tân Uyên', N'Huyện', N'11 06 31N, 106 49 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (724, 74, N'Dĩ An', N'Huyện', N'10 55 03N, 106 47 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (725, 74, N'Thuận An', N'Huyện', N'10 55 58N, 106 41 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (731, 75, N'Biên Hòa', N'Thành Phố', N'10 56 31N, 106 50 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (732, 75, N'Long Khánh', N'Thị Xã', N'10 56 24N, 107 14 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (734, 75, N'Tân Phú', N'Huyện', N'11 22 51N, 107 21 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (735, 75, N'Vĩnh Cửu', N'Huyện', N'11 14 31N, 107 00 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (736, 75, N'Định Quán', N'Huyện', N'11 12 41N, 107 17 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (737, 75, N'Trảng Bom', N'Huyện', N'10 58 39N, 107 00 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (738, 75, N'Thống Nhất', N'Huyện', N'10 58 29N, 107 09 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (739, 75, N'Cẩm Mỹ', N'Huyện', N'10 47 05N, 107 14 36E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (740, 75, N'Long Thành', N'Huyện', N'10 47 38N, 106 59 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (741, 75, N'Xuân Lộc', N'Huyện', N'10 55 39N, 107 24 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (742, 75, N'Nhơn Trạch', N'Huyện', N'10 39 18N, 106 53 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (747, 77, N'Vũng Tầu', N'Thành Phố', N'10 24 08N, 107 07 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (748, 77, N'Bà Rịa', N'Thị Xã', N'10 30 33N, 107 10 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (750, 77, N'Châu Đức', N'Huyện', N'10 39 23N, 107 15 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (751, 77, N'Xuyên Mộc', N'Huyện', N'10 37 46N, 107 29 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (752, 77, N'Long Điền', N'Huyện', N'10 26 47N, 107 12 53E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (753, 77, N'Đất Đỏ', N'Huyện', N'10 28 40N, 107 18 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (754, 77, N'Tân Thành', N'Huyện', N'10 34 50N, 107 05 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (755, 77, N'Côn Đảo', N'Huyện', N'8 42 25N, 106 36 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (760, 1, N'Quận 1', N'Quận', N'10 46 34N, 106 41 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (761, 1, N'Quận 12', N'Quận', N'10 51 43N, 106 39 32E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (762, 1, N'Thủ Đức', N'Quận', N'10 51 20N, 106 45 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (763, 1, N'Quận 9', N'Quận', N'10 49 49N, 106 49 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (764, 1, N'Gò Vấp', N'Quận', N'10 50 12N, 106 39 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (765, 1, N'Bình Thạnh', N'Quận', N'10 48 46N, 106 42 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (766, 1, N'Tân Bình', N'Quận', N'10 48 13N, 106 39 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (767, 1, N'Tân Phú', N'Quận', N'10 47 32N, 106 37 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (768, 1, N'Phú Nhuận', N'Quận', N'10 48 06N, 106 40 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (769, 1, N'Quận 2', N'Quận', N'10 46 51N, 106 45 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (770, 1, N'Quận 3', N'Quận', N'10 46 48N, 106 40 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (771, 1, N'Quận 10', N'Quận', N'10 46 25N, 106 40 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (772, 1, N'Quận 11', N'Quận', N'10 46 01N, 106 38 44E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (773, 1, N'Quận 4', N'Quận', N'10 45 42N, 106 42 09E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (774, 1, N'Quận 5', N'Quận', N'10 45 24N, 106 40 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (775, 1, N'Quận 6', N'Quận', N'10 44 46N, 106 38 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (776, 1, N'Quận 8', N'Quận', N'10 43 24N, 106 37 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (777, 1, N'Bình Tân', N'Quận', N'10 46 16N, 106 35 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (778, 1, N'Quận 7', N'Quận', N'10 44 19N, 106 43 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (783, 1, N'Củ Chi', N'Huyện', N'11 02 17N, 106 30 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (784, 1, N'Hóc Môn', N'Huyện', N'10 52 42N, 106 35 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (785, 1, N'Bình Chánh', N'Huyện', N'10 45 01N, 106 30 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (786, 1, N'Nhà Bè', N'Huyện', N'10 39 06N, 106 43 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (787, 1, N'Cần Giờ', N'Huyện', N'10 30 43N, 106 52 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (794, 80, N'Tân An', N'Thành Phố', N'10 31 36N, 106 24 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (796, 80, N'Tân Hưng', N'Huyện', N'10 49 05N, 105 39 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (797, 80, N'Vĩnh Hưng', N'Huyện', N'10 52 54N, 105 45 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (798, 80, N'Mộc Hóa', N'Huyện', N'10 47 09N, 105 57 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (799, 80, N'Tân Thạnh', N'Huyện', N'10 37 44N, 105 57 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (800, 80, N'Thạnh Hóa', N'Huyện', N'10 41 37N, 106 11 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (801, 80, N'Đức Huệ', N'Huyện', N'10 51 57N, 106 15 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (802, 80, N'Đức Hòa', N'Huyện', N'10 53 04N, 106 23 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (803, 80, N'Bến Lức', N'Huyện', N'10 41 40N, 106 26 28E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (804, 80, N'Thủ Thừa', N'Huyện', N'10 39 41N, 106 20 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (805, 80, N'Tân Trụ', N'Huyện', N'10 31 47N, 106 30 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (806, 80, N'Cần Đước', N'Huyện', N'10 32 21N, 106 36 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (807, 80, N'Cần Giuộc', N'Huyện', N'10 34 43N, 106 38 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (808, 80, N'Châu Thành', N'Huyện', N'10 27 52N, 106 30 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (815, 82, N'Mỹ Tho', N'Thành Phố', N'10 22 14N, 106 21 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (816, 82, N'Gò Công', N'Thị Xã', N'10 21 55N, 106 40 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (818, 82, N'Tân Phước', N'Huyện', N'10 30 36N, 106 13 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (819, 82, N'Cái Bè', N'Huyện', N'10 24 21N, 105 56 01E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (820, 82, N'Cai Lậy', N'Huyện', N'10 24 20N, 106 06 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (821, 82, N'Châu Thành', N'Huyện', N'20 25 21N, 106 16 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (822, 82, N'Chợ Gạo', N'Huyện', N'10 23 45N, 106 26 53E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (823, 82, N'Gò Công Tây', N'Huyện', N'10 19 55N, 106 35 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (824, 82, N'Gò Công Đông', N'Huyện', N'10 20 42N, 106 42 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (825, 82, N'Tân Phú Đông', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (829, 83, N'Bến Tre', N'Thành Phố', N'10 14 17N, 106 22 26E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (831, 83, N'Châu Thành', N'Huyện', N'10 17 24N, 106 17 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (832, 83, N'Chợ Lách', N'Huyện', N'10 13 26N, 106 09 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (833, 83, N'Mỏ Cày Nam', N'Huyện', N'10 06 56N, 106 19 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (834, 83, N'Giồng Trôm', N'Huyện', N'10 08 46N, 106 28 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (835, 83, N'Bình Đại', N'Huyện', N'10 09 56N, 106 37 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (836, 83, N'Ba Tri', N'Huyện', N'10 04 08N, 106 35 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (837, 83, N'Thạnh Phú', N'Huyện', N'9 55 53N, 106 32 45E')
GO
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (838, 83, N'Mỏ Cày Bắc', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (842, 84, N'Trà Vinh', N'Thị Xã', N'9 57 09N, 106 20 39E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (844, 84, N'Càng Long', N'Huyện', N'9 58 18N, 106 12 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (845, 84, N'Cầu Kè', N'Huyện', N'9 51 23N, 106 03 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (846, 84, N'Tiểu Cần', N'Huyện', N'9 48 37N, 106 12 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (847, 84, N'Châu Thành', N'Huyện', N'9 52 57N, 106 24 12E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (848, 84, N'Cầu Ngang', N'Huyện', N'9 47 14N, 106 29 19E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (849, 84, N'Trà Cú', N'Huyện', N'9 42 06N, 106 16 24E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (850, 84, N'Duyên Hải', N'Huyện', N'9 39 58N, 106 26 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (855, 86, N'Vĩnh Long', N'Thành Phố', N'10 15 09N, 105 56 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (857, 86, N'Long Hồ', N'Huyện', N'10 13 58N, 105 55 47E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (858, 86, N'Mang Thít', N'Huyện', N'10 10 58N, 106 05 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (859, 86, N'Vũng Liêm', N'Huyện', N'10 03 32N, 106 10 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (860, 86, N'Tam Bình', N'Huyện', N'10 03 58N, 105 58 03E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (861, 86, N'Bình Minh', N'Huyện', N'10 05 45N, 105 47 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (862, 86, N'Trà Ôn', N'Huyện', N'9 59 20N, 105 57 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (863, 86, N'Bình Tân', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (866, 87, N'Cao Lãnh', N'Thành Phố', N'10 27 38N, 105 37 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (867, 87, N'Sa Đéc', N'Thị Xã', N'10 19 22N, 105 44 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (868, 87, N'Hồng Ngự', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (869, 87, N'Tân Hồng', N'Huyện', N'10 52 48N, 105 29 21E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (870, 87, N'Hồng Ngự', N'Huyện', N'10 48 13N, 105 19 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (871, 87, N'Tam Nông', N'Huyện', N'10 44 06N, 105 30 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (872, 87, N'Tháp Mười', N'Huyện', N'10 33 36N, 105 47 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (873, 87, N'Cao Lãnh', N'Huyện', N'10 29 03N, 105 41 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (874, 87, N'Thanh Bình', N'Huyện', N'10 36 38N, 105 28 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (875, 87, N'Lấp Vò', N'Huyện', N'10 21 27N, 105 36 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (876, 87, N'Lai Vung', N'Huyện', N'10 14 43N, 105 38 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (877, 87, N'Châu Thành', N'Huyện', N'10 13 27N, 105 48 38E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (883, 89, N'Long Xuyên', N'Thành Phố', N'10 22 22N, 105 25 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (884, 89, N'Châu Đốc', N'Thị Xã', N'10 41 20N, 105 05 15E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (886, 89, N'An Phú', N'Huyện', N'10 50 12N, 105 05 33E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (887, 89, N'Tân Châu', N'Thị Xã', N'10 49 11N, 105 11 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (888, 89, N'Phú Tân', N'Huyện', N'10 40 26N, 105 14 40E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (889, 89, N'Châu Phú', N'Huyện', N'10 34 12N, 105 12 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (890, 89, N'Tịnh Biên', N'Huyện', N'10 33 30N, 105 00 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (891, 89, N'Tri Tôn', N'Huyện', N'10 24 41N, 104 56 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (892, 89, N'Châu Thành', N'Huyện', N'10 25 39N, 105 15 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (893, 89, N'Chợ Mới', N'Huyện', N'10 27 23N, 105 26 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (894, 89, N'Thoại Sơn', N'Huyện', N'10 16 45N, 105 15 59E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (899, 91, N'Rạch Giá', N'Thành Phố', N'10 01 35N, 105 06 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (900, 91, N'Hà Tiên', N'Thị Xã', N'10 22 54N, 104 30 10E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (902, 91, N'Kiên Lương', N'Huyện', N'10 20 21N, 104 39 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (903, 91, N'Hòn Đất', N'Huyện', N'10 14 20N, 104 55 57E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (904, 91, N'Tân Hiệp', N'Huyện', N'10 05 18N, 105 14 04E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (905, 91, N'Châu Thành', N'Huyện', N'9 57 37N, 105 10 16E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (906, 91, N'Giồng Giềng', N'Huyện', N'9 56 05N, 105 22 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (907, 91, N'Gò Quao', N'Huyện', N'9 43 17N, 105 17 06E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (908, 91, N'An Biên', N'Huyện', N'9 48 37N, 105 03 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (909, 91, N'An Minh', N'Huyện', N'9 40 24N, 104 59 05E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (910, 91, N'Vĩnh Thuận', N'Huyện', N'9 33 25N, 105 11 30E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (911, 91, N'Phú Quốc', N'Huyện', N'10 13 44N, 103 57 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (912, 91, N'Kiên Hải', N'Huyện', N'9 48 31N, 104 37 48E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (913, 91, N'U Minh Thượng', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (914, 91, N'Giang Thành', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (916, 92, N'Ninh Kiều', N'Quận', N'10 01 58N, 105 45 34E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (917, 92, N'Ô Môn', N'Quận', N'10 07 28N, 105 37 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (918, 92, N'Bình Thuỷ', N'Quận', N'10 03 42N, 105 43 17E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (919, 92, N'Cái Răng', N'Quận', N'9 59 57N, 105 46 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (923, 92, N'Thốt Nốt', N'Quận', N'10 14 23N, 105 32 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (924, 92, N'Vĩnh Thạnh', N'Huyện', N'10 11 35N, 105 22 45E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (925, 92, N'Cờ Đỏ', N'Huyện', N'10 02 48N, 105 29 46E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (926, 92, N'Phong Điền', N'Huyện', N'9 59 57N, 105 39 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (927, 92, N'Thới Lai', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (930, 93, N'Vị Thanh', N'Thị Xã', N'9 45 15N, 105 24 50E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (931, 93, N'Ngã Bảy', N'Thị Xã', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (932, 93, N'Châu Thành A', N'Huyện', N'9 55 50N, 105 38 31E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (933, 93, N'Châu Thành', N'Huyện', N'9 55 22N, 105 48 37E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (934, 93, N'Phụng Hiệp', N'Huyện', N'9 47 20N, 105 43 29E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (935, 93, N'Vị Thuỷ', N'Huyện', N'9 48 05N, 105 32 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (936, 93, N'Long Mỹ', N'Huyện', N'9 40 47N, 105 30 53E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (941, 94, N'Sóc Trăng', N'Thành Phố', N'9 36 39N, 105 59 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (942, 94, N'Châu Thành', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (943, 94, N'Kế Sách', N'Huyện', N'9 49 30N, 105 57 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (944, 94, N'Mỹ Tú', N'Huyện', N'9 38 21N, 105 49 52E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (945, 94, N'Cù Lao Dung', N'Huyện', N'9 37 36N, 106 12 13E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (946, 94, N'Long Phú', N'Huyện', N'9 34 38N, 106 06 07E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (947, 94, N'Mỹ Xuyên', N'Huyện', N'9 28 16N, 105 55 51E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (948, 94, N'Ngã Năm', N'Huyện', N'9 31 38N, 105 37 22E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (949, 94, N'Thạnh Trị', N'Huyện', N'9 28 05N, 105 43 02E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (950, 94, N'Vĩnh Châu', N'Huyện', N'9 20 50N, 105 59 58E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (951, 94, N'Trần Đề', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (954, 95, N'Bạc Liêu', N'Thị Xã', N'9 16 05N, 105 45 08E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (956, 95, N'Hồng Dân', N'Huyện', N'9 30 37N, 105 24 25E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (957, 95, N'Phước Long', N'Huyện', N'9 23 13N, 105 24 41E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (958, 95, N'Vĩnh Lợi', N'Huyện', N'9 16 51N, 105 40 54E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (959, 95, N'Giá Rai', N'Huyện', N'9 15 51N, 105 23 18E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (960, 95, N'Đông Hải', N'Huyện', N'9 08 11N, 105 24 42E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (961, 95, N'Hoà Bình', N'Huyện', N'')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (964, 96, N'Cà Mau', N'Thành Phố', N'9 10 33N, 105 11 11E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (966, 96, N'U Minh', N'Huyện', N'9 22 30N, 104 57 00E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (967, 96, N'Thới Bình', N'Huyện', N'9 22 50N, 105 07 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (968, 96, N'Trần Văn Thời', N'Huyện', N'9 07 36N, 104 57 27E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (969, 96, N'Cái Nước', N'Huyện', N'9 00 31N, 105 03 23E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (970, 96, N'Đầm Dơi', N'Huyện', N'8 57 48N, 105 13 56E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (971, 96, N'Năm Căn', N'Huyện', N'8 46 59N, 104 58 20E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (972, 96, N'Phú Tân', N'Huyện', N'8 52 47N, 104 53 35E')
INSERT [dbo].[Core_District] ([Id], [StateOrProvinceId], [Name], [Type], [Location]) VALUES (973, 96, N'Ngọc Hiển', N'Huyện', N'8 40 47N, 104 57 58E')
SET IDENTITY_INSERT [dbo].[Core_District] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_Entity] ON 

INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (1, N'nam', N'Nam', 1, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (2, N'tennis', N'Tennis', 2, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (3, N'bong-da-2', N'Bóng đá', 3, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (4, N'nu', N'Nữ', 4, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (5, N'phong-tap-the-duc-va-ren-luyen', N'Phòng tập thể dục và rèn luyện', 5, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (6, N'di-bo', N'Đi bộ', 6, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (7, N'tre-em', N'Trẻ em', 7, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (8, N'young-kids', N'Young Kids', 8, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (9, N'older-kids', N'Older Kids', 9, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (10, N'khac', N'Khác', 10, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (11, N'chay-bo', N'Chạy bộ', 11, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (12, N'bong-da', N'Bóng đá', 12, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (13, N'tu-do', N'Tự do', 13, N'Category')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (18, N'about-us', N'Về chúng tôi', 1, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (19, N'terms-of-use', N'
Điều khoản sử dụng', 2, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (20, N'privacy', N'Bảo mật', 3, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (21, N'help-center', N'Trung tâm chăm sóc khách hàng', 4, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (22, N'help-center/how-to-buy', N'Tại sao mua hàng', 5, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (23, N'help-center/shipping', N'Giao hàng', 6, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (24, N'help-center/how-to-return', N'Trở về', 7, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (25, N'help-center/warranty', N'Sự bảo đảm', 8, N'Page')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (29, N'nike', N'Nike', 1, N'Brand')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (30, N'samsung', N'Samsung', 2, N'Brand')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (31, N'dell', N'Dell', 3, N'Brand')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (35, N'air-max-90-city-pack', N'AIR MAX 90 CITY PACK', 15, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (36, N'air-max-270-react', N'AIR MAX 270 REACT', 16, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (37, N'zx-2k-boost', N'ZX 2K BOOST', 17, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (38, N'jordan-1-low-cactus', N'JORDAN 1 LOW CACTUS', 18, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (39, N'sneakers-puma-rs-x-hard-drive', N'Sneakers Puma RS-X Hard Drive', 19, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (40, N'xsport-adi.das-alpha.bounce-instinct-m', N'XSPORT Adi.das Alpha.bounce Instinct M', 20, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (41, N'xsport-ni.ke-air-max-97', N'XSPORT Ni.ke Air Max 97', 21, N'Product')
INSERT [dbo].[Core_Entity] ([Id], [Slug], [Name], [EntityId], [EntityTypeId]) VALUES (42, N'nike-air-max-2', N'Nike Air Max 2', 22, N'Product')
SET IDENTITY_INSERT [dbo].[Core_Entity] OFF
GO
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'Brand', 1, N'Catalog', N'Brand', N'BrandDetail')
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'Category', 1, N'Catalog', N'Category', N'CategoryDetail')
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'NewsCategory', 1, N'News', N'NewsCategory', N'NewsCategoryDetail')
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'NewsItem', 0, N'News', N'NewsItem', N'NewsItemDetail')
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'Page', 1, N'Cms', N'Page', N'PageDetail')
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'Product', 0, N'Catalog', N'Product', N'ProductDetail')
INSERT [dbo].[Core_EntityType] ([Id], [IsMenuable], [AreaName], [RoutingController], [RoutingAction]) VALUES (N'Vendor', 0, N'Core', N'Vendor', N'VendorDetail')
GO
SET IDENTITY_INSERT [dbo].[Core_Media] ON 

INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (1, NULL, 0, N'd74fd909-6fe0-4bc3-bf61-86d12dc98a2e.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (2, NULL, 0, N'81b606ea-0bb0-4cea-a9d7-6406175df9bb.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (3, NULL, 0, N'68c7ff8f-014e-46c8-8daa-f35c646cc10a.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (4, NULL, 0, N'89374e88-b14c-4d38-b5cd-eacdc5ce3015.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (5, NULL, 0, N'5119ade9-49fe-449f-aa42-c49a1e6134f6.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (10, NULL, 0, N'b37d564c-0089-4e7f-9329-ee415807f9c2.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (16, NULL, 0, N'd041917c-1fbf-49ec-8c7d-19c36a37b56f.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (25, NULL, 0, N'e0eb4727-15a9-41d8-874a-c925fa7ef289.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (26, NULL, 0, N'9ee92aaf-364e-452f-9a8b-c1c2d00cd0c0.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (27, NULL, 0, N'1fcb63e1-e806-4821-92d2-eea8f9cff80f.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (28, NULL, 0, N'fcb7d929-9257-4268-a7a8-f75616c57e39.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (33, NULL, 0, N'f0a37bbb-92e7-4632-a2da-064c8d9ad37f.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (34, NULL, 0, N'c0365d15-485a-4c97-b70b-f150209d40dc.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (35, NULL, 0, N'f374a7f3-bf82-4246-8898-ba3356ea8a07.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (36, NULL, 0, N'fba33a3b-1171-471a-9a94-2a65287d1cd7.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (37, NULL, 0, N'eb556215-ae93-4920-82c0-74e21c64c8fa.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (38, NULL, 0, N'71abcbd1-0571-4ad7-976b-97008b8987dd.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (39, NULL, 0, N'd7514527-abff-4db7-93c4-bfc545f2bf12.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (40, NULL, 0, N'9e07a601-497f-4856-9d7f-3ded845e24a8.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (41, NULL, 0, N'668649af-d4ae-4cbf-82fd-01d5c414885e.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (42, NULL, 0, N'e19ed9a9-33e5-4e77-897e-a48e76d973fe.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (43, NULL, 0, N'56908c33-5801-4655-84a0-65549cebe0a6.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (44, NULL, 0, N'7bdcda2d-2c16-4bf2-9ce8-f62fbac658f1.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (45, NULL, 0, N'af294708-288d-4cf1-a1da-b086f26827b5.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (46, NULL, 0, N'425a0771-6c4e-4f97-982c-35eac8f9183b.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (47, NULL, 0, N'c10b53db-568d-4188-8173-3f6e5e87b5f7.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (48, NULL, 0, N'd1408017-999f-438c-9346-4f0345036b16.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (53, NULL, 0, N'e2432fca-facb-44b8-a105-497ca28dd3ef.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (58, NULL, 0, N'75506b6b-5a5b-40ef-89a3-38e9fe580cfe.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (63, NULL, 0, N'6f80ea8a-edc1-47f8-bc16-9238a4b92ceb.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (68, NULL, 0, N'37230390-d199-4357-a493-63927c87c037.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (85, NULL, 0, N'93667153-25b6-4fb3-bdfc-ffe63d3f4116.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (86, NULL, 0, N'6a5ebd63-2a53-4d0e-b6f6-179633b9e995.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (87, NULL, 0, N'15ddc640-ec4e-4fac-ab2a-13b21ba9fe02.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (88, NULL, 0, N'9799a8cf-0a6c-477f-8c28-3905ae541082.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (89, NULL, 0, N'ed34d5c3-0a4e-47fb-9c99-533ae148d3f0.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (90, NULL, 0, N'ecae8487-2ec6-4e03-acdc-5f7813bb1c8a.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (91, NULL, 0, N'94a10f92-cdda-40fc-aa71-becd26df7a38.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (92, NULL, 0, N'76d47695-0fb7-4641-8829-f489b0d9f2e5.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (93, NULL, 0, N'1dbed583-0e64-4a21-9f98-6888d4a18d18.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (94, NULL, 0, N'aaaef528-a77e-42e2-9338-9ad9cb803f41.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (95, NULL, 0, N'ca21beb0-4e53-4f74-aaef-4c0a217e49d5.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (96, NULL, 0, N'd9f61952-43ad-4cb7-ae36-7384075938e1.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (97, NULL, 0, N'7ddf4cbe-0d2e-4dd2-8b25-763f3bb086fb.png', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (98, NULL, 0, N'b57f6972-f832-4cb9-9d85-c91787229da2.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (99, NULL, 0, N'e6419f1d-1b5a-442a-913e-08d2424f6c3b.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (100, NULL, 0, N'556c3e12-ddaa-4008-a42a-8b4c11eec198.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (101, NULL, 0, N'41acd80e-d6e9-4ac2-a5c7-3facb8e861db.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (105, NULL, 0, N'b00a26c6-6dd1-4d17-bc05-839c2f8dce85.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (114, NULL, 0, N'5daa3a00-7ba7-47b2-9e7e-5afb1fb84535.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (119, NULL, 0, N'cc92ab46-1db6-4d94-a744-a1f8e58e06f8.png', 0)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (120, NULL, 0, N'6fd2263e-e041-473d-8b61-fb5375fe3328.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (121, NULL, 0, N'8637be43-f2d2-4b85-83f6-12d4caa2d47d.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (122, NULL, 0, N'50125b3d-77c3-4a95-9123-8ce974ee2dd4.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (123, NULL, 0, N'eab6d05b-1b67-477a-8944-45606eeb919f.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (124, NULL, 0, N'8d2ffbbe-de02-481e-b56c-95df5e9f4117.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (125, NULL, 0, N'60743e5f-7e84-49cf-b2e5-8faae2e69754.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (126, NULL, 0, N'06d94b2c-973d-4b97-a3b5-c4b43c0fecc2.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (127, NULL, 0, N'9e2d71bd-c7d3-4bd4-a53c-76d5b0a27b40.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (132, NULL, 0, N'86691dae-e101-4ec5-89af-68a18f97c5c6.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (133, NULL, 0, N'29a34683-3915-4c57-a06f-0fcd98a33e43.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (134, NULL, 0, N'd3957514-449c-49d7-919f-4d316551e131.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (135, NULL, 0, N'6405c7bd-befc-44d8-9dbf-17ff85747a96.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (136, NULL, 0, N'c3884714-7aa6-4314-add7-2e700e0ea2e2.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (137, NULL, 0, N'4c7b15f9-81ec-45a2-b7cd-a7a58299aa82.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (138, NULL, 0, N'000e2f7b-4784-4aed-84d4-244e0178130f.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (139, NULL, 0, N'39f89cab-104d-40ad-a357-6b8139c0ec75.jpg', 1)
INSERT [dbo].[Core_Media] ([Id], [Caption], [FileSize], [FileName], [MediaType]) VALUES (140, NULL, 0, N'f48db8e1-69fb-417b-a100-4f6b397f2f30.jpg', 0)
SET IDENTITY_INSERT [dbo].[Core_Media] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_Role] ON 

INSERT [dbo].[Core_Role] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (1, N'admin', N'ADMIN', N'4776a1b2-dbe4-4056-82ec-8bed211d1454')
INSERT [dbo].[Core_Role] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (2, N'customer', N'CUSTOMER', N'00d172be-03a0-4856-8b12-26d63fcf4374')
INSERT [dbo].[Core_Role] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (3, N'guest', N'GUEST', N'd4754388-8355-4018-b728-218018836817')
INSERT [dbo].[Core_Role] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (4, N'vendor', N'VENDOR', N'71f10604-8c4d-4a7d-ac4a-ffefb11cefeb')
SET IDENTITY_INSERT [dbo].[Core_Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_StateOrProvince] ON 

INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (1, N'VN', NULL, N'Hồ Chí Minh', N'Thành Phố')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (2, N'VN', NULL, N'Hà Giang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (4, N'VN', NULL, N'Cao Bằng', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (6, N'VN', NULL, N'Bắc Kạn', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (8, N'VN', NULL, N'Tuyên Quang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (10, N'VN', NULL, N'Lào Cai', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (11, N'VN', NULL, N'Điện Biên', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (12, N'VN', NULL, N'Lai Châu', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (14, N'VN', NULL, N'Sơn La', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (15, N'VN', NULL, N'Yên Bái', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (17, N'VN', NULL, N'Hòa Bình', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (19, N'VN', NULL, N'Thái Nguyên', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (20, N'VN', NULL, N'Lạng Sơn', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (22, N'VN', NULL, N'Quảng Ninh', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (24, N'VN', NULL, N'Bắc Giang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (25, N'VN', NULL, N'Phú Thọ', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (26, N'VN', NULL, N'Vĩnh Phúc', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (27, N'VN', NULL, N'Bắc Ninh', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (30, N'VN', NULL, N'Hải Dương', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (31, N'VN', NULL, N'Hải Phòng', N'Thành Phố')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (33, N'VN', NULL, N'Hưng Yên', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (34, N'VN', NULL, N'Thái Bình', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (35, N'VN', NULL, N'Hà Nam', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (36, N'VN', NULL, N'Nam Định', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (37, N'VN', NULL, N'Ninh Bình', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (38, N'VN', NULL, N'Thanh Hóa', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (40, N'VN', NULL, N'Nghệ An', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (42, N'VN', NULL, N'Hà Tĩnh', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (44, N'VN', NULL, N'Quảng Bình', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (45, N'VN', NULL, N'Quảng Trị', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (46, N'VN', NULL, N'Thừa Thiên Huế', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (48, N'VN', NULL, N'Đà Nẵng', N'Thành Phố')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (49, N'VN', NULL, N'Quảng Nam', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (51, N'VN', NULL, N'Quảng Ngãi', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (52, N'VN', NULL, N'Bình Định', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (54, N'VN', NULL, N'Phú Yên', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (56, N'VN', NULL, N'Khánh Hòa', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (58, N'VN', NULL, N'Ninh Thuận', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (60, N'VN', NULL, N'Bình Thuận', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (62, N'VN', NULL, N'Kon Tum', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (64, N'VN', NULL, N'Gia Lai', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (66, N'VN', NULL, N'Đắk Lắk', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (67, N'VN', NULL, N'Đắk Nông', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (68, N'VN', NULL, N'Lâm Đồng', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (70, N'VN', NULL, N'Bình Phước', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (72, N'VN', NULL, N'Tây Ninh', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (74, N'VN', NULL, N'Bình Dương', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (75, N'VN', NULL, N'Đồng Nai', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (77, N'VN', NULL, N'Bà Rịa - Vũng Tàu', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (79, N'VN', NULL, N'Hà Nội', N'Thành Phố')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (80, N'VN', NULL, N'Long An', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (82, N'VN', NULL, N'Tiền Giang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (83, N'VN', NULL, N'Bến Tre', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (84, N'VN', NULL, N'Trà Vinh', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (86, N'VN', NULL, N'Vĩnh Long', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (87, N'VN', NULL, N'Đồng Tháp', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (89, N'VN', NULL, N'An Giang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (91, N'VN', NULL, N'Kiên Giang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (92, N'VN', NULL, N'Cần Thơ', N'Thành Phố')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (93, N'VN', NULL, N'Hậu Giang', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (94, N'VN', NULL, N'Sóc Trăng', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (95, N'VN', NULL, N'Bạc Liêu', N'Tỉnh')
INSERT [dbo].[Core_StateOrProvince] ([Id], [CountryId], [Code], [Name], [Type]) VALUES (96, N'VN', NULL, N'Cà Mau', N'Tỉnh')
SET IDENTITY_INSERT [dbo].[Core_StateOrProvince] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_User] ON 

INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (2, N'banhang@gmail.com', N'BANHANG@GMAIL.COM', N'banhang@gmail.com', N'BANHANG@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEKV4KiccT0wxJcvrhNxQ9dGroHNCNgmAEdJFmpevA8EUnkHrkSHB/+2Es2gnXhUoQA==', N'2MCKDJZHW7GEFSW46TBDFH7XMTCORDME', N'f74f876c-8899-45bb-99d7-1afcf00f12d1', NULL, 0, 0, NULL, 0, 0, N'5f72f83b-7436-4221-869c-1b69b2e23aae', N'System User', NULL, 1, CAST(N'2018-05-29T04:33:39.1890000+07:00' AS DateTimeOffset), CAST(N'2018-05-29T04:33:39.1890000+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en-US', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (10, N'admin@gmail.com', N'ADMIN@GMAIL.COM', N'admin@gmail.com', N'ADMIN@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEFGCW4/0rZ7my7n3PWd7Ad4EdB1B49xNgxLt5iE3cLWVfFpbdU4trnyvpdFQIiLJ+A==', N'RLVQXRDOIXQW3KYJGN4EXBDMAI6CF2FX', N'd38bf46c-b78a-42b3-924c-195325a2cbba', NULL, 0, 0, NULL, 1, 0, N'ed8210c3-24b0-4823-a744-80078cf12eb4', N'Shop Admin', NULL, 1, CAST(N'2018-05-29T04:33:39.1900000+07:00' AS DateTimeOffset), CAST(N'2018-05-29T04:33:39.1900000+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'vi', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (546, N'113f8b7b-bdf3-44c4-af27-b69d7cc05807@guest.simplcommerce.com', N'113F8B7B-BDF3-44C4-AF27-B69D7CC05807@GUEST.SIMPLCOMMERCE.COM', N'113f8b7b-bdf3-44c4-af27-b69d7cc05807@guest.simplcommerce.com', N'113F8B7B-BDF3-44C4-AF27-B69D7CC05807@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAEIXBmA+f6FxB+kpXDvgo2uVSs2PF60RqOMdNLy+31U3VmCISS5U24HzgLw8BCS+yjQ==', N'RTZUN7ZHW423YH2KYVG5ITU2WE5A52JV', N'2361ab6b-29d9-4f23-ad85-35428e377688', NULL, 0, 0, CAST(N'2220-12-25T23:31:47.2244619+07:00' AS DateTimeOffset), 1, 0, N'113f8b7b-bdf3-44c4-af27-b69d7cc05807', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:45.3510446+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:45.3510484+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (547, N'2ced163d-fe91-430f-9981-eae5983da120@guest.simplcommerce.com', N'2CED163D-FE91-430F-9981-EAE5983DA120@GUEST.SIMPLCOMMERCE.COM', N'2ced163d-fe91-430f-9981-eae5983da120@guest.simplcommerce.com', N'2CED163D-FE91-430F-9981-EAE5983DA120@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAEHvuPTlPcZXNQU+W2lamF3EU3jheAYqccG9g+UpBCFEKk9fIiUDcUSqTo+pCiDq0bg==', N'6VEIM6RH63FDVMB537YJJDQ64NZN25QM', N'c9293ab1-9a05-4b1c-a590-19b146628050', NULL, 0, 0, CAST(N'2220-12-25T23:31:45.3389163+07:00' AS DateTimeOffset), 1, 0, N'2ced163d-fe91-430f-9981-eae5983da120', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:48.1166432+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:48.1166543+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (548, N'1697796e-3b6e-4e19-8936-e1ffdbdbe8bf@guest.simplcommerce.com', N'1697796E-3B6E-4E19-8936-E1FFDBDBE8BF@GUEST.SIMPLCOMMERCE.COM', N'1697796e-3b6e-4e19-8936-e1ffdbdbe8bf@guest.simplcommerce.com', N'1697796E-3B6E-4E19-8936-E1FFDBDBE8BF@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAEFR/1SfVRV3G9rlynP+EJNxL9CvqmT3ooXLx0FZdBVH+oYXIm00GfV8UrP3UqFl7Og==', N'2L5ASRZXTSZWRADRWV4AVZTP4BB4KTUE', N'f1d27036-7796-45a2-aae9-6478a0414956', NULL, 0, 0, CAST(N'2220-12-25T23:31:43.6547240+07:00' AS DateTimeOffset), 1, 0, N'1697796e-3b6e-4e19-8936-e1ffdbdbe8bf', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:48.4722993+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:48.4723079+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (549, N'c300c1b1-e815-4d21-a631-6dd850436be3@guest.simplcommerce.com', N'C300C1B1-E815-4D21-A631-6DD850436BE3@GUEST.SIMPLCOMMERCE.COM', N'c300c1b1-e815-4d21-a631-6dd850436be3@guest.simplcommerce.com', N'C300C1B1-E815-4D21-A631-6DD850436BE3@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAEGZf6hSNERROVnvtFryuhp9VUPBpc6vLziMPyJYpCW/gSkSIszgD0amPC7VFgZICMQ==', N'VTPE7YKVSWZEJFH657FFGRTCGY7OY6FZ', N'fdffc64c-d2bb-4c14-a51a-aef5d4f3a655', NULL, 0, 0, CAST(N'2220-12-25T23:31:41.7001283+07:00' AS DateTimeOffset), 1, 0, N'c300c1b1-e815-4d21-a631-6dd850436be3', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:48.4729479+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:48.4729520+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (550, N'266cc874-63da-4503-85bc-8851301d2b99@guest.simplcommerce.com', N'266CC874-63DA-4503-85BC-8851301D2B99@GUEST.SIMPLCOMMERCE.COM', N'266cc874-63da-4503-85bc-8851301d2b99@guest.simplcommerce.com', N'266CC874-63DA-4503-85BC-8851301D2B99@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAEKDxfWbdBr7zQ/UAb0/wdFk7f7R5T1PwAgh5sjr8QiIkZ+HRfKqKjQJpmN8+jkiObA==', N'XAUS322IDWGICSRX3H3DODU33BITBNUK', N'b1a55858-1184-4b7a-8bba-b0972b0d8559', NULL, 0, 0, CAST(N'2220-12-25T23:31:39.0213912+07:00' AS DateTimeOffset), 1, 0, N'266cc874-63da-4503-85bc-8851301d2b99', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:50.5453807+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:50.5453887+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (551, N'c961f4b8-6e93-4f00-ac05-ff5fa769c375@guest.simplcommerce.com', N'C961F4B8-6E93-4F00-AC05-FF5FA769C375@GUEST.SIMPLCOMMERCE.COM', N'c961f4b8-6e93-4f00-ac05-ff5fa769c375@guest.simplcommerce.com', N'C961F4B8-6E93-4F00-AC05-FF5FA769C375@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAEC3huuKREW2LZmN5jhEWFNe8isqD1MlXQMZ2R9VJGhefJKFm1mRoCDQphIQBrvRtGg==', N'YULIQ2FQZVPPBFZLDWUHQN7KUGJACF7B', N'f3399067-3cc4-4625-b49e-2007bd0589bf', NULL, 0, 0, CAST(N'2220-12-25T23:31:36.2796455+07:00' AS DateTimeOffset), 1, 0, N'c961f4b8-6e93-4f00-ac05-ff5fa769c375', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:50.8590689+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:50.8590743+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (552, N'298f4eca-2a0e-446c-8a7e-780bba8a3ddf@guest.simplcommerce.com', N'298F4ECA-2A0E-446C-8A7E-780BBA8A3DDF@GUEST.SIMPLCOMMERCE.COM', N'298f4eca-2a0e-446c-8a7e-780bba8a3ddf@guest.simplcommerce.com', N'298F4ECA-2A0E-446C-8A7E-780BBA8A3DDF@GUEST.SIMPLCOMMERCE.COM', 0, N'AQAAAAEAACcQAAAAED24zrBwveXnQM7W+SDLn6QMMlSfbe0xnXF42P9bYl67HodS1ucXeoCNnekQg6seVA==', N'XDSPZD3ITEFNQGFTMG46BEH4DKMNI3QJ', N'ba550a6b-7d6f-4967-9f57-2951de146f36', NULL, 0, 0, CAST(N'2220-12-25T23:31:33.8085839+07:00' AS DateTimeOffset), 1, 0, N'298f4eca-2a0e-446c-8a7e-780bba8a3ddf', N'Guest', NULL, 1, CAST(N'2020-12-25T23:04:55.8454376+07:00' AS DateTimeOffset), CAST(N'2020-12-25T23:04:55.8454548+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'en', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (553, N'test@gmail.com', N'TEST@GMAIL.COM', N'test@gmail.com', N'TEST@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEHaw2/40YwFFFvuNljG4XCuS2k0PDzXxuZZGAqpoP4jRZHdjfUapjFr4c41GsmazXA==', N'THTUTSNBMRX6OPNR5Q3FZQ5KIWOBIGXD', N'014855b5-2553-4fd6-aa62-e9dde63ab8fc', NULL, 0, 0, NULL, 1, 0, N'00000000-0000-0000-0000-000000000000', N'test', NULL, 0, CAST(N'2021-01-05T21:28:08.5183993+07:00' AS DateTimeOffset), CAST(N'2021-01-05T21:28:08.5184020+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'vi', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (554, N'test1@gmail.com', N'TEST1@GMAIL.COM', N'test1@gmail.com', N'TEST1@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAELPfYtqzSipzz+iFtrFCGqXZ2bXru9Tgl0zOh3FP26HKjzoNlh0Bi+YfUPBA3yfezg==', N'2R6O67NIUOV25NNACCRVWLVYXZHKWL5S', N'1a632d8d-799e-4220-b8fc-5c40e3772992', NULL, 0, 0, NULL, 1, 0, N'00000000-0000-0000-0000-000000000000', N'Tien Quoc', NULL, 0, CAST(N'2021-01-15T10:28:13.0248183+07:00' AS DateTimeOffset), CAST(N'2021-01-15T10:28:13.0248235+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'vi', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (555, N'quoctienzero4@gmail.com', N'QUOCTIENZERO4@GMAIL.COM', N'quoctienzero4@gmail.com', N'QUOCTIENZERO4@GMAIL.COM', 0, NULL, N'HXTEJPF3AP3IKGWI3JKG6G3CQM722YAU', N'1127fa47-bef1-4d7a-8e9b-267dedb3f855', NULL, 0, 0, NULL, 1, 0, N'00000000-0000-0000-0000-000000000000', N'Tiến Quốc', NULL, 0, CAST(N'2021-01-28T14:10:39.7090989+07:00' AS DateTimeOffset), CAST(N'2021-01-28T14:10:39.7091086+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'vi', NULL)
INSERT [dbo].[Core_User] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [UserGuid], [FullName], [VendorId], [IsDeleted], [CreatedOn], [LatestUpdatedOn], [DefaultShippingAddressId], [DefaultBillingAddressId], [RefreshTokenHash], [Culture], [ExtensionData]) VALUES (556, N'peheovip20@gmail.com', N'PEHEOVIP20@GMAIL.COM', N'peheovip20@gmail.com', N'PEHEOVIP20@GMAIL.COM', 0, NULL, N'7EHFLY3J36IIFT4R4WZIVXF6GXMQF3N7', N'1c610a98-aaee-4bed-826d-23f029098620', NULL, 0, 0, NULL, 1, 0, N'00000000-0000-0000-0000-000000000000', N'Nghĩa Bùi', NULL, 0, CAST(N'2021-01-28T14:37:33.6472255+07:00' AS DateTimeOffset), CAST(N'2021-01-28T14:37:33.6472278+07:00' AS DateTimeOffset), NULL, NULL, NULL, N'vi', NULL)
SET IDENTITY_INSERT [dbo].[Core_User] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_UserAddress] ON 

INSERT [dbo].[Core_UserAddress] ([Id], [UserId], [AddressId], [AddressType], [LastUsedOn]) VALUES (1, 10, 2, 0, NULL)
INSERT [dbo].[Core_UserAddress] ([Id], [UserId], [AddressId], [AddressType], [LastUsedOn]) VALUES (2, 554, 3, 0, NULL)
INSERT [dbo].[Core_UserAddress] ([Id], [UserId], [AddressId], [AddressType], [LastUsedOn]) VALUES (3, 556, 4, 0, NULL)
SET IDENTITY_INSERT [dbo].[Core_UserAddress] OFF
GO
INSERT [dbo].[Core_UserLogin] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Google', N'107331427735818199322', N'Google', 555)
INSERT [dbo].[Core_UserLogin] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Google', N'117272309053492275328', N'Google', 556)
GO
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (10, 1)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (556, 1)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (546, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (547, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (548, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (549, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (550, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (551, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (552, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (553, 3)
INSERT [dbo].[Core_UserRole] ([UserId], [RoleId]) VALUES (554, 3)
GO
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'CarouselWidget', N'
Tiện ích băng chuyền', N'CarouselWidget', N'widget-carousel-create', N'widget-carousel-edit', CAST(N'2018-05-29T04:33:39.1640000+07:00' AS DateTimeOffset), 0)
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'CategoryWidget', N'
Tiện ích danh mục', N'CategoryWidget', N'widget-category-create', N'widget-category-edit', CAST(N'2018-05-29T04:33:39.1600000+07:00' AS DateTimeOffset), 0)
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'HtmlWidget', N'
Tiện ích Html', N'HtmlWidget', N'widget-html-create', N'widget-html-edit', CAST(N'2018-05-29T04:33:39.1640000+07:00' AS DateTimeOffset), 0)
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'ProductWidget', N'
Tiện ích sản phẩm', N'ProductWidget', N'widget-product-create', N'widget-product-edit', CAST(N'2018-05-29T04:33:39.1630000+07:00' AS DateTimeOffset), 0)
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'RecentlyViewedWidget', N'
Tiện ích đã xem gần đây', N'RecentlyViewedWidget', N'widget-recently-viewed-create', N'widget-recently-viewed-edit', CAST(N'2018-05-29T04:33:39.1640000+07:00' AS DateTimeOffset), 0)
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'SimpleProductWidget', N'
Tiện ích sản phẩm đơn giản', N'SimpleProductWidget', N'widget-simple-product-create', N'widget-simple-product-edit', CAST(N'2018-05-29T04:33:39.1630000+07:00' AS DateTimeOffset), 0)
INSERT [dbo].[Core_Widget] ([Id], [Name], [ViewComponentName], [CreateUrl], [EditUrl], [CreatedOn], [IsPublished]) VALUES (N'SpaceBarWidget', N'
Tiện ích SpaceBar', N'SpaceBarWidget', N'widget-spacebar-create', N'widget-spacebar-edit', CAST(N'2018-05-29T04:33:39.1640000+07:00' AS DateTimeOffset), 0)
GO
SET IDENTITY_INSERT [dbo].[Core_WidgetInstance] ON 

INSERT [dbo].[Core_WidgetInstance] ([Id], [Name], [CreatedOn], [LatestUpdatedOn], [PublishStart], [PublishEnd], [WidgetId], [WidgetZoneId], [DisplayOrder], [Data], [HtmlData]) VALUES (1, N'Trang chủ', CAST(N'2016-07-11T05:29:31.1868415+00:00' AS DateTimeOffset), CAST(N'2016-07-11T05:29:31.1868415+00:00' AS DateTimeOffset), CAST(N'2016-07-11T05:42:44.7520000+00:00' AS DateTimeOffset), NULL, N'CarouselWidget', 1, 0, N'[{"Image":"f7919fe7-320e-4641-b46f-e6e59497ba68.jpg","ImageUrl":"/user-content/8ded48da-6ae4-4080-b30d-19235b58a642.jpg","Caption":null,"SubCaption":null,"LinkText":null,"TargetUrl":"#"},{"Image":"77ed9f85-de7d-4d2f-838c-26dbf064ad4b.jpg","ImageUrl":"/user-content/1b4b978d-04eb-49f9-9a86-126354967ec1.jpg","Caption":null,"SubCaption":null,"LinkText":null,"TargetUrl":"#"},{"Image":"5bcda17d-3b2d-4749-8511-8a156b9c1bd5.jpg","ImageUrl":"/user-content/9f635a3c-6e53-4b45-bb1e-64fbf4e28602.jpg","Caption":null,"SubCaption":null,"LinkText":null,"TargetUrl":"#"}]', NULL)
INSERT [dbo].[Core_WidgetInstance] ([Id], [Name], [CreatedOn], [LatestUpdatedOn], [PublishStart], [PublishEnd], [WidgetId], [WidgetZoneId], [DisplayOrder], [Data], [HtmlData]) VALUES (2, N'Sản phẩm', CAST(N'2016-07-11T05:30:49.3473494+00:00' AS DateTimeOffset), CAST(N'2016-07-11T05:30:49.3473494+00:00' AS DateTimeOffset), CAST(N'2016-07-11T05:42:44.7523284+00:00' AS DateTimeOffset), NULL, N'ProductWidget', 2, 0, N'{"NumberOfProducts":4,"CategoryIds":null,"OrderBy":0,"FeaturedOnly":false}', NULL)
INSERT [dbo].[Core_WidgetInstance] ([Id], [Name], [CreatedOn], [LatestUpdatedOn], [PublishStart], [PublishEnd], [WidgetId], [WidgetZoneId], [DisplayOrder], [Data], [HtmlData]) VALUES (3, N'Nội dung quảng cáo', CAST(N'2018-01-03T15:20:53.1550824+07:00' AS DateTimeOffset), CAST(N'2018-01-03T15:20:53.1552733+07:00' AS DateTimeOffset), CAST(N'2018-01-03T08:16:47.1160000+00:00' AS DateTimeOffset), NULL, N'SpaceBarWidget', 3, 0, N'[{"IconHtml":"fa fa-truck","Title":"Miễn phí giao hàng","Description":"Đối với tất cả đơn đặt hàng","Image":"","ImageUrl":null},{"IconHtml":"fa fa-money","Title":"Hoàn tiền","Description":"30 ngày trả lại tiền","Image":"","ImageUrl":null},{"IconHtml":"fa fa-credit-card-alt","Title":"Thanh toán an toàn","Description":"Thanh toán trực tuyến được bảo vệ","Image":"","ImageUrl":null}]', NULL)
INSERT [dbo].[Core_WidgetInstance] ([Id], [Name], [CreatedOn], [LatestUpdatedOn], [PublishStart], [PublishEnd], [WidgetId], [WidgetZoneId], [DisplayOrder], [Data], [HtmlData]) VALUES (4, N'Quản trị', CAST(N'2016-07-11T05:42:44.7523284+00:00' AS DateTimeOffset), CAST(N'2016-07-11T05:42:44.7523284+00:00' AS DateTimeOffset), NULL, NULL, N'HtmlWidget', 3, 0, NULL, N'<div>    <h2 class="page-header">Administrator</h2>    <p>Manage your store. Admin email: admin@SimplCommerce.com. Admin password: 1qazZAQ!</p>    <p>        <a class="btn btn-primary" href="Admin" role="button">Go to Dashboard</a>    </p></div>')
SET IDENTITY_INSERT [dbo].[Core_WidgetInstance] OFF
GO
SET IDENTITY_INSERT [dbo].[Core_WidgetZone] ON 

INSERT [dbo].[Core_WidgetZone] ([Id], [Name], [Description]) VALUES (1, N'
Trang chủ nổi bật', NULL)
INSERT [dbo].[Core_WidgetZone] ([Id], [Name], [Description]) VALUES (2, N'Nội dung chính', NULL)
INSERT [dbo].[Core_WidgetZone] ([Id], [Name], [Description]) VALUES (3, N'Trang chủ nội dung chính', NULL)
SET IDENTITY_INSERT [dbo].[Core_WidgetZone] OFF
GO
SET IDENTITY_INSERT [dbo].[Inventory_Stock] ON 

INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (49, 17, 1, 100, 0)
INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (50, 16, 1, 100, 0)
INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (51, 15, 1, 100, 0)
INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (52, 18, 1, 200, 0)
INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (56, 19, 1, 100, 0)
INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (57, 21, 1, 100, 0)
INSERT [dbo].[Inventory_Stock] ([Id], [ProductId], [WarehouseId], [Quantity], [ReservedQuantity]) VALUES (58, 20, 1, 0, 0)
SET IDENTITY_INSERT [dbo].[Inventory_Stock] OFF
GO
SET IDENTITY_INSERT [dbo].[Inventory_StockHistory] ON 

INSERT [dbo].[Inventory_StockHistory] ([Id], [ProductId], [WarehouseId], [CreatedOn], [CreatedById], [AdjustedQuantity], [Note]) VALUES (23, 15, 1, CAST(N'2021-01-05T11:42:04.0787969+07:00' AS DateTimeOffset), 10, 100, NULL)
INSERT [dbo].[Inventory_StockHistory] ([Id], [ProductId], [WarehouseId], [CreatedOn], [CreatedById], [AdjustedQuantity], [Note]) VALUES (24, 16, 1, CAST(N'2021-01-05T11:42:04.3237908+07:00' AS DateTimeOffset), 10, 100, NULL)
INSERT [dbo].[Inventory_StockHistory] ([Id], [ProductId], [WarehouseId], [CreatedOn], [CreatedById], [AdjustedQuantity], [Note]) VALUES (25, 17, 1, CAST(N'2021-01-05T11:42:04.3605140+07:00' AS DateTimeOffset), 10, 100, NULL)
INSERT [dbo].[Inventory_StockHistory] ([Id], [ProductId], [WarehouseId], [CreatedOn], [CreatedById], [AdjustedQuantity], [Note]) VALUES (26, 19, 1, CAST(N'2021-01-05T21:37:21.6797753+07:00' AS DateTimeOffset), 10, 100, NULL)
INSERT [dbo].[Inventory_StockHistory] ([Id], [ProductId], [WarehouseId], [CreatedOn], [CreatedById], [AdjustedQuantity], [Note]) VALUES (27, 21, 1, CAST(N'2021-01-27T22:09:36.0817665+07:00' AS DateTimeOffset), 10, 100, NULL)
INSERT [dbo].[Inventory_StockHistory] ([Id], [ProductId], [WarehouseId], [CreatedOn], [CreatedById], [AdjustedQuantity], [Note]) VALUES (28, 18, 1, CAST(N'2021-01-27T22:12:05.4528256+07:00' AS DateTimeOffset), 10, 100, NULL)
SET IDENTITY_INSERT [dbo].[Inventory_StockHistory] OFF
GO
SET IDENTITY_INSERT [dbo].[Inventory_Warehouse] ON 

INSERT [dbo].[Inventory_Warehouse] ([Id], [Name], [AddressId], [VendorId]) VALUES (1, N'Kho mặc định', 1, NULL)
SET IDENTITY_INSERT [dbo].[Inventory_Warehouse] OFF
GO
INSERT [dbo].[Localization_Culture] ([Id], [Name]) VALUES (N'en-US', N'English')
INSERT [dbo].[Localization_Culture] ([Id], [Name]) VALUES (N'vi-VN', N'Tiếng Việt')
GO
SET IDENTITY_INSERT [dbo].[Localization_Resource] ON 

INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (1, N'Register', N'Đăng ký', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (2, N'Hello {0}!', N'Chào {0}!', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (3, N'Log in', N'Đăng nhập', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (4, N'Log off', N'Đăng xuất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (5, N'The Email field is required.', N'Địa chỉ email cần phải có ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (6, N'Email', N'Địa chỉ email', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (7, N'User List', N'Danh sách người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (8, N'Remember me?', N'Ghi nhớ?', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (9, N'Password', N'Mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (10, N'Use a local account to log in.', N'Sử dụng tài khoản của bạn để đăng nhập', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (11, N'Register as a new user?', N'Đăng ký người dùng mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (12, N'Forgot your password?', N'Quên mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (13, N'Use another service to log in.', N'Đăng nhập bằng các tài khoản khác', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (14, N'Full name', N'Họ và tên', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (15, N'Confirm password', N'Xác nhận mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (16, N'Create a new account.', N'Tạo tài khoản mới.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (17, N'All', N'Tất cả', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (18, N'Home', N'Trang chủ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (19, N'Add to cart', N'Thêm vào giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (20, N'Product detail', N'Mô tả sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (21, N'Product specification', N'Thông số kỹ thuật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (22, N'Rate this product', N'Đánh giá sản phẩm này', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (23, N'Review comment', N'Mô tả đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (24, N'Review title', N'Tiêu đề đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (25, N'Posted by', N'Đánh giá bởi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (26, N'Submit review', N'Gửi đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (27, N'You have', N'Bạn có', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (28, N'products in your cart', N'sản phẩm trong giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (29, N'Continue shopping', N'Tiếp tục mua sắm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (30, N'View cart', N'Xem giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (31, N'The product has been added to your cart', N'Sản phẩm đã được thêm vào giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (32, N'Cart subtotal', N'Thành tiền', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (33, N'Shopping Cart', N'Giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (34, N'Product', N'Sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (35, N'Price', N'Giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (36, N'Quantity', N'Số lượng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (37, N'There are no items in this cart.', N'Chưa có sản phẩm nào trong giỏ hàng của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (38, N'Go to shopping', N'Đi mua sắm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (39, N'Order summary', N'Tóm lược đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (40, N'Subtotal', N'Thành tiền', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (41, N'Process to Checkout', N'Tiến hành thanh toán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (42, N'Shipping address', N'Địa chỉ giao hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (43, N'Add another address', N'Thêm địa chỉ mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (44, N'Contact name', N'Tên người liên hệ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (45, N'Address', N'Địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (46, N'State or Province', N'Thành phố / Tỉnh', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (47, N'District', N'Quận / Huyện', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (48, N'Phone', N'Số điện thoại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (49, N'Order', N'Đặt hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (50, N'products', N'sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (51, N'reviews', N'nhận xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (52, N'Add Review', N'Viết nhận xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (53, N'Customer reviews', N'Nhận xét của khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (54, N'Your review will be shown within the next 24h.', N'Nhận xét của bạn sẽ được hiển thị trong vòng 24 tiếng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (55, N'Thank you {0} for your review', N'Cảm ơn {0} đã gửi nhận xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (56, N'Rating average', N'Đánh giá trung bình', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (57, N'stars', N'sao', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (58, N'Filter by', N'Tìm theo', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (59, N'Category', N'Danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (60, N'Brand', N'Nhãn hiệu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (61, N'Sort by:', N'Sắp xếp theo:', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (62, N'results', N'kết quả', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (63, N'View options', N'Xem các tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (64, N'Associate your {0} account.', N'Liên kết với tài khoản {0} của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (65, N'Users', N'Người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (66, N'Vendors', N'Người bán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (67, N'Reviews', N'Đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (68, N'Products', N'Sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (69, N'Categories', N'Danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (70, N'Brands', N'Nhãn hiệu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (71, N'Product Options', N'Tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (72, N'Product Attribute', N'Thuộc tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (73, N'Product Attribute Groups', N'Nhóm thuộc tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (74, N'Product Templates', N'Mẫu sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (75, N'Sales', N'Bán hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (76, N'Orders', N'Đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (77, N'Content Management', N'Quán lý nội dung', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (78, N'Pages', N'Trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (79, N'Widgets', N'Widgets', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (80, N'System', N'Hệ thống', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (81, N'Configuration', N'Cấu hình', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (82, N'Translations', N'Translations', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (83, N'Dashboard', N'Dashboard', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (84, N'Incomplete orders', N'Đơn hàng chưa xong', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (85, N'Pending reviews', N'Đánh giá đang chờ duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (86, N'Most searched keywords', N'Từ khóa được tìm kiếm nhiều nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (87, N'Most viewed products', N'Sản phẩm được xem nhiều nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (88, N'OrderId', N'Số đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (89, N'Order Status', N'Trạng thái đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (90, N'Customer', N'Khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (91, N'Created On', N'Ngày tạo', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (92, N'SubTotal', N'Tổng tiền', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (93, N'Actions', N'Hành động', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (94, N'Site', N'Site', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (95, N'Catalog', N'Quản lý sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (96, N'Title', N'Tiêu đề', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (97, N'Comment', N'Bình luận', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (98, N'Status', N'Trạng thái', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (99, N'Rating', N'Xếp hạng', N'vi-VN')
GO
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (100, N'Keyword', N'Từ khóa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (101, N'Count', N'Số lần', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (102, N'Create User', N'Tạo người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (103, N'FullName', N'Họ và tên', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (104, N'Roles', N'Roles', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (105, N'Edit User', N'Chỉnh sửa user', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (106, N'Manager of Vendor', N'Manager of Vendor', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (107, N'Save', N'Lưu lại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (108, N'Cancel', N'Hủy', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (109, N'Phone Number', N'Số điện thoại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (110, N'Create Vendor', N'Tạo người bán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (111, N'Is Active', N'Đang hoạt động', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (112, N'Edit Vendor', N'Chỉnh sửa người bán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (113, N'Managers', N'Quản lý', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (114, N'Description', N'Mô tả', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (115, N'Pending', N'Đang chờ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (116, N'Approved', N'Đã duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (117, N'Not Approved', N'Không duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (118, N'Approve', N'Duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (119, N'Create Product', N'Tạo sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (120, N'Has Options', N'Tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (121, N'Is Visible Individually', N'Hiển thị riêng lẻ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (122, N'Is Featured', N'Nổi bậc', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (123, N'Is Allowed To Order', N'Cho phép đặt hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (124, N'Is Called For Pricing', N'Gọi để biết giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (125, N'Stock Quantity', N'Số lượng trong kho', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (126, N'Is Published', N'Công bố', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (127, N'Yes', N'Có', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (128, N'No', N'Không', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (129, N'Edit Product', N'Chỉnh sửa sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (130, N'Product Name', N'Tên sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (131, N'Short Description', N'Mô tả ngắn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (132, N'Specification', N'Thông số kỹ thuật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (133, N'Old Price', N'Giá cũ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (134, N'Special Price', N'Giá đặc biệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (135, N'Special Price Start', N'Bắt đầu giá đặc biệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (136, N'Special Price End', N'Kết thúc giá đặc biệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (137, N'Thumbnail', N'Hình đại diện', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (138, N'Product Images', N'Hình sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (139, N'Product Documents', N'Tài liệu kỹ thuật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (140, N'Out Of Stock', N'Hết hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (141, N'Available Options', N'Tùy chọn khả dụng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (142, N'Add Option', N'Thêm tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (143, N'Option Values', N'Giá trị của tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (144, N'Delete Option', N'Xóa tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (145, N'Generate Combinations', N'Tự động tạo tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (146, N'Product Variations', N'Những tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (147, N'Option Combinations', N'Sự kết hợp của những tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (148, N'Images', N'Hình ảnh', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (149, N'Apply', N'Áp dụng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (150, N'Available Attributes', N'Đặt tính khả dụng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (151, N'Add Attribute', N'Thêm đặc tính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (152, N'Product Attributes', N'Đặc tính của sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (153, N'Attribute Name', N'Tên đặc tính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (154, N'Value', N'Giá trị', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (155, N'General Information', N'Thông tin chung', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (156, N'Category Mapping', N'Danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (157, N'Related Products', N'Sản phẩm tương tự', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (158, N'Manage Related Products', N'Quản lý sản phẩm tương tự', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (159, N'Create Category', N'Tạo danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (160, N'Edit Category', N'Chỉnh sửa danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (161, N'Create Brand', N'Tạo nhãn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (162, N'Edit Brand', N'Chỉnh sửa nhãn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (163, N'Name', N'Tên', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (164, N'Parent Category', N'Danh mục cha', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (165, N'Group', N'Nhóm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (166, N'Create Product Attribute', N'Tạo đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (167, N'Edit Product Attribute', N'Chỉnh sửa đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (168, N'Create Product Attribute Group', N'Tạo nhóm đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (169, N'Edit Product Attribute Group', N'Chỉnh sửa nhóm đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (170, N'Create Product Option', N'Tạo tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (171, N'Edit Product Option', N'Chỉnh sửa tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (172, N'Create Product Display Widget', N'Create Product Display Widget', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (173, N'Edit Product Display Widget', N'Edit Product Display Widget', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (174, N'Widget Name', N'Widget Name', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (175, N'Widget Zone', N'Widget Zone', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (176, N'Publish Start', N'Thời gian bắt đầu công bố', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (177, N'Publish End', N'Thời gian kết thúc', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (178, N'Number of Products', N'Số lượng sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (179, N'Order By', N'Sắp xếp theo', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (180, N'Create Product Template', N'Tạo mẫu sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (181, N'Edit Product Template', N'Chỉnh sửa mẫu sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (182, N'Added Attributes', N'Những đặt tính đã thêm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (183, N'Back', N'Quay về', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (184, N'Order Detail', N'Chi tiết đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (185, N'Order Information', N'Thông tin đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (186, N'Change', N'Thay đổi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (187, N'Product Information', N'Thông tin sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (188, N'Shipping Information', N'Thông tin giao hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (189, N'Application Settings', N'Application Settings', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (190, N'Create Page', N'Tạo trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (191, N'Edit Page', N'Chỉnh sửa trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (192, N'Body', N'Nội dung chính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (193, N'Account Dashboard', N'Quản lý tài khoản', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (194, N'Account Information', N'Thông tin tài khoản', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (195, N'Edit', N'Chỉnh sửa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (196, N'Security', N'Bảo mật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (197, N'Create', N'Tạo mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (198, N'External Logins', N'Đăng nhập từ mạng xã hội', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (199, N'Manage', N'Quản lý', N'vi-VN')
GO
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (200, N'Default shipping address', N'Địa chỉ nhận hàng mặc định', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (201, N'Manage address', N'Quản lý địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (202, N'You don''t have any default address', N'Bạn chưa có địa chỉ mặc định nào', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (203, N'Order History', N'Lịch sử mua hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (204, N'Address Book', N'Sổ địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (205, N'Add Address', N'Thêm địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (206, N'Delete', N'Xóa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (207, N'Set as default shipping address', N'Chọn làm địa chỉ mặc định khi nhận hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (208, N'Edit Address', N'Chỉnh sửa địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (209, N'Create Address', N'Tạo địa chỉ mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (210, N'Your account', N'Tài khoản của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (211, N'Date', N'Ngày', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (212, N'Customer Groups', N'Nhóm khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (213, N'Add to Compare', N'Thêm vào để so sánh', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (214, N'Not available', N'Không có sẵn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (215, N'You save', N'Bạn tiết kiệm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (216, N'Submit', N'Gửi đi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (217, N'All Categories', N'Tất cả danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (218, N'Search here...', N'Tìm kiếm ở đây...', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (219, N'The {0} field is required.', N'Trường {0} là bắt buộc.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (220, N'The value {0} is not valid for {1}.', N'Giá trị {0} thì không hợp lệ cho {1}.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (221, N'The value {0} is invalid.', N'Giá trị {0} thì không hợp lệ.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (222, N'The supplied value is invalid for {0}.', N'Giá trị đã cung cấp không hợp lệ cho {0}.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (223, N'Null value is invalid.', N'Giá trị trống thì không hợp lệ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (224, N'A value is required.', N'Một giá trị là bắt buộc', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (225, N'A value for the {0} property was not provided.', N'Giá trị cho {0} đã không được cung cấp.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (226, N'Register', N'Đăng ký', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (227, N'Hello {0}!', N'Chào {0}!', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (228, N'Log in', N'Đăng nhập', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (229, N'Log off', N'Đăng xuất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (230, N'The Email field is required.', N'Địa chỉ email cần phải có ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (231, N'Email', N'Địa chỉ email', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (232, N'User List', N'Danh sách người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (233, N'Remember me?', N'Ghi nhớ?', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (234, N'Password', N'Mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (235, N'Use a local account to log in.', N'Sử dụng tài khoản của bạn để đăng nhập', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (236, N'Register as a new user?', N'Đăng ký người dùng mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (237, N'Forgot your password?', N'Quên mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (238, N'Use another service to log in.', N'Đăng nhập bằng các tài khoản khác', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (239, N'Full name', N'Họ và tên', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (240, N'Confirm password', N'Xác nhận mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (241, N'Create a new account.', N'Tạo tài khoản mới.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (242, N'All', N'Tất cả', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (243, N'Home', N'Trang chủ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (244, N'Add to cart', N'Thêm vào giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (245, N'Product detail', N'Mô tả sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (246, N'Product specification', N'Thông số kỹ thuật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (247, N'Rate this product', N'Đánh giá sản phẩm này', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (248, N'Review comment', N'Mô tả đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (249, N'Review title', N'Tiêu đề đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (250, N'Posted by', N'Đánh giá bởi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (251, N'Submit review', N'Gửi đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (252, N'You have', N'Bạn có', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (253, N'products in your cart', N'sản phẩm trong giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (254, N'Continue shopping', N'Tiếp tục mua sắm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (255, N'View cart', N'Xem giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (256, N'The product has been added to your cart', N'Sản phẩm đã được thêm vào giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (257, N'Cart subtotal', N'Thành tiền', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (258, N'Shopping Cart', N'Giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (259, N'Product', N'Sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (260, N'Price', N'Giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (261, N'Quantity', N'Số lượng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (262, N'There are no items in this cart.', N'Chưa có sản phẩm nào trong giỏ hàng của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (263, N'Go to shopping', N'Đi mua sắm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (264, N'Order summary', N'Tóm lược đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (265, N'Subtotal', N'Thành tiền', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (266, N'Process to Checkout', N'Tiến hành thanh toán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (267, N'Shipping address', N'Địa chỉ giao hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (268, N'Add another address', N'Thêm địa chỉ mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (269, N'Contact name', N'Tên người liên hệ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (270, N'Address', N'Địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (271, N'State or Province', N'Thành phố / Tỉnh', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (272, N'District', N'Quận / Huyện', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (273, N'Phone', N'Số điện thoại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (274, N'Order', N'Đặt hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (275, N'products', N'sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (276, N'reviews', N'nhận xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (277, N'Add Review', N'Viết nhận xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (278, N'Customer reviews', N'Nhận xét của khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (279, N'Your review will be shown within the next 24h.', N'Nhận xét của bạn sẽ được hiển thị trong vòng 24 tiếng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (280, N'Thank you {0} for your review', N'Cảm ơn {0} đã gửi nhận xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (281, N'Rating average', N'Đánh giá trung bình', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (282, N'stars', N'sao', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (283, N'Filter by', N'Tìm theo', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (284, N'Category', N'Danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (285, N'Brand', N'Nhãn hiệu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (286, N'Sort by:', N'Sắp xếp theo:', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (287, N'results', N'kết quả', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (288, N'View options', N'Xem các tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (289, N'Associate your {0} account.', N'Liên kết với tài khoản {0} của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (290, N'Users', N'Người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (291, N'Vendors', N'Người bán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (292, N'Reviews', N'Đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (293, N'Products', N'Sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (294, N'Categories', N'Danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (295, N'Brands', N'Nhãn hiệu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (296, N'Product Options', N'Tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (297, N'Product Attribute', N'Thuộc tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (298, N'Product Attribute Groups', N'Nhóm thuộc tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (299, N'Product Templates', N'Mẫu sản phẩm', N'vi-VN')
GO
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (300, N'Sales', N'Bán hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (301, N'Orders', N'Đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (302, N'Content Management', N'Quán lý nội dung', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (303, N'Pages', N'Trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (304, N'Widgets', N'Widgets', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (305, N'System', N'Hệ thống', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (306, N'Configuration', N'Cấu hình', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (307, N'Translations', N'Translations', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (308, N'Dashboard', N'Dashboard', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (309, N'Incomplete orders', N'Đơn hàng chưa xong', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (310, N'Pending reviews', N'Đánh giá đang chờ duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (311, N'Most searched keywords', N'Từ khóa được tìm kiếm nhiều nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (312, N'Most viewed products', N'Sản phẩm được xem nhiều nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (313, N'OrderId', N'Số đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (314, N'Order Status', N'Trạng thái đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (315, N'Customer', N'Khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (316, N'Created On', N'Ngày tạo', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (317, N'SubTotal', N'Tổng tiền', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (318, N'Actions', N'Hành động', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (319, N'Site', N'Site', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (320, N'Catalog', N'Quản lý sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (321, N'Title', N'Tiêu đề', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (322, N'Comment', N'Bình luận', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (323, N'Status', N'Trạng thái', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (324, N'Rating', N'Xếp hạng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (325, N'Keyword', N'Từ khóa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (326, N'Count', N'Số lần', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (327, N'Create User', N'Tạo người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (328, N'FullName', N'Họ và tên', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (329, N'Roles', N'Roles', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (330, N'Edit User', N'Chỉnh sửa user', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (331, N'Manager of Vendor', N'Manager of Vendor', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (332, N'Save', N'Lưu lại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (333, N'Cancel', N'Hủy', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (334, N'Phone Number', N'Số điện thoại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (335, N'Create Vendor', N'Tạo người bán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (336, N'Is Active', N'Đang hoạt động', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (337, N'Edit Vendor', N'Chỉnh sửa người bán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (338, N'Managers', N'Quản lý', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (339, N'Description', N'Mô tả', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (340, N'Pending', N'Đang chờ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (341, N'Approved', N'Đã duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (342, N'Not Approved', N'Không duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (343, N'Approve', N'Duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (344, N'Create Product', N'Tạo sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (345, N'Has Options', N'Tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (346, N'Is Visible Individually', N'Hiển thị riêng lẻ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (347, N'Is Featured', N'Nổi bậc', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (348, N'Is Allowed To Order', N'Cho phép đặt hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (349, N'Is Called For Pricing', N'Gọi để biết giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (350, N'Stock Quantity', N'Số lượng trong kho', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (351, N'Is Published', N'Công bố', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (352, N'Yes', N'Có', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (353, N'No', N'Không', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (354, N'Edit Product', N'Chỉnh sửa sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (355, N'Product Name', N'Tên sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (356, N'Short Description', N'Mô tả ngắn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (357, N'Specification', N'Thông số kỹ thuật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (358, N'Old Price', N'Giá cũ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (359, N'Special Price', N'Giá đặc biệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (360, N'Special Price Start', N'Bắt đầu giá đặc biệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (361, N'Special Price End', N'Kết thúc giá đặc biệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (362, N'Thumbnail', N'Hình đại diện', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (363, N'Product Images', N'Hình sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (364, N'Product Documents', N'Tài liệu kỹ thuật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (365, N'Out Of Stock', N'Hết hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (366, N'Available Options', N'Tùy chọn khả dụng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (367, N'Add Option', N'Thêm tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (368, N'Option Values', N'Giá trị của tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (369, N'Delete Option', N'Xóa tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (370, N'Generate Combinations', N'Tự động tạo tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (371, N'Product Variations', N'Những tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (372, N'Option Combinations', N'Sự kết hợp của những tùy chọn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (373, N'Images', N'Hình ảnh', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (374, N'Apply', N'Áp dụng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (375, N'Available Attributes', N'Đặt tính khả dụng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (376, N'Add Attribute', N'Thêm đặc tính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (377, N'Product Attributes', N'Đặc tính của sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (378, N'Attribute Name', N'Tên đặc tính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (379, N'Value', N'Giá trị', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (380, N'General Information', N'Thông tin chung', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (381, N'Category Mapping', N'Danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (382, N'Related Products', N'Sản phẩm tương tự', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (383, N'Manage Related Products', N'Quản lý sản phẩm tương tự', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (384, N'Create Category', N'Tạo danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (385, N'Edit Category', N'Chỉnh sửa danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (386, N'Create Brand', N'Tạo nhãn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (387, N'Edit Brand', N'Chỉnh sửa nhãn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (388, N'Name', N'Tên', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (389, N'Parent Category', N'Danh mục cha', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (390, N'Group', N'Nhóm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (391, N'Create Product Attribute', N'Tạo đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (392, N'Edit Product Attribute', N'Chỉnh sửa đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (393, N'Create Product Attribute Group', N'Tạo nhóm đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (394, N'Edit Product Attribute Group', N'Chỉnh sửa nhóm đặt tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (395, N'Create Product Option', N'Tạo tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (396, N'Edit Product Option', N'Chỉnh sửa tùy chọn sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (397, N'Create Product Display Widget', N'Create Product Display Widget', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (398, N'Edit Product Display Widget', N'Edit Product Display Widget', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (399, N'Widget Name', N'Widget Name', N'vi-VN')
GO
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (400, N'Widget Zone', N'Widget Zone', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (401, N'Publish Start', N'Thời gian bắt đầu công bố', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (402, N'Publish End', N'Thời gian kết thúc', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (403, N'Number of Products', N'Số lượng sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (404, N'Order By', N'Sắp xếp theo', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (405, N'Create Product Template', N'Tạo mẫu sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (406, N'Edit Product Template', N'Chỉnh sửa mẫu sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (407, N'Added Attributes', N'Những đặt tính đã thêm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (408, N'Back', N'Quay về', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (409, N'Order Detail', N'Chi tiết đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (410, N'Order Information', N'Thông tin đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (411, N'Change', N'Thay đổi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (412, N'Product Information', N'Thông tin sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (413, N'Shipping Information', N'Thông tin giao hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (414, N'Application Settings', N'Application Settings', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (415, N'Create Page', N'Tạo trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (416, N'Edit Page', N'Chỉnh sửa trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (417, N'Body', N'Nội dung chính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (418, N'Account Dashboard', N'Quản lý tài khoản', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (419, N'Account Information', N'Thông tin tài khoản', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (420, N'Edit', N'Chỉnh sửa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (421, N'Security', N'Bảo mật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (422, N'Create', N'Tạo mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (423, N'External Logins', N'Đăng nhập từ mạng xã hội', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (424, N'Manage', N'Quản lý', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (425, N'Default shipping address', N'Địa chỉ nhận hàng mặc định', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (426, N'Manage address', N'Quản lý địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (427, N'You don''t have any default address', N'Bạn chưa có địa chỉ mặc định nào', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (428, N'Order History', N'Lịch sử mua hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (429, N'Address Book', N'Sổ địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (430, N'Add Address', N'Thêm địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (431, N'Delete', N'Xóa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (432, N'Set as default shipping address', N'Chọn làm địa chỉ mặc định khi nhận hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (433, N'Edit Address', N'Chỉnh sửa địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (434, N'Create Address', N'Tạo địa chỉ mới', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (435, N'Your account', N'Tài khoản của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (436, N'Date', N'Ngày', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (437, N'Customer Groups', N'Nhóm khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (438, N'Add to Compare', N'Thêm vào để so sánh', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (439, N'Not available', N'Không có sẵn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (440, N'You save', N'Bạn tiết kiệm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (441, N'Submit', N'Gửi đi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (442, N'All Categories', N'Tất cả danh mục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (443, N'Search here...', N'Tìm kiếm ở đây...', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (444, N'The {0} field is required.', N'Trường {0} là bắt buộc.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (445, N'The value {0} is not valid for {1}.', N'Giá trị {0} thì không hợp lệ cho {1}.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (446, N'The value {0} is invalid.', N'Giá trị {0} thì không hợp lệ.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (447, N'The supplied value is invalid for {0}.', N'Giá trị đã cung cấp không hợp lệ cho {0}.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (448, N'Null value is invalid.', N'Giá trị trống thì không hợp lệ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (449, N'A value is required.', N'Một giá trị là bắt buộc', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (450, N'A value for the {0} property was not provided.', N'Giá trị cho {0} đã không được cung cấp.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (451, N'Manage your account', N'Manage your account', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (452, N'We are sorry!', N'We are sorry!', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (453, N'The page you have requested cannot be found', N'The page you have requested cannot be found', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (454, N'Maybe the page was moved or deleted, or perhaps you just mistyped the address.', N'Maybe the page was moved or deleted, or perhaps you just mistyped the address.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (455, N'Why not try and find your way using the navigation bar above or click on the logo to return our home page.', N'Why not try and find your way using the navigation bar above or click on the logo to return our home page.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (456, N'Free Shipping', N'Free Shipping', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (457, N'For orders over $99', N'For orders over $99', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (458, N'Money Return', N'Money Return', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (459, N'30 days money return', N'30 days money return', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (460, N'Safe Payment', N'Safe Payment', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (461, N'Protected online payment', N'Protected online payment', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (462, N'Chúng tôi xin lỗi!', N'Chúng tôi xin lỗi!', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (463, N'Không thể tìm thấy trang bạn đã yêu cầu', N'Không thể tìm thấy trang bạn đã yêu cầu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (464, N'Có thể trang đã bị di chuyển hoặc bị xóa, hoặc có thể bạn vừa nhập sai địa chỉ.', N'Có thể trang đã bị di chuyển hoặc bị xóa, hoặc có thể bạn vừa nhập sai địa chỉ.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (465, N'Tại sao không thử và tìm cách của bạn bằng cách sử dụng thanh điều hướng ở trên hoặc nhấp vào biểu tượng để trở về trang chủ của chúng tôi.', N'Tại sao không thử và tìm cách của bạn bằng cách sử dụng thanh điều hướng ở trên hoặc nhấp vào biểu tượng để trở về trang chủ của chúng tôi.', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (466, N'Quản lý', N'Quản lý', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (467, N'Chào {0}!', N'Chào {0}!', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (468, N'Bảng điều khiển', N'Bảng điều khiển', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (469, N'Đăng xuất', N'Đăng xuất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (470, N'Tìm kiếm...', N'Tìm kiếm...', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (471, N'BẢN TIN', N'BẢN TIN', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (472, N'Tài khoản của bạn', N'Tài khoản của bạn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (473, N'Trang tổng quan tài khoản', N'Trang tổng quan tài khoản', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (474, N'Thông tin tài khoản', N'Thông tin tài khoản', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (475, N'Cài đặt của tôi', N'Cài đặt của tôi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (476, N'Địa chỉ', N'Địa chỉ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (477, N'Lịch sử đặt hàng', N'Lịch sử đặt hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (478, N'Danh sách mong muốn', N'Danh sách mong muốn', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (479, N'Sửa', N'Sửa', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (480, N'Bảo mật', N'Bảo mật', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (481, N'Mật khẩu', N'Mật khẩu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (482, N'Thay đổi', N'Thay đổi', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (483, N'Đăng nhập bên ngoài', N'Đăng nhập bên ngoài', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (484, N'Địa chỉ giao hàng mặc định', N'Địa chỉ giao hàng mặc định', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (485, N'Trang', N'Trang', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (486, N'Người dùng', N'Người dùng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (487, N'Nhà cung cấp', N'Nhà cung cấp', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (488, N'Nhóm khách hàng', N'Nhóm khách hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (489, N'Đánh giá', N'Đánh giá', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (490, N'Xem lại câu trả lời', N'Xem lại câu trả lời', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (491, N'Bình luận', N'Bình luận', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (492, N'Quản lý khu vực liên hệ', N'Quản lý khu vực liên hệ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (493, N'Quản lý liên hệ', N'Quản lý liên hệ', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (494, N'Mục lục', N'Mục lục', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (495, N'Các sản phẩm', N'Các sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (496, N'Giá sản phẩm', N'Giá sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (497, N'Thể loại', N'Thể loại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (498, N'Nhãn hiệu', N'Nhãn hiệu', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (499, N'Tùy chọn sản phẩm', N'Tùy chọn sản phẩm', N'vi-VN')
GO
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (500, N'Nhóm thuộc tính sản phẩm', N'Nhóm thuộc tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (501, N'Thuộc tính sản phẩm', N'Thuộc tính sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (502, N'Mẫu sản phẩm', N'Mẫu sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (503, N'Bán hàng', N'Bán hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (504, N'Đơn hàng', N'Đơn hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (505, N'Lô hàng', N'Lô hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (506, N'Hàng tồn kho', N'Hàng tồn kho', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (507, N'Nhà kho', N'Nhà kho', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (508, N'Quản lý kho sản phẩm', N'Quản lý kho sản phẩm', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (509, N'Khuyến mại', N'Khuyến mại', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (510, N'Quy tắc giỏ hàng', N'Quy tắc giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (511, N'Quy tắc giá giỏ hàng', N'Quy tắc giá giỏ hàng', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (512, N'Nội dung', N'Nội dung', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (513, N'Mục chính', N'Mục chính', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (514, N'Tường', N'Tường', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (515, N'Hệ thống', N'Hệ thống', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (516, N'Chủ đề', N'Chủ đề', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (517, N'Quốc gia', N'Quốc gia', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (518, N'Tiểu bang hoặc Tỉnh thành', N'Tiểu bang hoặc Tỉnh thành', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (519, N'Các hạng thuế', N'Các hạng thuế', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (520, N'Thuế suất', N'Thuế suất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (521, N'Nhà cung cấp vận chuyển', N'Nhà cung cấp vận chuyển', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (522, N'Nhà cung cấp thanh toán', N'Nhà cung cấp thanh toán', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (523, N'Cài đặt', N'Cài đặt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (524, N'Bản dịch', N'Bản dịch', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (525, N'Đơn đặt hàng mới nhất', N'Đơn đặt hàng mới nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (526, N'Đang chờ phê duyệt', N'Đang chờ phê duyệt', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (527, N'Câu trả lời đang chờ xem xét', N'Câu trả lời đang chờ xem xét', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (528, N'Từ khóa được tìm kiếm nhiều nhất', N'Từ khóa được tìm kiếm nhiều nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (529, N'Sản phẩm được xem nhiều nhất', N'Sản phẩm được xem nhiều nhất', N'vi-VN')
INSERT [dbo].[Localization_Resource] ([Id], [Key], [Value], [CultureId]) VALUES (530, N'Trang chủ', N'Trang chủ', N'vi-VN')
SET IDENTITY_INSERT [dbo].[Localization_Resource] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders_Order] ON 

INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (3, 10, CAST(N'2020-12-30T00:05:54.3382533+07:00' AS DateTimeOffset), 10, CAST(N'2020-12-30T00:05:54.3380790+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(4109000.00 AS Decimal(18, 2)), CAST(4109000.00 AS Decimal(18, 2)), 6, 5, 1, NULL, NULL, 0, N'Free', CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4109000.00 AS Decimal(18, 2)), N'CashOnDelivery', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (4, 554, CAST(N'2021-01-15T10:35:27.9184262+07:00' AS DateTimeOffset), 554, CAST(N'2021-01-15T10:29:44.6948370+07:00' AS DateTimeOffset), 554, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(46400000.00 AS Decimal(18, 2)), CAST(46400000.00 AS Decimal(18, 2)), 8, 7, 80, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(46410000.00 AS Decimal(18, 2)), N'MomoPayment', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (5, 10, CAST(N'2021-01-27T22:22:13.8411225+07:00' AS DateTimeOffset), 10, CAST(N'2021-01-27T22:22:13.8409783+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(18300000.00 AS Decimal(18, 2)), CAST(18300000.00 AS Decimal(18, 2)), 10, 9, 1, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(18310000.00 AS Decimal(18, 2)), N'CashOnDelivery', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (6, 10, CAST(N'2021-01-27T22:24:22.4230912+07:00' AS DateTimeOffset), 10, CAST(N'2021-01-27T22:24:22.4230910+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(23100000.00 AS Decimal(18, 2)), CAST(23100000.00 AS Decimal(18, 2)), 12, 11, 1, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(23110000.00 AS Decimal(18, 2)), N'CashOnDelivery', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (7, 10, CAST(N'2021-01-27T23:09:26.7166168+07:00' AS DateTimeOffset), 10, CAST(N'2021-01-27T23:09:26.7163763+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(2300000.00 AS Decimal(18, 2)), CAST(2300000.00 AS Decimal(18, 2)), 14, 13, 1, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(2310000.00 AS Decimal(18, 2)), N'CashOnDelivery', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (8, 556, CAST(N'2021-01-28T14:39:45.2796564+07:00' AS DateTimeOffset), 556, CAST(N'2021-01-28T14:39:45.2788490+07:00' AS DateTimeOffset), 556, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(19000000.00 AS Decimal(18, 2)), CAST(19000000.00 AS Decimal(18, 2)), 16, 15, 1, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(19010000.00 AS Decimal(18, 2)), N'CashOnDelivery', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (9, 556, CAST(N'2021-01-28T14:41:00.9818195+07:00' AS DateTimeOffset), 556, CAST(N'2021-01-28T14:41:00.9818186+07:00' AS DateTimeOffset), 556, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(19000000.00 AS Decimal(18, 2)), CAST(19000000.00 AS Decimal(18, 2)), 18, 17, 1, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(19010000.00 AS Decimal(18, 2)), N'CashOnDelivery', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_Order] ([Id], [CustomerId], [LatestUpdatedOn], [LatestUpdatedById], [CreatedOn], [CreatedById], [VendorId], [CouponCode], [CouponRuleName], [DiscountAmount], [SubTotal], [SubTotalWithDiscount], [ShippingAddressId], [BillingAddressId], [OrderStatus], [OrderNote], [ParentId], [IsMasterOrder], [ShippingMethod], [ShippingFeeAmount], [TaxAmount], [OrderTotal], [PaymentMethod], [PaymentFeeAmount]) VALUES (10, 10, CAST(N'2021-01-28T14:49:59.9809182+07:00' AS DateTimeOffset), 10, CAST(N'2021-01-28T14:44:49.7591480+07:00' AS DateTimeOffset), 10, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(7500000.00 AS Decimal(18, 2)), CAST(7500000.00 AS Decimal(18, 2)), 20, 19, 80, NULL, NULL, 0, N'Standard', CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(7510000.00 AS Decimal(18, 2)), N'MomoPayment', CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Orders_Order] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders_OrderAddress] ON 

INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (5, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (6, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (7, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 784, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (8, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 784, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (9, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (10, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (11, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (12, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (13, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (14, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (15, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 274, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (16, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 274, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (17, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 274, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (18, N'nghia', N'131231241231232', N'12312', NULL, N'ấdsda', N'áđâs', 274, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (19, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
INSERT [dbo].[Orders_OrderAddress] ([Id], [ContactName], [Phone], [AddressLine1], [AddressLine2], [City], [ZipCode], [DistrictId], [StateOrProvinceId], [CountryId]) VALUES (20, N'Nhu Tran', N'0397557146', N'97 A Hồng Lạc , P.10 , Q. Tân Bình', NULL, N'Hồ Chí Minh', N'7000', 250, 1, N'VN')
SET IDENTITY_INSERT [dbo].[Orders_OrderAddress] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders_OrderHistory] ON 

INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (4, 3, NULL, 1, N'{"CustomerId":10,"LatestUpdatedOn":"2020-12-30T00:05:54.3382533+07:00","LatestUpdatedById":10,"CreatedOn":"2020-12-30T00:05:54.338079+07:00","CreatedById":10,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":4109000.00,"SubTotalWithDiscount":4109000.00,"ShippingAddressId":6,"ShippingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":6},"BillingAddressId":5,"BillingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":5},"OrderItems":[{"ProductId":14,"ProductPrice":4109000.00,"Quantity":1,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":3}],"OrderStatus":1,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Free","ShippingFeeAmount":0.0,"TaxAmount":0.00,"OrderTotal":4109000.00,"PaymentMethod":"CashOnDelivery","PaymentFeeAmount":0.00,"Children":[],"Id":3}', NULL, CAST(N'2020-12-30T00:05:54.7780125+07:00' AS DateTimeOffset), 10)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (5, 4, NULL, 1, N'{"CustomerId":554,"LatestUpdatedOn":"2021-01-15T10:29:44.6951478+07:00","LatestUpdatedById":554,"CreatedOn":"2021-01-15T10:29:44.694837+07:00","CreatedById":554,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":46400000.00,"SubTotalWithDiscount":46400000.00,"ShippingAddressId":8,"ShippingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":784,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":8},"BillingAddressId":7,"BillingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":784,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":7},"OrderItems":[{"ProductId":17,"ProductPrice":2900000.00,"Quantity":8,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":4},{"ProductId":16,"ProductPrice":5800000.00,"Quantity":4,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":5}],"OrderStatus":20,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":46410000.00,"PaymentMethod":"MomoPayment","PaymentFeeAmount":0.0,"Children":[],"Id":4}', NULL, CAST(N'2021-01-15T10:29:45.0906428+07:00' AS DateTimeOffset), 554)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (6, 4, 20, 80, N'{"CustomerId":554,"LatestUpdatedOn":"2021-01-15T10:35:27.9184262+07:00","LatestUpdatedById":554,"CreatedOn":"2021-01-15T10:29:44.694837+07:00","CreatedById":554,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.00,"SubTotal":46400000.00,"SubTotalWithDiscount":46400000.00,"ShippingAddressId":8,"ShippingAddress":null,"BillingAddressId":7,"BillingAddress":null,"OrderItems":[{"ProductId":17,"ProductPrice":2900000.00,"Quantity":8,"DiscountAmount":0.00,"TaxAmount":0.00,"TaxPercent":0.00,"Id":4},{"ProductId":16,"ProductPrice":5800000.00,"Quantity":4,"DiscountAmount":0.00,"TaxAmount":0.00,"TaxPercent":0.00,"Id":5}],"OrderStatus":80,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":46410000.00,"PaymentMethod":"MomoPayment","PaymentFeeAmount":0.00,"Children":[],"Id":4}', N'System canceled', CAST(N'2021-01-15T10:35:27.9831824+07:00' AS DateTimeOffset), 2)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (7, 5, NULL, 1, N'{"CustomerId":10,"LatestUpdatedOn":"2021-01-27T22:22:13.8411225+07:00","LatestUpdatedById":10,"CreatedOn":"2021-01-27T22:22:13.8409783+07:00","CreatedById":10,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":18300000.00,"SubTotalWithDiscount":18300000.00,"ShippingAddressId":10,"ShippingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":10},"BillingAddressId":9,"BillingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":9},"OrderItems":[{"ProductId":18,"ProductPrice":4800000.00,"Quantity":2,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":6},{"ProductId":17,"ProductPrice":2900000.00,"Quantity":3,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":7}],"OrderStatus":1,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":18310000.00,"PaymentMethod":"CashOnDelivery","PaymentFeeAmount":0.00,"Children":[],"Id":5}', NULL, CAST(N'2021-01-27T22:22:14.6468480+07:00' AS DateTimeOffset), 10)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (8, 6, NULL, 1, N'{"CustomerId":10,"LatestUpdatedOn":"2021-01-27T22:24:22.4230912+07:00","LatestUpdatedById":10,"CreatedOn":"2021-01-27T22:24:22.423091+07:00","CreatedById":10,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":23100000.00,"SubTotalWithDiscount":23100000.00,"ShippingAddressId":12,"ShippingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":12},"BillingAddressId":11,"BillingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":11},"OrderItems":[{"ProductId":18,"ProductPrice":4800000.00,"Quantity":3,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":8},{"ProductId":17,"ProductPrice":2900000.00,"Quantity":3,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":9}],"OrderStatus":1,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":23110000.00,"PaymentMethod":"CashOnDelivery","PaymentFeeAmount":0.00,"Children":[],"Id":6}', NULL, CAST(N'2021-01-27T22:24:22.4837522+07:00' AS DateTimeOffset), 10)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (9, 7, NULL, 1, N'{"CustomerId":10,"LatestUpdatedOn":"2021-01-27T23:09:26.7166168+07:00","LatestUpdatedById":10,"CreatedOn":"2021-01-27T23:09:26.7163763+07:00","CreatedById":10,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":2300000.00,"SubTotalWithDiscount":2300000.00,"ShippingAddressId":14,"ShippingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":14},"BillingAddressId":13,"BillingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":13},"OrderItems":[{"ProductId":19,"ProductPrice":2300000.00,"Quantity":1,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":10}],"OrderStatus":1,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":2310000.00,"PaymentMethod":"CashOnDelivery","PaymentFeeAmount":0.00,"Children":[],"Id":7}', NULL, CAST(N'2021-01-27T23:09:26.9718546+07:00' AS DateTimeOffset), 10)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (10, 8, NULL, 1, N'{"CustomerId":556,"LatestUpdatedOn":"2021-01-28T14:39:45.2796564+07:00","LatestUpdatedById":556,"CreatedOn":"2021-01-28T14:39:45.278849+07:00","CreatedById":556,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":19000000.00,"SubTotalWithDiscount":19000000.00,"ShippingAddressId":16,"ShippingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":274,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":16},"BillingAddressId":15,"BillingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":274,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":15},"OrderItems":[{"ProductId":21,"ProductPrice":1500000.00,"Quantity":5,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":11},{"ProductId":19,"ProductPrice":2300000.00,"Quantity":5,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":12}],"OrderStatus":1,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":19010000.00,"PaymentMethod":"CashOnDelivery","PaymentFeeAmount":0.00,"Children":[],"Id":8}', NULL, CAST(N'2021-01-28T14:39:46.0528161+07:00' AS DateTimeOffset), 556)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (11, 9, NULL, 1, N'{"CustomerId":556,"LatestUpdatedOn":"2021-01-28T14:41:00.9818195+07:00","LatestUpdatedById":556,"CreatedOn":"2021-01-28T14:41:00.9818186+07:00","CreatedById":556,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":19000000.00,"SubTotalWithDiscount":19000000.00,"ShippingAddressId":18,"ShippingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":274,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":18},"BillingAddressId":17,"BillingAddress":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"City":"ấdsda","ZipCode":"áđâs","DistrictId":274,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":17},"OrderItems":[{"ProductId":21,"ProductPrice":1500000.00,"Quantity":5,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":13},{"ProductId":19,"ProductPrice":2300000.00,"Quantity":5,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":14}],"OrderStatus":1,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":19010000.00,"PaymentMethod":"CashOnDelivery","PaymentFeeAmount":0.00,"Children":[],"Id":9}', NULL, CAST(N'2021-01-28T14:41:01.3033665+07:00' AS DateTimeOffset), 556)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (12, 10, NULL, 1, N'{"CustomerId":10,"LatestUpdatedOn":"2021-01-28T14:44:49.7591485+07:00","LatestUpdatedById":10,"CreatedOn":"2021-01-28T14:44:49.759148+07:00","CreatedById":10,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.0,"SubTotal":7500000.00,"SubTotalWithDiscount":7500000.00,"ShippingAddressId":20,"ShippingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":20},"BillingAddressId":19,"BillingAddress":{"ContactName":"Nhu Tran","Phone":"0397557146","AddressLine1":"97 A Hồng Lạc , P.10 , Q. Tân Bình","AddressLine2":null,"City":"Hồ Chí Minh","ZipCode":"7000","DistrictId":250,"District":null,"StateOrProvinceId":1,"StateOrProvince":null,"CountryId":"VN","Country":null,"Id":19},"OrderItems":[{"ProductId":21,"ProductPrice":1500000.00,"Quantity":5,"DiscountAmount":0.0,"TaxAmount":0.00,"TaxPercent":0.0,"Id":15}],"OrderStatus":20,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":7510000.00,"PaymentMethod":"MomoPayment","PaymentFeeAmount":0.0,"Children":[],"Id":10}', NULL, CAST(N'2021-01-28T14:44:49.8840918+07:00' AS DateTimeOffset), 10)
INSERT [dbo].[Orders_OrderHistory] ([Id], [OrderId], [OldStatus], [NewStatus], [OrderSnapshot], [Note], [CreatedOn], [CreatedById]) VALUES (13, 10, 20, 80, N'{"CustomerId":10,"LatestUpdatedOn":"2021-01-28T14:49:59.9809182+07:00","LatestUpdatedById":10,"CreatedOn":"2021-01-28T14:44:49.759148+07:00","CreatedById":10,"VendorId":null,"CouponCode":null,"CouponRuleName":null,"DiscountAmount":0.00,"SubTotal":7500000.00,"SubTotalWithDiscount":7500000.00,"ShippingAddressId":20,"ShippingAddress":null,"BillingAddressId":19,"BillingAddress":null,"OrderItems":[{"ProductId":21,"ProductPrice":1500000.00,"Quantity":5,"DiscountAmount":0.00,"TaxAmount":0.00,"TaxPercent":0.00,"Id":15}],"OrderStatus":80,"OrderNote":null,"ParentId":null,"IsMasterOrder":false,"ShippingMethod":"Standard","ShippingFeeAmount":10000.00,"TaxAmount":0.00,"OrderTotal":7510000.00,"PaymentMethod":"MomoPayment","PaymentFeeAmount":0.00,"Children":[],"Id":10}', N'System canceled', CAST(N'2021-01-28T14:50:00.0990633+07:00' AS DateTimeOffset), 2)
SET IDENTITY_INSERT [dbo].[Orders_OrderHistory] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders_OrderItem] ON 

INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (4, 4, 17, CAST(2900000.00 AS Decimal(18, 2)), 8, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (5, 4, 16, CAST(5800000.00 AS Decimal(18, 2)), 4, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (6, 5, 18, CAST(4800000.00 AS Decimal(18, 2)), 2, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (7, 5, 17, CAST(2900000.00 AS Decimal(18, 2)), 3, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (8, 6, 18, CAST(4800000.00 AS Decimal(18, 2)), 3, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (9, 6, 17, CAST(2900000.00 AS Decimal(18, 2)), 3, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (10, 7, 19, CAST(2300000.00 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (11, 8, 21, CAST(1500000.00 AS Decimal(18, 2)), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (12, 8, 19, CAST(2300000.00 AS Decimal(18, 2)), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (13, 9, 21, CAST(1500000.00 AS Decimal(18, 2)), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (14, 9, 19, CAST(2300000.00 AS Decimal(18, 2)), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Orders_OrderItem] ([Id], [OrderId], [ProductId], [ProductPrice], [Quantity], [DiscountAmount], [TaxAmount], [TaxPercent]) VALUES (15, 10, 21, CAST(1500000.00 AS Decimal(18, 2)), 5, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Orders_OrderItem] OFF
GO
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'Braintree', N'Braintree', 1, N'payments-braintree-config', N'BraintreeLanding', N'{"PublicKey": "nmqxkdkn366z52ws", "PrivateKey" : "a5b25e3758d09cbef2c8ac7c10b9465c", "MerchantId" : "ct5s7t3zwjpfqkpj", "IsProduction" : "false"}')
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'Cashfree', N'Cashfree Payment Gateway', 0, N'payments-cashfree-config', N'CashfreeLanding', N'{ "IsSandbox":true, "AppId":"358035b02486f36ca27904540853", "SecretKey":"26f48dcd6a27f89f59f28e65849e587916dd57b9" }')
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'CoD', N'
Thanh toán khi giao hàng', 1, N'payments-cod-config', N'CoDLanding', NULL)
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'MomoPayment', N'Thanh toán MoMo', 1, N'payments-momo-config', N'MomoLanding', N'{"IsSandbox":true,"PartnerCode":"MOMOKPX820201229","AccessKey":"u2SpNUspMxE8XblU","SecretKey":"JKGIQFQLitFkeKbzKjMuJ60V0RLN2Wun","PaymentFee":0.0}')
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'NganLuong', N'Ngan Luong Payment', 0, N'payments-nganluong-config', N'NganLuongLanding', N'{"IsSandbox":true, "MerchantId": 47249, "MerchantPassword": "e530745693dbde678f9da98a7c821a07", "ReceiverEmail": "nlqthien@gmail.com"}')
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'PaypalExpress', N'Paypal Express', 1, N'payments-paypalExpress-config', N'PaypalExpressLanding', N'{"IsSandbox":true,"ClientId":"AS6NjI8lPwrJbbDII_Ky8_sKMSqV2NdHBEdmhcUIvgMYATL6OUm0RHVJBN5h5vl73Sf7cj6RWNrfZAWR","ClientSecret":"EB4ShA4A45q59MbDSqe9sg1LPrhl6E7bXHcDyEvrm6HZDrdNh5tPXwjHH3rp5LMTLK5fJCX-2WbnZuu7","PaymentFee":5.0,"Environment":"sandbox","EnvironmentUrlPart":".sandbox"}')
INSERT [dbo].[Payments_PaymentProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [LandingViewComponentName], [AdditionalSettings]) VALUES (N'Stripe', N'Stripe', 0, N'payments-stripe-config', N'StripeLanding', N'{"PublicKey": "pk_test_6pRNASCoBOKtIshFeQd4XMUh", "PrivateKey" : "sk_test_BQokikJOvBiI2HlWgH4olfQ2"}')
GO
SET IDENTITY_INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ON 

INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (1, 18, 22, CAST(N'2020-11-22T09:30:07.5225485+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (2, 29, 14, CAST(N'2020-11-22T09:30:37.7436816+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (3, 285, 5, CAST(N'2020-11-25T01:32:55.1736594+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (4, 294, 14, CAST(N'2020-11-25T23:10:36.9760436+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (5, 10, 14, CAST(N'2021-01-05T10:45:25.9154088+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (6, 35, 9, CAST(N'2020-11-28T23:16:32.2108768+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (7, 10, 5, CAST(N'2021-01-05T11:03:39.2374272+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (8, 10, 1, CAST(N'2020-12-14T23:43:05.8765381+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (9, 54, 9, CAST(N'2020-11-30T22:04:13.9817819+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (10, 92, 14, CAST(N'2020-11-30T22:06:48.7335418+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (11, 99, 5, CAST(N'2020-11-30T22:06:55.9554111+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (12, 488, 14, CAST(N'2020-12-09T01:19:56.7147918+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (13, 10, 9, CAST(N'2021-01-05T10:58:19.2686031+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (14, 0, 14, CAST(N'2021-01-05T10:37:45.5537644+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (15, 0, 9, CAST(N'2020-12-14T23:38:26.1724845+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (16, 10, 15, CAST(N'2021-01-05T12:28:55.0325239+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (17, 10, 17, CAST(N'2021-01-27T22:24:02.9434913+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (18, 10, 18, CAST(N'2021-01-27T23:09:33.3970603+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (19, 0, 18, CAST(N'2021-01-27T22:08:45.0260574+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (20, 10, 19, CAST(N'2021-01-27T23:09:12.0457020+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (21, 554, 17, CAST(N'2021-01-15T10:28:21.9706058+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (22, 554, 16, CAST(N'2021-01-15T10:33:02.6479929+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (23, 554, 18, CAST(N'2021-01-15T10:31:01.4448407+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (24, 554, 19, CAST(N'2021-01-15T10:31:57.0508316+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (25, 0, 21, CAST(N'2021-01-28T14:25:30.7900953+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (26, 0, 15, CAST(N'2021-01-27T21:34:52.1626928+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (27, 0, 20, CAST(N'2021-01-28T14:12:23.3832020+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (28, 0, 19, CAST(N'2021-01-27T22:04:43.2525342+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (29, 10, 20, CAST(N'2021-01-27T23:09:05.2062044+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (30, 556, 21, CAST(N'2021-01-28T14:40:18.1835961+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (31, 556, 20, CAST(N'2021-01-28T14:37:53.6709542+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (32, 556, 19, CAST(N'2021-01-28T14:40:29.5527628+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (33, 10, 22, CAST(N'2021-01-28T14:48:13.9222558+07:00' AS DateTimeOffset))
INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] ([Id], [UserId], [ProductId], [LatestViewedOn]) VALUES (34, 10, 21, CAST(N'2021-01-28T14:44:24.0496702+07:00' AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[ProductRecentlyViewed_RecentlyViewedProduct] OFF
GO
SET IDENTITY_INSERT [dbo].[Search_Query] ON 

INSERT [dbo].[Search_Query] ([Id], [QueryText], [ResultsCount], [CreatedOn]) VALUES (1, N'air', 2, CAST(N'2021-01-15T10:27:36.7633621+07:00' AS DateTimeOffset))
INSERT [dbo].[Search_Query] ([Id], [QueryText], [ResultsCount], [CreatedOn]) VALUES (2, N'air', 3, CAST(N'2021-01-28T14:20:47.0338750+07:00' AS DateTimeOffset))
INSERT [dbo].[Search_Query] ([Id], [QueryText], [ResultsCount], [CreatedOn]) VALUES (3, N'XSPORT', 2, CAST(N'2021-01-28T14:24:46.1990331+07:00' AS DateTimeOffset))
INSERT [dbo].[Search_Query] ([Id], [QueryText], [ResultsCount], [CreatedOn]) VALUES (4, N'XSPORT', 1, CAST(N'2021-01-28T14:25:09.3476013+07:00' AS DateTimeOffset))
INSERT [dbo].[Search_Query] ([Id], [QueryText], [ResultsCount], [CreatedOn]) VALUES (5, N'XSPORT', 2, CAST(N'2021-01-28T14:25:11.1451577+07:00' AS DateTimeOffset))
INSERT [dbo].[Search_Query] ([Id], [QueryText], [ResultsCount], [CreatedOn]) VALUES (6, N'air', 4, CAST(N'2021-01-28T14:50:08.0038650+07:00' AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[Search_Query] OFF
GO
INSERT [dbo].[Shipping_ShippingProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [ToAllShippingEnabledCountries], [OnlyCountryIdsString], [ToAllShippingEnabledStatesOrProvinces], [OnlyStateOrProvinceIdsString], [AdditionalSettings], [ShippingPriceServiceTypeName]) VALUES (N'FreeShip', N'Free Ship', 0, N'', 1, NULL, 1, NULL, N'{MinimumOrderAmount : 1}', N'SimplCommerce.Module.ShippingFree.Services.FreeShippingServiceProvider,SimplCommerce.Module.ShippingFree')
INSERT [dbo].[Shipping_ShippingProvider] ([Id], [Name], [IsEnabled], [ConfigureUrl], [ToAllShippingEnabledCountries], [OnlyCountryIdsString], [ToAllShippingEnabledStatesOrProvinces], [OnlyStateOrProvinceIdsString], [AdditionalSettings], [ShippingPriceServiceTypeName]) VALUES (N'TableRate', N'Table Rate', 1, N'shipping-table-rate-config', 1, NULL, 1, NULL, NULL, N'SimplCommerce.Module.ShippingTableRate.Services.TableRateShippingServiceProvider,SimplCommerce.Module.ShippingTableRate')
GO
SET IDENTITY_INSERT [dbo].[ShippingTableRate_PriceAndDestination] ON 

INSERT [dbo].[ShippingTableRate_PriceAndDestination] ([Id], [CountryId], [StateOrProvinceId], [DistrictId], [ZipCode], [Note], [MinOrderSubtotal], [ShippingPrice]) VALUES (1, N'VN', NULL, NULL, NULL, NULL, CAST(0.00 AS Decimal(18, 2)), CAST(10000.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[ShippingTableRate_PriceAndDestination] OFF
GO
SET IDENTITY_INSERT [dbo].[ShoppingCart_Cart] ON 

INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (16, 10, 10, CAST(N'2020-12-29T21:53:13.7644830+07:00' AS DateTimeOffset), CAST(N'2020-12-29T21:53:13.7646121+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Free', 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":0,"BillingAddressId":0,"ShippingMethod":"Free","NewAddressForm":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"StateOrProvinceId":1,"DistrictId":250,"CountryId":"VN","City":"ấdsda","ZipCode":"áđâs","StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (17, 10, 10, CAST(N'2020-12-29T22:41:38.2385965+07:00' AS DateTimeOffset), CAST(N'2020-12-29T22:41:38.2387092+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Free', 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":1,"BillingAddressId":0,"ShippingMethod":"Free","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (19, 10, 10, CAST(N'2020-12-29T23:55:53.9933878+07:00' AS DateTimeOffset), CAST(N'2020-12-29T23:55:53.9937226+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Free', 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":1,"BillingAddressId":0,"ShippingMethod":"Free","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (23, 554, 554, CAST(N'2021-01-15T10:28:25.1755035+07:00' AS DateTimeOffset), CAST(N'2021-01-15T10:28:25.1760476+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":0,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"StateOrProvinceId":1,"DistrictId":784,"CountryId":"VN","City":"ấdsda","ZipCode":"áđâs","StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (33, 10, 10, CAST(N'2021-01-27T22:12:48.4441825+07:00' AS DateTimeOffset), CAST(N'2021-01-27T22:12:48.4443563+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":1,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (34, 10, 10, CAST(N'2021-01-27T22:23:58.5552572+07:00' AS DateTimeOffset), CAST(N'2021-01-27T22:23:58.5552588+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":1,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (38, 10, 10, CAST(N'2021-01-27T23:09:13.3912352+07:00' AS DateTimeOffset), CAST(N'2021-01-27T23:09:13.3914197+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":1,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (39, 556, 556, CAST(N'2021-01-28T14:38:28.3988662+07:00' AS DateTimeOffset), CAST(N'2021-01-28T14:38:28.3990671+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":0,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":"nghia","Phone":"131231241231232","AddressLine1":"12312","AddressLine2":null,"StateOrProvinceId":1,"DistrictId":274,"CountryId":"VN","City":"ấdsda","ZipCode":"áđâs","StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (40, 556, 556, CAST(N'2021-01-28T14:40:21.7374700+07:00' AS DateTimeOffset), CAST(N'2021-01-28T14:40:21.7377057+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":3,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
INSERT [dbo].[ShoppingCart_Cart] ([Id], [CustomerId], [CreatedById], [CreatedOn], [LatestUpdatedOn], [IsActive], [CouponCode], [CouponRuleName], [ShippingMethod], [IsProductPriceIncludeTax], [ShippingAmount], [TaxAmount], [ShippingData], [OrderNote], [LockedOnCheckout]) VALUES (41, 10, 10, CAST(N'2021-01-28T14:44:26.3578602+07:00' AS DateTimeOffset), CAST(N'2021-01-28T14:44:26.3578620+07:00' AS DateTimeOffset), 0, NULL, NULL, N'Standard', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'{"ExistingShippingAddresses":[],"ShippingAddressId":1,"BillingAddressId":0,"ShippingMethod":"Standard","NewAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"UseShippingAddressAsBillingAddress":true,"NewBillingAddressForm":{"ContactName":null,"Phone":null,"AddressLine1":null,"AddressLine2":null,"StateOrProvinceId":0,"DistrictId":null,"CountryId":null,"City":null,"ZipCode":null,"StateOrProvinces":null,"Districts":null,"ShipableContries":null},"OrderNote":null}', NULL, 1)
SET IDENTITY_INSERT [dbo].[ShoppingCart_Cart] OFF
GO
SET IDENTITY_INSERT [dbo].[ShoppingCart_CartItem] ON 

INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (21, CAST(N'2021-01-15T10:28:25.2008592+07:00' AS DateTimeOffset), 17, 8, 23)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (22, CAST(N'2021-01-15T10:28:59.9786169+07:00' AS DateTimeOffset), 16, 4, 23)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (26, CAST(N'2021-01-27T22:12:48.4584738+07:00' AS DateTimeOffset), 18, 2, 33)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (27, CAST(N'2021-01-27T22:12:58.6260622+07:00' AS DateTimeOffset), 17, 3, 33)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (28, CAST(N'2021-01-27T22:23:58.5560923+07:00' AS DateTimeOffset), 18, 3, 34)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (29, CAST(N'2021-01-27T22:24:05.1869930+07:00' AS DateTimeOffset), 17, 3, 34)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (30, CAST(N'2021-01-27T23:09:13.4225466+07:00' AS DateTimeOffset), 19, 1, 38)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (31, CAST(N'2021-01-28T14:38:28.4192588+07:00' AS DateTimeOffset), 21, 5, 39)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (32, CAST(N'2021-01-28T14:38:48.7753602+07:00' AS DateTimeOffset), 19, 5, 39)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (33, CAST(N'2021-01-28T14:40:21.7394330+07:00' AS DateTimeOffset), 21, 5, 40)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (34, CAST(N'2021-01-28T14:40:33.1119864+07:00' AS DateTimeOffset), 19, 5, 40)
INSERT [dbo].[ShoppingCart_CartItem] ([Id], [CreatedOn], [ProductId], [Quantity], [CartId]) VALUES (35, CAST(N'2021-01-28T14:44:26.3583736+07:00' AS DateTimeOffset), 21, 5, 41)
SET IDENTITY_INSERT [dbo].[ShoppingCart_CartItem] OFF
GO
SET IDENTITY_INSERT [dbo].[Tax_TaxClass] ON 

INSERT [dbo].[Tax_TaxClass] ([Id], [Name]) VALUES (1, N'VAT')
SET IDENTITY_INSERT [dbo].[Tax_TaxClass] OFF
GO
SET IDENTITY_INSERT [dbo].[WishList_WishList] ON 

INSERT [dbo].[WishList_WishList] ([Id], [UserId], [SharingCode], [CreatedOn], [LatestUpdatedOn]) VALUES (1, 2, NULL, CAST(N'2020-11-25T23:24:59.0996599+07:00' AS DateTimeOffset), CAST(N'2020-11-25T23:24:59.1000223+07:00' AS DateTimeOffset))
INSERT [dbo].[WishList_WishList] ([Id], [UserId], [SharingCode], [CreatedOn], [LatestUpdatedOn]) VALUES (2, 10, NULL, CAST(N'2020-12-08T02:17:14.5808431+07:00' AS DateTimeOffset), CAST(N'2020-12-08T02:17:14.5815591+07:00' AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[WishList_WishList] OFF
GO
/****** Object:  Index [IX_ActivityLog_Activity_ActivityTypeId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ActivityLog_Activity_ActivityTypeId] ON [dbo].[ActivityLog_Activity]
(
	[ActivityTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Category_ParentId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Category_ParentId] ON [dbo].[Catalog_Category]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Category_ThumbnailImageId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Category_ThumbnailImageId] ON [dbo].[Catalog_Category]
(
	[ThumbnailImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Product_BrandId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Product_BrandId] ON [dbo].[Catalog_Product]
(
	[BrandId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Product_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Product_CreatedById] ON [dbo].[Catalog_Product]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Product_LatestUpdatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Product_LatestUpdatedById] ON [dbo].[Catalog_Product]
(
	[LatestUpdatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Product_TaxClassId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Product_TaxClassId] ON [dbo].[Catalog_Product]
(
	[TaxClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_Product_ThumbnailImageId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_Product_ThumbnailImageId] ON [dbo].[Catalog_Product]
(
	[ThumbnailImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductAttribute_GroupId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductAttribute_GroupId] ON [dbo].[Catalog_ProductAttribute]
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductCategory_CategoryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductCategory_CategoryId] ON [dbo].[Catalog_ProductCategory]
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductCategory_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductCategory_ProductId] ON [dbo].[Catalog_ProductCategory]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductOptionCombination_OptionId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductOptionCombination_OptionId] ON [dbo].[Catalog_ProductOptionCombination]
(
	[OptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductOptionCombination_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductOptionCombination_ProductId] ON [dbo].[Catalog_ProductOptionCombination]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductOptionValue_OptionId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductOptionValue_OptionId] ON [dbo].[Catalog_ProductOptionValue]
(
	[OptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductOptionValue_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductOptionValue_ProductId] ON [dbo].[Catalog_ProductOptionValue]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductPriceHistory_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductPriceHistory_CreatedById] ON [dbo].[Catalog_ProductPriceHistory]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductPriceHistory_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductPriceHistory_ProductId] ON [dbo].[Catalog_ProductPriceHistory]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Catalog_ProductTemplateProductAttribute_ProductAttributeId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Catalog_ProductTemplateProductAttribute_ProductAttributeId] ON [dbo].[Catalog_ProductTemplateProductAttribute]
(
	[ProductAttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cms_MenuItem_EntityId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Cms_MenuItem_EntityId] ON [dbo].[Cms_MenuItem]
(
	[EntityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cms_MenuItem_MenuId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Cms_MenuItem_MenuId] ON [dbo].[Cms_MenuItem]
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cms_MenuItem_ParentId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Cms_MenuItem_ParentId] ON [dbo].[Cms_MenuItem]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cms_Page_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Cms_Page_CreatedById] ON [dbo].[Cms_Page]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Cms_Page_LatestUpdatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Cms_Page_LatestUpdatedById] ON [dbo].[Cms_Page]
(
	[LatestUpdatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Comments_Comment_ParentId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Comments_Comment_ParentId] ON [dbo].[Comments_Comment]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Comments_Comment_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Comments_Comment_UserId] ON [dbo].[Comments_Comment]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Contacts_Contact_ContactAreaId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Contacts_Contact_ContactAreaId] ON [dbo].[Contacts_Contact]
(
	[ContactAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Core_Address_CountryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_Address_CountryId] ON [dbo].[Core_Address]
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_Address_DistrictId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_Address_DistrictId] ON [dbo].[Core_Address]
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_Address_StateOrProvinceId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_Address_StateOrProvinceId] ON [dbo].[Core_Address]
(
	[StateOrProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Core_CustomerGroup_Name]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Core_CustomerGroup_Name] ON [dbo].[Core_CustomerGroup]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_CustomerGroupUser_CustomerGroupId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_CustomerGroupUser_CustomerGroupId] ON [dbo].[Core_CustomerGroupUser]
(
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_District_StateOrProvinceId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_District_StateOrProvinceId] ON [dbo].[Core_District]
(
	[StateOrProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Core_Entity_EntityTypeId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_Entity_EntityTypeId] ON [dbo].[Core_Entity]
(
	[EntityTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[Core_Role]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_RoleClaim_RoleId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_RoleClaim_RoleId] ON [dbo].[Core_RoleClaim]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Core_StateOrProvince_CountryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_StateOrProvince_CountryId] ON [dbo].[Core_StateOrProvince]
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[Core_User]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_User_DefaultBillingAddressId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_User_DefaultBillingAddressId] ON [dbo].[Core_User]
(
	[DefaultBillingAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_User_DefaultShippingAddressId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_User_DefaultShippingAddressId] ON [dbo].[Core_User]
(
	[DefaultShippingAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_User_VendorId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_User_VendorId] ON [dbo].[Core_User]
(
	[VendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[Core_User]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_UserAddress_AddressId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_UserAddress_AddressId] ON [dbo].[Core_UserAddress]
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_UserAddress_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_UserAddress_UserId] ON [dbo].[Core_UserAddress]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_UserClaim_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_UserClaim_UserId] ON [dbo].[Core_UserClaim]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_UserLogin_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_UserLogin_UserId] ON [dbo].[Core_UserLogin]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_UserRole_RoleId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_UserRole_RoleId] ON [dbo].[Core_UserRole]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Core_WidgetInstance_WidgetId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_WidgetInstance_WidgetId] ON [dbo].[Core_WidgetInstance]
(
	[WidgetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Core_WidgetInstance_WidgetZoneId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Core_WidgetInstance_WidgetZoneId] ON [dbo].[Core_WidgetInstance]
(
	[WidgetZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_Stock_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_Stock_ProductId] ON [dbo].[Inventory_Stock]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_Stock_WarehouseId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_Stock_WarehouseId] ON [dbo].[Inventory_Stock]
(
	[WarehouseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_StockHistory_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_StockHistory_CreatedById] ON [dbo].[Inventory_StockHistory]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_StockHistory_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_StockHistory_ProductId] ON [dbo].[Inventory_StockHistory]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_StockHistory_WarehouseId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_StockHistory_WarehouseId] ON [dbo].[Inventory_StockHistory]
(
	[WarehouseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_Warehouse_AddressId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_Warehouse_AddressId] ON [dbo].[Inventory_Warehouse]
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Inventory_Warehouse_VendorId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Inventory_Warehouse_VendorId] ON [dbo].[Inventory_Warehouse]
(
	[VendorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Localization_LocalizedContentProperty_CultureId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Localization_LocalizedContentProperty_CultureId] ON [dbo].[Localization_LocalizedContentProperty]
(
	[CultureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Localization_Resource_CultureId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Localization_Resource_CultureId] ON [dbo].[Localization_Resource]
(
	[CultureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_News_NewsItem_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_News_NewsItem_CreatedById] ON [dbo].[News_NewsItem]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_News_NewsItem_LatestUpdatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_News_NewsItem_LatestUpdatedById] ON [dbo].[News_NewsItem]
(
	[LatestUpdatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_News_NewsItem_ThumbnailImageId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_News_NewsItem_ThumbnailImageId] ON [dbo].[News_NewsItem]
(
	[ThumbnailImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_News_NewsItemCategory_NewsItemId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_News_NewsItemCategory_NewsItemId] ON [dbo].[News_NewsItemCategory]
(
	[NewsItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_Order_BillingAddressId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_Order_BillingAddressId] ON [dbo].[Orders_Order]
(
	[BillingAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_Order_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_Order_CreatedById] ON [dbo].[Orders_Order]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_Order_CustomerId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_Order_CustomerId] ON [dbo].[Orders_Order]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_Order_LatestUpdatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_Order_LatestUpdatedById] ON [dbo].[Orders_Order]
(
	[LatestUpdatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_Order_ParentId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_Order_ParentId] ON [dbo].[Orders_Order]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_Order_ShippingAddressId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_Order_ShippingAddressId] ON [dbo].[Orders_Order]
(
	[ShippingAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Orders_OrderAddress_CountryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderAddress_CountryId] ON [dbo].[Orders_OrderAddress]
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_OrderAddress_DistrictId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderAddress_DistrictId] ON [dbo].[Orders_OrderAddress]
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_OrderAddress_StateOrProvinceId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderAddress_StateOrProvinceId] ON [dbo].[Orders_OrderAddress]
(
	[StateOrProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_OrderHistory_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderHistory_CreatedById] ON [dbo].[Orders_OrderHistory]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_OrderHistory_OrderId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderHistory_OrderId] ON [dbo].[Orders_OrderHistory]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_OrderItem_OrderId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderItem_OrderId] ON [dbo].[Orders_OrderItem]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Orders_OrderItem_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Orders_OrderItem_ProductId] ON [dbo].[Orders_OrderItem]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Payments_Payment_OrderId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Payments_Payment_OrderId] ON [dbo].[Payments_Payment]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CartRuleCategory_CategoryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CartRuleCategory_CategoryId] ON [dbo].[Pricing_CartRuleCategory]
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CartRuleCustomerGroup_CustomerGroupId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CartRuleCustomerGroup_CustomerGroupId] ON [dbo].[Pricing_CartRuleCustomerGroup]
(
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CartRuleProduct_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CartRuleProduct_ProductId] ON [dbo].[Pricing_CartRuleProduct]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CartRuleUsage_CartRuleId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CartRuleUsage_CartRuleId] ON [dbo].[Pricing_CartRuleUsage]
(
	[CartRuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CartRuleUsage_CouponId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CartRuleUsage_CouponId] ON [dbo].[Pricing_CartRuleUsage]
(
	[CouponId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CartRuleUsage_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CartRuleUsage_UserId] ON [dbo].[Pricing_CartRuleUsage]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_CatalogRuleCustomerGroup_CustomerGroupId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_CatalogRuleCustomerGroup_CustomerGroupId] ON [dbo].[Pricing_CatalogRuleCustomerGroup]
(
	[CustomerGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Pricing_Coupon_CartRuleId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Pricing_Coupon_CartRuleId] ON [dbo].[Pricing_Coupon]
(
	[CartRuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductComparison_ComparingProduct_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ProductComparison_ComparingProduct_ProductId] ON [dbo].[ProductComparison_ComparingProduct]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductComparison_ComparingProduct_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ProductComparison_ComparingProduct_UserId] ON [dbo].[ProductComparison_ComparingProduct]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Reviews_Reply_ReviewId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Reviews_Reply_ReviewId] ON [dbo].[Reviews_Reply]
(
	[ReviewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Reviews_Reply_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Reviews_Reply_UserId] ON [dbo].[Reviews_Reply]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Reviews_Review_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Reviews_Review_UserId] ON [dbo].[Reviews_Review]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Shipments_Shipment_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Shipments_Shipment_CreatedById] ON [dbo].[Shipments_Shipment]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Shipments_Shipment_OrderId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Shipments_Shipment_OrderId] ON [dbo].[Shipments_Shipment]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Shipments_Shipment_WarehouseId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Shipments_Shipment_WarehouseId] ON [dbo].[Shipments_Shipment]
(
	[WarehouseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Shipments_ShipmentItem_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Shipments_ShipmentItem_ProductId] ON [dbo].[Shipments_ShipmentItem]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Shipments_ShipmentItem_ShipmentId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Shipments_ShipmentItem_ShipmentId] ON [dbo].[Shipments_ShipmentItem]
(
	[ShipmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ShippingTableRate_PriceAndDestination_CountryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShippingTableRate_PriceAndDestination_CountryId] ON [dbo].[ShippingTableRate_PriceAndDestination]
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShippingTableRate_PriceAndDestination_DistrictId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShippingTableRate_PriceAndDestination_DistrictId] ON [dbo].[ShippingTableRate_PriceAndDestination]
(
	[DistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShippingTableRate_PriceAndDestination_StateOrProvinceId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShippingTableRate_PriceAndDestination_StateOrProvinceId] ON [dbo].[ShippingTableRate_PriceAndDestination]
(
	[StateOrProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShoppingCart_Cart_CreatedById]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShoppingCart_Cart_CreatedById] ON [dbo].[ShoppingCart_Cart]
(
	[CreatedById] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShoppingCart_Cart_CustomerId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShoppingCart_Cart_CustomerId] ON [dbo].[ShoppingCart_Cart]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShoppingCart_CartItem_CartId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShoppingCart_CartItem_CartId] ON [dbo].[ShoppingCart_CartItem]
(
	[CartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShoppingCart_CartItem_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_ShoppingCart_CartItem_ProductId] ON [dbo].[ShoppingCart_CartItem]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Tax_TaxRate_CountryId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Tax_TaxRate_CountryId] ON [dbo].[Tax_TaxRate]
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Tax_TaxRate_StateOrProvinceId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Tax_TaxRate_StateOrProvinceId] ON [dbo].[Tax_TaxRate]
(
	[StateOrProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Tax_TaxRate_TaxClassId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_Tax_TaxRate_TaxClassId] ON [dbo].[Tax_TaxRate]
(
	[TaxClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_WishList_WishList_UserId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_WishList_WishList_UserId] ON [dbo].[WishList_WishList]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_WishList_WishListItem_ProductId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_WishList_WishListItem_ProductId] ON [dbo].[WishList_WishListItem]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_WishList_WishListItem_WishListId]    Script Date: 29/01/2021 9:48:56 SA ******/
CREATE NONCLUSTERED INDEX [IX_WishList_WishListItem_WishListId] ON [dbo].[WishList_WishListItem]
(
	[WishListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ShoppingCart_Cart] ADD  DEFAULT (CONVERT([bit],(0))) FOR [LockedOnCheckout]
GO
ALTER TABLE [dbo].[ActivityLog_Activity]  WITH CHECK ADD  CONSTRAINT [FK_ActivityLog_Activity_ActivityLog_ActivityType_ActivityTypeId] FOREIGN KEY([ActivityTypeId])
REFERENCES [dbo].[ActivityLog_ActivityType] ([Id])
GO
ALTER TABLE [dbo].[ActivityLog_Activity] CHECK CONSTRAINT [FK_ActivityLog_Activity_ActivityLog_ActivityType_ActivityTypeId]
GO
ALTER TABLE [dbo].[Catalog_Category]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Category_Catalog_Category_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Catalog_Category] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Category] CHECK CONSTRAINT [FK_Catalog_Category_Catalog_Category_ParentId]
GO
ALTER TABLE [dbo].[Catalog_Category]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Category_Core_Media_ThumbnailImageId] FOREIGN KEY([ThumbnailImageId])
REFERENCES [dbo].[Core_Media] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Category] CHECK CONSTRAINT [FK_Catalog_Category_Core_Media_ThumbnailImageId]
GO
ALTER TABLE [dbo].[Catalog_Product]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Product_Catalog_Brand_BrandId] FOREIGN KEY([BrandId])
REFERENCES [dbo].[Catalog_Brand] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Product] CHECK CONSTRAINT [FK_Catalog_Product_Catalog_Brand_BrandId]
GO
ALTER TABLE [dbo].[Catalog_Product]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Product_Core_Media_ThumbnailImageId] FOREIGN KEY([ThumbnailImageId])
REFERENCES [dbo].[Core_Media] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Product] CHECK CONSTRAINT [FK_Catalog_Product_Core_Media_ThumbnailImageId]
GO
ALTER TABLE [dbo].[Catalog_Product]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Product_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Product] CHECK CONSTRAINT [FK_Catalog_Product_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Catalog_Product]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Product_Core_User_LatestUpdatedById] FOREIGN KEY([LatestUpdatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Product] CHECK CONSTRAINT [FK_Catalog_Product_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[Catalog_Product]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_Product_Tax_TaxClass_TaxClassId] FOREIGN KEY([TaxClassId])
REFERENCES [dbo].[Tax_TaxClass] ([Id])
GO
ALTER TABLE [dbo].[Catalog_Product] CHECK CONSTRAINT [FK_Catalog_Product_Tax_TaxClass_TaxClassId]
GO
ALTER TABLE [dbo].[Catalog_ProductAttribute]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductAttribute_Catalog_ProductAttributeGroup_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Catalog_ProductAttributeGroup] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductAttribute] CHECK CONSTRAINT [FK_Catalog_ProductAttribute_Catalog_ProductAttributeGroup_GroupId]
GO
ALTER TABLE [dbo].[Catalog_ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductCategory_Catalog_Category_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Catalog_Category] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductCategory] CHECK CONSTRAINT [FK_Catalog_ProductCategory_Catalog_Category_CategoryId]
GO
ALTER TABLE [dbo].[Catalog_ProductCategory]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductCategory_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductCategory] CHECK CONSTRAINT [FK_Catalog_ProductCategory_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionCombination]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductOptionCombination_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductOptionCombination] CHECK CONSTRAINT [FK_Catalog_ProductOptionCombination_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionCombination]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductOptionCombination_Catalog_ProductOption_OptionId] FOREIGN KEY([OptionId])
REFERENCES [dbo].[Catalog_ProductOption] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductOptionCombination] CHECK CONSTRAINT [FK_Catalog_ProductOptionCombination_Catalog_ProductOption_OptionId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionValue]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductOptionValue_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductOptionValue] CHECK CONSTRAINT [FK_Catalog_ProductOptionValue_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductOptionValue]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductOptionValue_Catalog_ProductOption_OptionId] FOREIGN KEY([OptionId])
REFERENCES [dbo].[Catalog_ProductOption] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductOptionValue] CHECK CONSTRAINT [FK_Catalog_ProductOptionValue_Catalog_ProductOption_OptionId]
GO
ALTER TABLE [dbo].[Catalog_ProductPriceHistory]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductPriceHistory_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductPriceHistory] CHECK CONSTRAINT [FK_Catalog_ProductPriceHistory_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Catalog_ProductPriceHistory]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductPriceHistory_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Catalog_ProductPriceHistory] CHECK CONSTRAINT [FK_Catalog_ProductPriceHistory_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Catalog_ProductTemplateProductAttribute]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductTemplateProductAttribute_Catalog_ProductAttribute_ProductAttributeId] FOREIGN KEY([ProductAttributeId])
REFERENCES [dbo].[Catalog_ProductAttribute] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Catalog_ProductTemplateProductAttribute] CHECK CONSTRAINT [FK_Catalog_ProductTemplateProductAttribute_Catalog_ProductAttribute_ProductAttributeId]
GO
ALTER TABLE [dbo].[Catalog_ProductTemplateProductAttribute]  WITH CHECK ADD  CONSTRAINT [FK_Catalog_ProductTemplateProductAttribute_Catalog_ProductTemplate_ProductTemplateId] FOREIGN KEY([ProductTemplateId])
REFERENCES [dbo].[Catalog_ProductTemplate] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Catalog_ProductTemplateProductAttribute] CHECK CONSTRAINT [FK_Catalog_ProductTemplateProductAttribute_Catalog_ProductTemplate_ProductTemplateId]
GO
ALTER TABLE [dbo].[Cms_MenuItem]  WITH CHECK ADD  CONSTRAINT [FK_Cms_MenuItem_Cms_Menu_MenuId] FOREIGN KEY([MenuId])
REFERENCES [dbo].[Cms_Menu] ([Id])
GO
ALTER TABLE [dbo].[Cms_MenuItem] CHECK CONSTRAINT [FK_Cms_MenuItem_Cms_Menu_MenuId]
GO
ALTER TABLE [dbo].[Cms_MenuItem]  WITH CHECK ADD  CONSTRAINT [FK_Cms_MenuItem_Cms_MenuItem_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Cms_MenuItem] ([Id])
GO
ALTER TABLE [dbo].[Cms_MenuItem] CHECK CONSTRAINT [FK_Cms_MenuItem_Cms_MenuItem_ParentId]
GO
ALTER TABLE [dbo].[Cms_MenuItem]  WITH CHECK ADD  CONSTRAINT [FK_Cms_MenuItem_Core_Entity_EntityId] FOREIGN KEY([EntityId])
REFERENCES [dbo].[Core_Entity] ([Id])
GO
ALTER TABLE [dbo].[Cms_MenuItem] CHECK CONSTRAINT [FK_Cms_MenuItem_Core_Entity_EntityId]
GO
ALTER TABLE [dbo].[Cms_Page]  WITH CHECK ADD  CONSTRAINT [FK_Cms_Page_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Cms_Page] CHECK CONSTRAINT [FK_Cms_Page_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Cms_Page]  WITH CHECK ADD  CONSTRAINT [FK_Cms_Page_Core_User_LatestUpdatedById] FOREIGN KEY([LatestUpdatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Cms_Page] CHECK CONSTRAINT [FK_Cms_Page_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[Comments_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Comment_Comments_Comment_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Comments_Comment] ([Id])
GO
ALTER TABLE [dbo].[Comments_Comment] CHECK CONSTRAINT [FK_Comments_Comment_Comments_Comment_ParentId]
GO
ALTER TABLE [dbo].[Comments_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Comment_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Comments_Comment] CHECK CONSTRAINT [FK_Comments_Comment_Core_User_UserId]
GO
ALTER TABLE [dbo].[Contacts_Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contacts_Contact_Contacts_ContactArea_ContactAreaId] FOREIGN KEY([ContactAreaId])
REFERENCES [dbo].[Contacts_ContactArea] ([Id])
GO
ALTER TABLE [dbo].[Contacts_Contact] CHECK CONSTRAINT [FK_Contacts_Contact_Contacts_ContactArea_ContactAreaId]
GO
ALTER TABLE [dbo].[Core_Address]  WITH CHECK ADD  CONSTRAINT [FK_Core_Address_Core_Country_CountryId] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Core_Country] ([Id])
GO
ALTER TABLE [dbo].[Core_Address] CHECK CONSTRAINT [FK_Core_Address_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Core_Address]  WITH CHECK ADD  CONSTRAINT [FK_Core_Address_Core_District_DistrictId] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[Core_District] ([Id])
GO
ALTER TABLE [dbo].[Core_Address] CHECK CONSTRAINT [FK_Core_Address_Core_District_DistrictId]
GO
ALTER TABLE [dbo].[Core_Address]  WITH CHECK ADD  CONSTRAINT [FK_Core_Address_Core_StateOrProvince_StateOrProvinceId] FOREIGN KEY([StateOrProvinceId])
REFERENCES [dbo].[Core_StateOrProvince] ([Id])
GO
ALTER TABLE [dbo].[Core_Address] CHECK CONSTRAINT [FK_Core_Address_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Core_CustomerGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_Core_CustomerGroupUser_Core_CustomerGroup_CustomerGroupId] FOREIGN KEY([CustomerGroupId])
REFERENCES [dbo].[Core_CustomerGroup] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Core_CustomerGroupUser] CHECK CONSTRAINT [FK_Core_CustomerGroupUser_Core_CustomerGroup_CustomerGroupId]
GO
ALTER TABLE [dbo].[Core_CustomerGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_Core_CustomerGroupUser_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Core_CustomerGroupUser] CHECK CONSTRAINT [FK_Core_CustomerGroupUser_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_District]  WITH CHECK ADD  CONSTRAINT [FK_Core_District_Core_StateOrProvince_StateOrProvinceId] FOREIGN KEY([StateOrProvinceId])
REFERENCES [dbo].[Core_StateOrProvince] ([Id])
GO
ALTER TABLE [dbo].[Core_District] CHECK CONSTRAINT [FK_Core_District_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Core_Entity]  WITH CHECK ADD  CONSTRAINT [FK_Core_Entity_Core_EntityType_EntityTypeId] FOREIGN KEY([EntityTypeId])
REFERENCES [dbo].[Core_EntityType] ([Id])
GO
ALTER TABLE [dbo].[Core_Entity] CHECK CONSTRAINT [FK_Core_Entity_Core_EntityType_EntityTypeId]
GO
ALTER TABLE [dbo].[Core_RoleClaim]  WITH CHECK ADD  CONSTRAINT [FK_Core_RoleClaim_Core_Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Core_Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Core_RoleClaim] CHECK CONSTRAINT [FK_Core_RoleClaim_Core_Role_RoleId]
GO
ALTER TABLE [dbo].[Core_StateOrProvince]  WITH CHECK ADD  CONSTRAINT [FK_Core_StateOrProvince_Core_Country_CountryId] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Core_Country] ([Id])
GO
ALTER TABLE [dbo].[Core_StateOrProvince] CHECK CONSTRAINT [FK_Core_StateOrProvince_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Core_User]  WITH CHECK ADD  CONSTRAINT [FK_Core_User_Core_UserAddress_DefaultBillingAddressId] FOREIGN KEY([DefaultBillingAddressId])
REFERENCES [dbo].[Core_UserAddress] ([Id])
GO
ALTER TABLE [dbo].[Core_User] CHECK CONSTRAINT [FK_Core_User_Core_UserAddress_DefaultBillingAddressId]
GO
ALTER TABLE [dbo].[Core_User]  WITH CHECK ADD  CONSTRAINT [FK_Core_User_Core_UserAddress_DefaultShippingAddressId] FOREIGN KEY([DefaultShippingAddressId])
REFERENCES [dbo].[Core_UserAddress] ([Id])
GO
ALTER TABLE [dbo].[Core_User] CHECK CONSTRAINT [FK_Core_User_Core_UserAddress_DefaultShippingAddressId]
GO
ALTER TABLE [dbo].[Core_User]  WITH CHECK ADD  CONSTRAINT [FK_Core_User_Core_Vendor_VendorId] FOREIGN KEY([VendorId])
REFERENCES [dbo].[Core_Vendor] ([Id])
GO
ALTER TABLE [dbo].[Core_User] CHECK CONSTRAINT [FK_Core_User_Core_Vendor_VendorId]
GO
ALTER TABLE [dbo].[Core_UserAddress]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserAddress_Core_Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Core_Address] ([Id])
GO
ALTER TABLE [dbo].[Core_UserAddress] CHECK CONSTRAINT [FK_Core_UserAddress_Core_Address_AddressId]
GO
ALTER TABLE [dbo].[Core_UserAddress]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserAddress_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Core_UserAddress] CHECK CONSTRAINT [FK_Core_UserAddress_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserClaim]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserClaim_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Core_UserClaim] CHECK CONSTRAINT [FK_Core_UserClaim_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserLogin]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserLogin_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Core_UserLogin] CHECK CONSTRAINT [FK_Core_UserLogin_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserRole_Core_Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Core_Role] ([Id])
GO
ALTER TABLE [dbo].[Core_UserRole] CHECK CONSTRAINT [FK_Core_UserRole_Core_Role_RoleId]
GO
ALTER TABLE [dbo].[Core_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserRole_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Core_UserRole] CHECK CONSTRAINT [FK_Core_UserRole_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_UserToken]  WITH CHECK ADD  CONSTRAINT [FK_Core_UserToken_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Core_UserToken] CHECK CONSTRAINT [FK_Core_UserToken_Core_User_UserId]
GO
ALTER TABLE [dbo].[Core_WidgetInstance]  WITH CHECK ADD  CONSTRAINT [FK_Core_WidgetInstance_Core_Widget_WidgetId] FOREIGN KEY([WidgetId])
REFERENCES [dbo].[Core_Widget] ([Id])
GO
ALTER TABLE [dbo].[Core_WidgetInstance] CHECK CONSTRAINT [FK_Core_WidgetInstance_Core_Widget_WidgetId]
GO
ALTER TABLE [dbo].[Core_WidgetInstance]  WITH CHECK ADD  CONSTRAINT [FK_Core_WidgetInstance_Core_WidgetZone_WidgetZoneId] FOREIGN KEY([WidgetZoneId])
REFERENCES [dbo].[Core_WidgetZone] ([Id])
GO
ALTER TABLE [dbo].[Core_WidgetInstance] CHECK CONSTRAINT [FK_Core_WidgetInstance_Core_WidgetZone_WidgetZoneId]
GO
ALTER TABLE [dbo].[Inventory_Stock]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Stock_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Inventory_Stock] CHECK CONSTRAINT [FK_Inventory_Stock_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Inventory_Stock]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Stock_Inventory_Warehouse_WarehouseId] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[Inventory_Warehouse] ([Id])
GO
ALTER TABLE [dbo].[Inventory_Stock] CHECK CONSTRAINT [FK_Inventory_Stock_Inventory_Warehouse_WarehouseId]
GO
ALTER TABLE [dbo].[Inventory_StockHistory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_StockHistory_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Inventory_StockHistory] CHECK CONSTRAINT [FK_Inventory_StockHistory_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Inventory_StockHistory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_StockHistory_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Inventory_StockHistory] CHECK CONSTRAINT [FK_Inventory_StockHistory_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Inventory_StockHistory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_StockHistory_Inventory_Warehouse_WarehouseId] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[Inventory_Warehouse] ([Id])
GO
ALTER TABLE [dbo].[Inventory_StockHistory] CHECK CONSTRAINT [FK_Inventory_StockHistory_Inventory_Warehouse_WarehouseId]
GO
ALTER TABLE [dbo].[Inventory_Warehouse]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Warehouse_Core_Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Core_Address] ([Id])
GO
ALTER TABLE [dbo].[Inventory_Warehouse] CHECK CONSTRAINT [FK_Inventory_Warehouse_Core_Address_AddressId]
GO
ALTER TABLE [dbo].[Inventory_Warehouse]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Warehouse_Core_Vendor_VendorId] FOREIGN KEY([VendorId])
REFERENCES [dbo].[Core_Vendor] ([Id])
GO
ALTER TABLE [dbo].[Inventory_Warehouse] CHECK CONSTRAINT [FK_Inventory_Warehouse_Core_Vendor_VendorId]
GO
ALTER TABLE [dbo].[Localization_LocalizedContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_Localization_LocalizedContentProperty_Localization_Culture_CultureId] FOREIGN KEY([CultureId])
REFERENCES [dbo].[Localization_Culture] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Localization_LocalizedContentProperty] CHECK CONSTRAINT [FK_Localization_LocalizedContentProperty_Localization_Culture_CultureId]
GO
ALTER TABLE [dbo].[Localization_Resource]  WITH CHECK ADD  CONSTRAINT [FK_Localization_Resource_Localization_Culture_CultureId] FOREIGN KEY([CultureId])
REFERENCES [dbo].[Localization_Culture] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Localization_Resource] CHECK CONSTRAINT [FK_Localization_Resource_Localization_Culture_CultureId]
GO
ALTER TABLE [dbo].[News_NewsItem]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsItem_Core_Media_ThumbnailImageId] FOREIGN KEY([ThumbnailImageId])
REFERENCES [dbo].[Core_Media] ([Id])
GO
ALTER TABLE [dbo].[News_NewsItem] CHECK CONSTRAINT [FK_News_NewsItem_Core_Media_ThumbnailImageId]
GO
ALTER TABLE [dbo].[News_NewsItem]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsItem_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[News_NewsItem] CHECK CONSTRAINT [FK_News_NewsItem_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[News_NewsItem]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsItem_Core_User_LatestUpdatedById] FOREIGN KEY([LatestUpdatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[News_NewsItem] CHECK CONSTRAINT [FK_News_NewsItem_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[News_NewsItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsItemCategory_News_NewsCategory_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[News_NewsCategory] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[News_NewsItemCategory] CHECK CONSTRAINT [FK_News_NewsItemCategory_News_NewsCategory_CategoryId]
GO
ALTER TABLE [dbo].[News_NewsItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsItemCategory_News_NewsItem_NewsItemId] FOREIGN KEY([NewsItemId])
REFERENCES [dbo].[News_NewsItem] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[News_NewsItemCategory] CHECK CONSTRAINT [FK_News_NewsItemCategory_News_NewsItem_NewsItemId]
GO
ALTER TABLE [dbo].[Orders_Order]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Order_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Orders_Order] CHECK CONSTRAINT [FK_Orders_Order_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Orders_Order]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Order_Core_User_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Orders_Order] CHECK CONSTRAINT [FK_Orders_Order_Core_User_CustomerId]
GO
ALTER TABLE [dbo].[Orders_Order]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Order_Core_User_LatestUpdatedById] FOREIGN KEY([LatestUpdatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Orders_Order] CHECK CONSTRAINT [FK_Orders_Order_Core_User_LatestUpdatedById]
GO
ALTER TABLE [dbo].[Orders_Order]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Order_Orders_Order_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Orders_Order] ([Id])
GO
ALTER TABLE [dbo].[Orders_Order] CHECK CONSTRAINT [FK_Orders_Order_Orders_Order_ParentId]
GO
ALTER TABLE [dbo].[Orders_Order]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Order_Orders_OrderAddress_BillingAddressId] FOREIGN KEY([BillingAddressId])
REFERENCES [dbo].[Orders_OrderAddress] ([Id])
GO
ALTER TABLE [dbo].[Orders_Order] CHECK CONSTRAINT [FK_Orders_Order_Orders_OrderAddress_BillingAddressId]
GO
ALTER TABLE [dbo].[Orders_Order]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Order_Orders_OrderAddress_ShippingAddressId] FOREIGN KEY([ShippingAddressId])
REFERENCES [dbo].[Orders_OrderAddress] ([Id])
GO
ALTER TABLE [dbo].[Orders_Order] CHECK CONSTRAINT [FK_Orders_Order_Orders_OrderAddress_ShippingAddressId]
GO
ALTER TABLE [dbo].[Orders_OrderAddress]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderAddress_Core_Country_CountryId] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Core_Country] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderAddress] CHECK CONSTRAINT [FK_Orders_OrderAddress_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Orders_OrderAddress]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderAddress_Core_District_DistrictId] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[Core_District] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderAddress] CHECK CONSTRAINT [FK_Orders_OrderAddress_Core_District_DistrictId]
GO
ALTER TABLE [dbo].[Orders_OrderAddress]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderAddress_Core_StateOrProvince_StateOrProvinceId] FOREIGN KEY([StateOrProvinceId])
REFERENCES [dbo].[Core_StateOrProvince] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderAddress] CHECK CONSTRAINT [FK_Orders_OrderAddress_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Orders_OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderHistory_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderHistory] CHECK CONSTRAINT [FK_Orders_OrderHistory_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Orders_OrderHistory]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderHistory_Orders_Order_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders_Order] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderHistory] CHECK CONSTRAINT [FK_Orders_OrderHistory_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Orders_OrderItem]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderItem_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderItem] CHECK CONSTRAINT [FK_Orders_OrderItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Orders_OrderItem]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderItem_Orders_Order_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders_Order] ([Id])
GO
ALTER TABLE [dbo].[Orders_OrderItem] CHECK CONSTRAINT [FK_Orders_OrderItem_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Payments_Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Payment_Orders_Order_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders_Order] ([Id])
GO
ALTER TABLE [dbo].[Payments_Payment] CHECK CONSTRAINT [FK_Payments_Payment_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCategory]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleCategory_Catalog_Category_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Catalog_Category] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CartRuleCategory] CHECK CONSTRAINT [FK_Pricing_CartRuleCategory_Catalog_Category_CategoryId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCategory]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleCategory_Pricing_CartRule_CartRuleId] FOREIGN KEY([CartRuleId])
REFERENCES [dbo].[Pricing_CartRule] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Pricing_CartRuleCategory] CHECK CONSTRAINT [FK_Pricing_CartRuleCategory_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCustomerGroup]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleCustomerGroup_Core_CustomerGroup_CustomerGroupId] FOREIGN KEY([CustomerGroupId])
REFERENCES [dbo].[Core_CustomerGroup] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CartRuleCustomerGroup] CHECK CONSTRAINT [FK_Pricing_CartRuleCustomerGroup_Core_CustomerGroup_CustomerGroupId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleCustomerGroup]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleCustomerGroup_Pricing_CartRule_CartRuleId] FOREIGN KEY([CartRuleId])
REFERENCES [dbo].[Pricing_CartRule] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Pricing_CartRuleCustomerGroup] CHECK CONSTRAINT [FK_Pricing_CartRuleCustomerGroup_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleProduct]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleProduct_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CartRuleProduct] CHECK CONSTRAINT [FK_Pricing_CartRuleProduct_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleProduct]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleProduct_Pricing_CartRule_CartRuleId] FOREIGN KEY([CartRuleId])
REFERENCES [dbo].[Pricing_CartRule] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Pricing_CartRuleProduct] CHECK CONSTRAINT [FK_Pricing_CartRuleProduct_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleUsage_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage] CHECK CONSTRAINT [FK_Pricing_CartRuleUsage_Core_User_UserId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleUsage_Pricing_CartRule_CartRuleId] FOREIGN KEY([CartRuleId])
REFERENCES [dbo].[Pricing_CartRule] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage] CHECK CONSTRAINT [FK_Pricing_CartRuleUsage_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CartRuleUsage_Pricing_Coupon_CouponId] FOREIGN KEY([CouponId])
REFERENCES [dbo].[Pricing_Coupon] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CartRuleUsage] CHECK CONSTRAINT [FK_Pricing_CartRuleUsage_Pricing_Coupon_CouponId]
GO
ALTER TABLE [dbo].[Pricing_CatalogRuleCustomerGroup]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CatalogRuleCustomerGroup_Core_CustomerGroup_CustomerGroupId] FOREIGN KEY([CustomerGroupId])
REFERENCES [dbo].[Core_CustomerGroup] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CatalogRuleCustomerGroup] CHECK CONSTRAINT [FK_Pricing_CatalogRuleCustomerGroup_Core_CustomerGroup_CustomerGroupId]
GO
ALTER TABLE [dbo].[Pricing_CatalogRuleCustomerGroup]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_CatalogRuleCustomerGroup_Pricing_CatalogRule_CatalogRuleId] FOREIGN KEY([CatalogRuleId])
REFERENCES [dbo].[Pricing_CatalogRule] ([Id])
GO
ALTER TABLE [dbo].[Pricing_CatalogRuleCustomerGroup] CHECK CONSTRAINT [FK_Pricing_CatalogRuleCustomerGroup_Pricing_CatalogRule_CatalogRuleId]
GO
ALTER TABLE [dbo].[Pricing_Coupon]  WITH CHECK ADD  CONSTRAINT [FK_Pricing_Coupon_Pricing_CartRule_CartRuleId] FOREIGN KEY([CartRuleId])
REFERENCES [dbo].[Pricing_CartRule] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Pricing_Coupon] CHECK CONSTRAINT [FK_Pricing_Coupon_Pricing_CartRule_CartRuleId]
GO
ALTER TABLE [dbo].[ProductComparison_ComparingProduct]  WITH CHECK ADD  CONSTRAINT [FK_ProductComparison_ComparingProduct_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[ProductComparison_ComparingProduct] CHECK CONSTRAINT [FK_ProductComparison_ComparingProduct_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[ProductComparison_ComparingProduct]  WITH CHECK ADD  CONSTRAINT [FK_ProductComparison_ComparingProduct_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[ProductComparison_ComparingProduct] CHECK CONSTRAINT [FK_ProductComparison_ComparingProduct_Core_User_UserId]
GO
ALTER TABLE [dbo].[Reviews_Reply]  WITH CHECK ADD  CONSTRAINT [FK_Reviews_Reply_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Reviews_Reply] CHECK CONSTRAINT [FK_Reviews_Reply_Core_User_UserId]
GO
ALTER TABLE [dbo].[Reviews_Reply]  WITH CHECK ADD  CONSTRAINT [FK_Reviews_Reply_Reviews_Review_ReviewId] FOREIGN KEY([ReviewId])
REFERENCES [dbo].[Reviews_Review] ([Id])
GO
ALTER TABLE [dbo].[Reviews_Reply] CHECK CONSTRAINT [FK_Reviews_Reply_Reviews_Review_ReviewId]
GO
ALTER TABLE [dbo].[Reviews_Review]  WITH CHECK ADD  CONSTRAINT [FK_Reviews_Review_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Reviews_Review] CHECK CONSTRAINT [FK_Reviews_Review_Core_User_UserId]
GO
ALTER TABLE [dbo].[Shipments_Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipments_Shipment_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[Shipments_Shipment] CHECK CONSTRAINT [FK_Shipments_Shipment_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[Shipments_Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipments_Shipment_Inventory_Warehouse_WarehouseId] FOREIGN KEY([WarehouseId])
REFERENCES [dbo].[Inventory_Warehouse] ([Id])
GO
ALTER TABLE [dbo].[Shipments_Shipment] CHECK CONSTRAINT [FK_Shipments_Shipment_Inventory_Warehouse_WarehouseId]
GO
ALTER TABLE [dbo].[Shipments_Shipment]  WITH CHECK ADD  CONSTRAINT [FK_Shipments_Shipment_Orders_Order_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders_Order] ([Id])
GO
ALTER TABLE [dbo].[Shipments_Shipment] CHECK CONSTRAINT [FK_Shipments_Shipment_Orders_Order_OrderId]
GO
ALTER TABLE [dbo].[Shipments_ShipmentItem]  WITH CHECK ADD  CONSTRAINT [FK_Shipments_ShipmentItem_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[Shipments_ShipmentItem] CHECK CONSTRAINT [FK_Shipments_ShipmentItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[Shipments_ShipmentItem]  WITH CHECK ADD  CONSTRAINT [FK_Shipments_ShipmentItem_Shipments_Shipment_ShipmentId] FOREIGN KEY([ShipmentId])
REFERENCES [dbo].[Shipments_Shipment] ([Id])
GO
ALTER TABLE [dbo].[Shipments_ShipmentItem] CHECK CONSTRAINT [FK_Shipments_ShipmentItem_Shipments_Shipment_ShipmentId]
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination]  WITH CHECK ADD  CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_Country_CountryId] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Core_Country] ([Id])
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination] CHECK CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination]  WITH CHECK ADD  CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_District_DistrictId] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[Core_District] ([Id])
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination] CHECK CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_District_DistrictId]
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination]  WITH CHECK ADD  CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_StateOrProvince_StateOrProvinceId] FOREIGN KEY([StateOrProvinceId])
REFERENCES [dbo].[Core_StateOrProvince] ([Id])
GO
ALTER TABLE [dbo].[ShippingTableRate_PriceAndDestination] CHECK CONSTRAINT [FK_ShippingTableRate_PriceAndDestination_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[ShoppingCart_Cart]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_Cart_Core_User_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[ShoppingCart_Cart] CHECK CONSTRAINT [FK_ShoppingCart_Cart_Core_User_CreatedById]
GO
ALTER TABLE [dbo].[ShoppingCart_Cart]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_Cart_Core_User_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[ShoppingCart_Cart] CHECK CONSTRAINT [FK_ShoppingCart_Cart_Core_User_CustomerId]
GO
ALTER TABLE [dbo].[ShoppingCart_CartItem]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_CartItem_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[ShoppingCart_CartItem] CHECK CONSTRAINT [FK_ShoppingCart_CartItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[ShoppingCart_CartItem]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_CartItem_ShoppingCart_Cart_CartId] FOREIGN KEY([CartId])
REFERENCES [dbo].[ShoppingCart_Cart] ([Id])
GO
ALTER TABLE [dbo].[ShoppingCart_CartItem] CHECK CONSTRAINT [FK_ShoppingCart_CartItem_ShoppingCart_Cart_CartId]
GO
ALTER TABLE [dbo].[Tax_TaxRate]  WITH CHECK ADD  CONSTRAINT [FK_Tax_TaxRate_Core_Country_CountryId] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Core_Country] ([Id])
GO
ALTER TABLE [dbo].[Tax_TaxRate] CHECK CONSTRAINT [FK_Tax_TaxRate_Core_Country_CountryId]
GO
ALTER TABLE [dbo].[Tax_TaxRate]  WITH CHECK ADD  CONSTRAINT [FK_Tax_TaxRate_Core_StateOrProvince_StateOrProvinceId] FOREIGN KEY([StateOrProvinceId])
REFERENCES [dbo].[Core_StateOrProvince] ([Id])
GO
ALTER TABLE [dbo].[Tax_TaxRate] CHECK CONSTRAINT [FK_Tax_TaxRate_Core_StateOrProvince_StateOrProvinceId]
GO
ALTER TABLE [dbo].[Tax_TaxRate]  WITH CHECK ADD  CONSTRAINT [FK_Tax_TaxRate_Tax_TaxClass_TaxClassId] FOREIGN KEY([TaxClassId])
REFERENCES [dbo].[Tax_TaxClass] ([Id])
GO
ALTER TABLE [dbo].[Tax_TaxRate] CHECK CONSTRAINT [FK_Tax_TaxRate_Tax_TaxClass_TaxClassId]
GO
ALTER TABLE [dbo].[WishList_WishList]  WITH CHECK ADD  CONSTRAINT [FK_WishList_WishList_Core_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Core_User] ([Id])
GO
ALTER TABLE [dbo].[WishList_WishList] CHECK CONSTRAINT [FK_WishList_WishList_Core_User_UserId]
GO
ALTER TABLE [dbo].[WishList_WishListItem]  WITH CHECK ADD  CONSTRAINT [FK_WishList_WishListItem_Catalog_Product_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Catalog_Product] ([Id])
GO
ALTER TABLE [dbo].[WishList_WishListItem] CHECK CONSTRAINT [FK_WishList_WishListItem_Catalog_Product_ProductId]
GO
ALTER TABLE [dbo].[WishList_WishListItem]  WITH CHECK ADD  CONSTRAINT [FK_WishList_WishListItem_WishList_WishList_WishListId] FOREIGN KEY([WishListId])
REFERENCES [dbo].[WishList_WishList] ([Id])
GO
ALTER TABLE [dbo].[WishList_WishListItem] CHECK CONSTRAINT [FK_WishList_WishListItem_WishList_WishList_WishListId]
GO
USE [master]
GO
ALTER DATABASE [SimplCommerce] SET  READ_WRITE 
GO
