﻿using System.Collections.Generic;
using System.Globalization;

namespace SimplCommerce.Infrastructure.Helpers
{
    public static class CurrencyHelper
    {
        private static readonly List<string> _zeroDecimalCurrencies = new List<string> {"VND"};

        public static bool IsZeroDecimalCurrencies(CultureInfo cultureInfo)
        {
            var regionInfo = new RegionInfo(System.Threading.Thread.CurrentThread.CurrentUICulture.LCID);
            return _zeroDecimalCurrencies.Contains(regionInfo.ISOCurrencySymbol);
        }
    }
}
