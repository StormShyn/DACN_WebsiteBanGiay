﻿using SimplCommerce.Infrastructure.Models;
using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Infrastructure.Localization
{
    public class Resource : EntityBase
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [StringLength(450)]
        public string Key { get; set; }

        public string Value { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string CultureId { get; set; }

        public Culture Culture { get; set; }
    }
}