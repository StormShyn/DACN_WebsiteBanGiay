﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.WishList.Areas.WishList.ViewModels
{
    public class ShareWishListForm
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [EmailAddress]
        public string EmailAddress { get; set; }
        
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Message { get; set; }
    }
}
