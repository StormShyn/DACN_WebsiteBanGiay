﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Cms.Areas.Cms.ViewModels
{
    public class PageForm
    {
        public PageForm()
        {
            IsPublished = true;
        }

        public long Id { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Slug { get; set; }

        public string MetaTitle { get; set; }

        public string MetaKeywords { get; set; }

        public string MetaDescription { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Body { get; set; }

        public bool IsPublished { get; set; }
    }
}
