﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Contacts.Areas.Contacts.ViewModels
{
    public class ContactAreaForm
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Name { get; set; }
    }
}
