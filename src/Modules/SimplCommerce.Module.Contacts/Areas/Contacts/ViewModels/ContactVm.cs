﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Contacts.Areas.Contacts.ViewModels
{
    public class ContactVm
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Address { get; set; }

        public string Content { get; set; }

        public long ContactAreaId { get; set; }

        public IList<ContactAreaVm> ContactAreas { get; set; } = new List<ContactAreaVm>();

        public CompanyInformation Company { get; set; }
    }
}
