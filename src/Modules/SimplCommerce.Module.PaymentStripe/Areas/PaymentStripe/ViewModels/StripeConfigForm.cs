﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.PaymentStripe.Areas.PaymentStripe.ViewModels
{
    public class StripeConfigForm
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string PublicKey { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string PrivateKey { get; set; }
    }
}
