﻿using System.ComponentModel.DataAnnotations;
using SimplCommerce.Infrastructure.Models;

namespace SimplCommerce.Module.ActivityLog.Models
{
    public class ActivityType : EntityBase
    {
        public ActivityType (long id)
        {
            Id = id;
        }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [StringLength(450)]
        public string Name { get; set; }
    }
}
