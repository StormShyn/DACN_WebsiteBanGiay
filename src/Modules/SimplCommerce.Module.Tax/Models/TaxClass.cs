﻿using System.ComponentModel.DataAnnotations;
using SimplCommerce.Infrastructure.Models;

namespace SimplCommerce.Module.Tax.Models
{
    public class TaxClass : EntityBase
    {
        public TaxClass() { }

        public TaxClass(long id)
        {
            Id = id;
        }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [StringLength(450)]
        public string Name { get; set; }
    }
}
