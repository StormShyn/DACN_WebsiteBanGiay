﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace SimplCommerce.Module.Tax.Areas.Tax.ViewModels
{
    public class TaxRateImportForm
    {
        public bool IncludeHeader { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string CsvDelimiter { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public IFormFile CsvFile { get; set; }
    }
}
