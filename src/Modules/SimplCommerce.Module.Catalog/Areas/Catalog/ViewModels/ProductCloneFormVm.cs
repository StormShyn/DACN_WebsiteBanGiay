﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Catalog.Areas.Catalog.ViewModels
{
    public class ProductCloneFormVm
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Slug { get; set; }
    }
}
