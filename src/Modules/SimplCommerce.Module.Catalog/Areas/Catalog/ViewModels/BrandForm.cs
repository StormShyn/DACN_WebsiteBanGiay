﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Catalog.Areas.Catalog.ViewModels
{
    public class BrandForm
    {
        public BrandForm()
        {
            IsPublished = true;
        }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Slug { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Name { get; set; }

        public bool IsPublished { get; set; }
    }
}
