#pragma checksum "C:\Users\peheo\Desktop\SimplCommerce-master - Copy - Copy\SimplCommerce-master\src\Modules\SimplCommerce.Module.SampleData\Areas\SampleData\Views\Shared\Components\SampleDataForm\Default.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "deabbe4f8743d951ed3c1a5d1828e0b8d3ed94b4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_SampleData_Views_Shared_Components_SampleDataForm_Default), @"mvc.1.0.view", @"/Areas/SampleData/Views/Shared/Components/SampleDataForm/Default.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\peheo\Desktop\SimplCommerce-master - Copy - Copy\SimplCommerce-master\src\Modules\SimplCommerce.Module.SampleData\Areas\SampleData\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\peheo\Desktop\SimplCommerce-master - Copy - Copy\SimplCommerce-master\src\Modules\SimplCommerce.Module.SampleData\Areas\SampleData\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\peheo\Desktop\SimplCommerce-master - Copy - Copy\SimplCommerce-master\src\Modules\SimplCommerce.Module.SampleData\Areas\SampleData\Views\_ViewImports.cshtml"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"deabbe4f8743d951ed3c1a5d1828e0b8d3ed94b4", @"/Areas/SampleData/Views/Shared/Components/SampleDataForm/Default.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8348485d980c07ec0eb4e4b2fd46d42de572e333", @"/Areas/SampleData/Views/_ViewImports.cshtml")]
    public class Areas_SampleData_Views_Shared_Components_SampleDataForm_Default : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<SimplCommerce.Module.SampleData.Areas.SampleData.ViewModels.SampleDataOption>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<div>
   <!-- <h2 class=""page-header"">Sample data</h2>
    <h4>Delete all current catalog data and create sample catalog</h4>
    <form asp-area=""SampleData"" asp-controller=""SampleData"" asp-action=""ResetToSample"" method=""post"">
        <div class=""form-row"">
            <div class=""col-auto"">
                <label class=""col-form-label"" asp-for=""Industry"">Industry</label>
            </div>
            <div class=""col-auto"">
                <select class=""form-control"" asp-for=""Industry"" asp-items=""Model.AvailableIndustries.Select(x => new SelectListItem { Value = x, Text = x })""></select>
            </div>
            <div class=""col-auto"">
                <button type=""submit"" class=""btn btn-danger"">Do it!</button>
            </div>
        </div>
    </form> -->
</div>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IConfiguration AppSetting { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<SimplCommerce.Module.SampleData.Areas.SampleData.ViewModels.SampleDataOption> Html { get; private set; }
    }
}
#pragma warning restore 1591
