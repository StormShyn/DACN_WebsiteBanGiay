﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Core.Areas.Core.ViewModels.Manage
{
    public class UserInfoVm
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Email { get; set; }
    }
}
