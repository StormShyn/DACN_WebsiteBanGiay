﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Core.Areas.Core.ViewModels.Manage
{
    public class VerifyPhoneNumberViewModel
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        public string Code { get; set; }

        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }
}
