﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.Core.Areas.Core.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "{0} là bắt buộc.")]
        [EmailAddress]
        public string Email { get; set; }
    }
}
